import {fetchWrapper} from '../_helpers';

const baseUrl = `${process.env.REACT_APP_PATIENT_MS_BASE_URL}`;

export const patientService = {
    getAll,
    getById,
    create,
    update
};

function getAll(token) {
    return fetchWrapper.get(`${baseUrl}/list`, token);
}

function getById(id, token) {
    return fetchWrapper.get(`${baseUrl}/${id}`, token);
}

function create(params, token) {
    return fetchWrapper.post(`${baseUrl}`, params, token);
}

function update(id, params, token) {
    return fetchWrapper.put(`${baseUrl}/${id}`, params, token);
}
