import {fetchWrapper} from '../_helpers';

const baseUrl = `${process.env.REACT_APP_NOTE_MS_BASE_URL}`;

export const noteService = {
    getByPatientId,
    create,
    update,
    getByPatientFamily
};

function getByPatientId(patientId, token) {
    return fetchWrapper.get(`${baseUrl}/${patientId}`, token);
}

function getByPatientFamily(patientFamily, token) {
    return fetchWrapper.getWithParams(`${baseUrl}`, patientFamily, token);
}

function create(body, token) {
    return fetchWrapper.post(`${baseUrl}`, body, token);
}

function update(noteId, body, token) {
    return fetchWrapper.put(`${baseUrl}/${noteId}`, body, token);
}
