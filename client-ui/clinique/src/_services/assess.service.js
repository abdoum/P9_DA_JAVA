import { fetchWrapper } from '../_helpers';

const baseUrl = `${process.env.REACT_APP_ASSESS_MS_BASE_URL}`;

export const assessService = {
    getByPatientId,
    getByPatientFamily
};

function getByPatientId(patientId, token) {
    return fetchWrapper.get(`${baseUrl}/${patientId}`, token);
}

function getByPatientFamily(params, token) {
    return fetchWrapper.getWithParams(`${baseUrl}`, params, token);
}