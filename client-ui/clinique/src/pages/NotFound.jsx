import {Link} from "react-router-dom";
import {Button, Result} from "antd";
import {paths} from "../utils/Paths";

export default function  NotFound()
{
    return (
        <Result
            status="404"
            title="404"
            subTitle="La page demandée n'a pas été trouvée."
            extra={<Button type="primary"><Link to={paths.list}>Accueil</Link></Button>}
        />
    )
}