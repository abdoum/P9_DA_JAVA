import {useNavigate} from "react-router-dom";
import {PageHeader} from "antd";
import NoteEditor from "../components/NoteEditor";
import {paths} from "../utils/Paths";

export default function PatientHistory() {
    const navigate = useNavigate();
    return (
        <nav className='p-20'>
            <PageHeader
                className="site-page-header"
                onBack={() => navigate(paths.list)}
                title="Historique du patient"
            />
            <NoteEditor/>
        </nav>
    );
}
