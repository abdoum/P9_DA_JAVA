import AddPatientForm from "../components/AddPatientForm";

export default function  Add()
{
    return (
        <div className='px-4'>
            <AddPatientForm />
        </div>
    )
}