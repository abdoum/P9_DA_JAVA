import PatientTable from "../components/PatientTable";
import {motion} from "framer-motion";
import {Component} from "react";
import {withRouter} from "../components/Wrapper";
import {withKeycloak} from "@react-keycloak/web";

class List extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="p-10">
                <header className="h-full flex justify-center flex-col">
                    <motion.h1
                        animate={{fontSize: 50, x: 30}} className="text-orange-500 text-5xl">
                        Patients
                    </motion.h1>
                </header>
                    <PatientTable/>
            </div>
        )
    }
}

export default withRouter(withKeycloak(List))