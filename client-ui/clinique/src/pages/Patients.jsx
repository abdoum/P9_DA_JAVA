import { Route, Routes } from 'react-router-dom';

import List from './List';
import  Edit from './Edit';
import {paths} from "../utils/Paths";

function Patients({match} ) {
    const { path } = match;

    return (
        <Routes>
            <Route exact path={paths.list} component={List} />
            <Route path={`${path}/${paths.add}`} component={Edit} />
            <Route path={`${path}/${paths.edit}`} component={Edit} />
        </Routes>
    );
}

export { Patients };