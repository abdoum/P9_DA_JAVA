import {PageHeader} from "antd";
import AssessReport from "../components/AssessReport";
import {Component} from "react";
import {withRouter} from "../components/Wrapper";
import {paths} from "../utils/Paths";

class Assess extends Component {
    constructor(props) {
        super(props);
        this.navigateToPath = this.navigateToPath.bind(this)
        this.state={
            patient: props.state
        }
    }

    navigateToPath(path) {
        this.props.navigate(path)
    }

    render() {
        return (
            <nav className='p-20 text-orange-500'>
                <PageHeader
                    className="text-orange-500"
                    onBack={() => this.navigateToPath(paths.list)}
                    title="Rapport de diabète"
                />
                <AssessReport patient={this.state.patient} />
            </nav>
        );
    }
}

export default withRouter(Assess)
