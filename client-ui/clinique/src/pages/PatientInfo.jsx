import {useLocation, useNavigate} from "react-router-dom";
import {PageHeader} from "antd";
import {paths} from "../utils/Paths";
import PatientDetails from "../components/PatientDetails";

export default function PatientInfo() {
    const navigate = useNavigate();
    const patient = useLocation().state;
    return (
        <nav className='p-20'>
            <PageHeader
                className="site-page-header"
                onBack={() => navigate(paths.list)}
                title={`${patient.given} ${patient.family.toUpperCase()}`}
            />
            <PatientDetails patient={patient}/>
        </nav>
    );
}
