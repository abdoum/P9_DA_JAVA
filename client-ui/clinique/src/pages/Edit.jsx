import EditPatientForm from "../components/EditPatientForm";
import {useLocation, useNavigate, useParams} from "react-router-dom";
import {PageHeader} from "antd";
import {paths} from "../utils/Paths";

export default function  Edit()
{
    const {id} = useParams();
    const {history} = useLocation();
    const navigate = useNavigate();

    return (
        <div className='px-4'>
            <PageHeader
                className="site-page-header"
                onBack={() => navigate(paths.list)}
                title="Modifier le patient"
            />
            <EditPatientForm match={{history, id}} />
        </div>
    )
}