import {Link} from "react-router-dom";
import {paths} from "../utils/Paths";

export function Landing() {
    return (
        <div className="h-full w-full bg-amber-400 from-orange-500 bg-gradient-to-br p-20 ">
            <h1 className="text-5xl text-white">Mediscreen</h1>
            <p className="text-2xl text-white">
                <Link className="text-white underline decoration-dotted" to={paths.list}>Connectez-vous</Link> pour continuer...</p>
        </div>
    );
}