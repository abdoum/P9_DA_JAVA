import './App.less';
import {Route, Routes} from "react-router-dom";
import List from "./pages/List";
import Edit from "./pages/Edit";
import Add from "./pages/Add";
import NotFound from "./pages/NotFound";
import Assess from "./pages/Assess";
import Keycloak from "keycloak-js";
import {ReactKeycloakProvider} from '@react-keycloak/web'
import {Layout, Spin} from "antd";
import Sider from "antd/es/layout/Sider";
import Nav from "./components/Nav";
import {Content} from "antd/es/layout/layout";
import PrivateRoute from "./components/PrivateRoute";
import PatientHistory from "./pages/PatientHistory";
import {Landing} from "./pages/Landing";
import {paths} from "./utils/Paths";
import PatientInfo from "./pages/PatientInfo";

function App() {
    document.title = "Clinique";
    const keycloak = new Keycloak({
        url: `http://localhost:8080/auth`,
        realm: "mediscreen",
        clientId: "mediscreen-app"
    })
    const initOptions = {pkceMethod: 'S256'}

    const handleOnEvent = async (event) => {
        if (event === 'onAuthSuccess') {
            if (keycloak.authenticated) {
                // message.success("Connecté!")
            } else {
                // message.error(error)
            }
        }
    }

    const loadingComponent = (
        <div className="m-20 mb-20 pl-3 pr-5 text-center backdrop-blur">
            <Spin/>
        </div>
    )

    return (
        <Layout style={{
            height: '100vh',
        }}>
            <ReactKeycloakProvider
                authClient={keycloak}
                initOptions={initOptions}
                LoadingComponent={loadingComponent}
                onEvent={(event, error) => handleOnEvent(event, error)}
            >
                <Sider>
                    <Nav props={keycloak}/>
                </Sider>
                <Layout>
                    <Content>
                        <Routes>
                            <Route path={paths.landing} element={<Landing/>}/>
                            <Route path={paths.list} element={<PrivateRoute><List props={keycloak}/></PrivateRoute>}/>
                            <Route path={paths.add} element={<PrivateRoute><Add/></PrivateRoute>}/>
                            <Route path={`${paths.patient}/:id`} element={<PrivateRoute><PatientInfo/></PrivateRoute>}/>
                            <Route path={`${paths.edit}/:id`} element={<Edit/>}/>
                            <Route path={`${paths.history}/:id`} element={<PrivateRoute><PatientHistory/></PrivateRoute>}/>
                            <Route path={`${paths.assess}/:id`}
                                   element={<PrivateRoute><Assess props={keycloak}/></PrivateRoute>}/>
                            <Route path='*' element={<NotFound/>}/>
                        </Routes>
                    </Content>
                </Layout>
            </ReactKeycloakProvider>
        </Layout>
    )
}

export default App;
