
export const isPractitioner = (keycloak) => {
    return keycloak && keycloak.tokenParsed && keycloak.tokenParsed.resource_access['mediscreen-app'].roles.includes('PRACTITIONER')
}

export const handleLogError = (error) => {
    if (error.response) {
        console.log(error.response.data);
    } else if (error.request) {
        console.log(error.request);
    } else {
        console.log(error.message);
    }
}