import moment from "moment";

export default function dateInFrench(date) {
    return moment(date).locale('fr').format('dddd DD MMMM yyyy',);
}