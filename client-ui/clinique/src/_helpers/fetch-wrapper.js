import axios from "axios";

export const fetchWrapper = {
    get,
    post,
    put,
    getWithParams
};

function get(url, token = '') {
    return instance.get(url, {headers: getHeaders(token)}).then(handleResponse);
}

function getWithParams(url, params, token = '') {
    let path = new URL(url)
    path.search = new URLSearchParams(params);
    return instance.get(path, {headers: getHeaders(token)}).then(handleResponse);
}

function post(url, body, token = '') {
    return instance.post(url, jsonBody(body), {headers: getHeaders(token)}).then(handleResponse);
}

function put(url, body, token = '') {
    return instance.put(url, jsonBody(body), {headers: getHeaders(token)}).then(handleResponse);
}

// -- Axios

const instance = axios.create()

instance.interceptors.response.use(response => {
    return response;
}, function (error) {
    if (error?.response?.status === 404) {
        return {status: error?.response?.status};
    }
    return Promise.reject(error?.response);
});

// -- Helper functions

function handleResponse(response) {
    let statusCode = response.status.toString();
    let data = response.data;
    if (statusCode.substring(0,1) !== '2' ) {
        const error = (data?.validationErrors) || (data && data.message);
        return Promise.reject(error);
    }
    return data;
}

function bearerAuth(token) {
    return `Bearer ${token}`
}

function getHeaders(token) {
    return {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': bearerAuth(token)
    };
}

function jsonBody(body) {
    return JSON.stringify(body);
}