import React from "react";
import {Button, Form, Space} from "antd";
import TextArea from "antd/es/input/TextArea";
import {useNavigate} from "react-router-dom";
import {paths} from "../utils/Paths";

export default function Editor({onChange, onSubmit, submitting, value}) {
    const inputRef = React.useRef(null);
    const navigate = useNavigate();
    const sharedProps = {
        style: {
            width: '100%',
        },
        defaultValue: 'Patient states that ',
        ref: inputRef,
    };

    function goHome() {
       return  navigate(paths.list)
    }

    return (
        <>
            <Form.Item>
                <TextArea allowClear onFocus={() => {
                    inputRef.current.focus({
                        cursor: 'end',
                    });
                }} {...sharedProps}
                          rows={4} autoSize={{minRows: 2, maxRows: 6}}
                          onChange={onChange} value={value}/>
            </Form.Item>
            <Form.Item>
                <Space>
                <Button htmlType="submit" loading={submitting} onClick={onSubmit} type="primary">
                    Ajouter une note
                </Button><Button onClick={goHome}>Annuler</Button>
                </Space>
            </Form.Item>
        </>
    )
}
