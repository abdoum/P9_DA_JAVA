import {useNavigate} from "react-router-dom";
import {Button, Descriptions, Tooltip} from "antd";
import {paths} from "../utils/Paths";

export default function PatientDetails({ patient}) {
    const navigate = useNavigate();
    return (
        <>
            <div className='py-6'><Tooltip title="modifier">
                <Button onClick={()=> navigate(`/${paths.edit}/${patient.id}`)} type="dashed"  danger>Modifier</Button>
            </Tooltip></div>

            <Descriptions title="" bordered>
                <Descriptions.Item
                    label="First Name" span={4}> {patient.given?.toUpperCase()}</Descriptions.Item>
                <Descriptions.Item label="Family Name"
                                   span={4}> {patient.family?.toUpperCase()} </Descriptions.Item>
                <Descriptions.Item label="Birth Date" span={2}> {patient.dob} </Descriptions.Item>
                <Descriptions.Item label="Sex"> {patient.sex} </Descriptions.Item>
                <Descriptions.Item label="Address" span={4}> {patient.address} </Descriptions.Item>
                <Descriptions.Item label="Phone"> {patient.phone} </Descriptions.Item>
            </Descriptions>
        </>
    )
}