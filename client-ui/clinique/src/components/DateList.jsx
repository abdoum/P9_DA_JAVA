import {Comment, List, Timeline} from "antd";
import React from "react";
import dateInFrench from "../_helpers/DateInFrench";
import EditableNote from "./EditableNote";
import DateSince from "./DateSince";

export default function DateList(props) {
    const notesByDate = props.notesByDate;
    console.log(notesByDate)
    const listItems = notesByDate.map((day) => day.list).map(list=>
        <Timeline.Item label={dateInFrench(list?._id)} key={list?._id}>
            <List
                className="comment-list"
                header={`${list?.length} notes`}
                itemLayout="horizontal"
                dataSource={list}
                renderItem={note => (
                    <li>
                        <Comment
                            actions={[]}
                            author={note.note.practitioner ? note.note.practitioner : "Practitioner"}
                            avatar={note.note.avatar ? note.note.avatar : 'https://joeschmoe.io/api/v1/random'}
                            content={
                                <EditableNote note={note.note}/>
                            }
                            datetime={note.note.updatedAt ? DateSince(note.note.updatedAt) : DateSince(note.note.createdAt)}
                        />
                    </li>
                )}
            />
        </Timeline.Item>
    );
    return (
        <>{listItems}</>
    );
}
