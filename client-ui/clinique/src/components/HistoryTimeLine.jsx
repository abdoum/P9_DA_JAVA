import {Timeline} from "antd";
import React, {useEffect, useRef} from "react";
import 'moment/locale/fr';
import DateList from "./DateList";


export default function HistoryTimeLine({notesByDate}) {
    const mountedRef = useRef(true)
        let days = notesByDate?.map(noteByDate=> noteByDate.list.map(singleNote=>singleNote.note));

    useEffect(() => {
            console.log(notesByDate)
            console.log(days)
            return () => {
                mountedRef.current = false
            };
        }
    );

    return (
        <Timeline mode={'left'}>
            <DateList notesByDate={notesByDate} />
                {/*<Timeline.Item label={dateInFrench()}>*/}
                {/*    <List*/}
                {/*        className="comment-list"*/}
                {/*        header={`${days?.length} notes`}*/}
                {/*        itemLayout="horizontal"*/}
                {/*        dataSource={days}*/}
                {/*        renderItem={day => (*/}
                {/*            <li>*/}
                {/*                <Comment*/}
                {/*                    actions={[]}*/}
                {/*                    author={day.practitioner ? day.practitioner : "Practitioner"}*/}
                {/*                    avatar={day.avatar ? day.avatar : 'https://joeschmoe.io/api/v1/random'}*/}
                {/*                    content={*/}
                {/*                        <EditableNote note={day}/>*/}
                {/*                    }*/}
                {/*                    datetime={day.updatedAt ? DateSince(day.updatedAt) : DateSince(day.createdAt)}*/}
                {/*                />*/}
                {/*            </li>*/}
                {/*        )}*/}
                {/*    />*/}
                {/*</Timeline.Item>*/}
            <Timeline.Item label={`Aujourd'hui`}></Timeline.Item>
        </Timeline>
    )
}