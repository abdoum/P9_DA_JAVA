import {useEffect, useState} from 'react';
import {useForm} from 'react-hook-form';
import * as Yup from 'yup';
import {yupResolver} from '@hookform/resolvers/yup';
import {patientService} from "../_services";
import {useNavigate} from "react-router-dom";
import {Form, message} from "antd";
import {useKeycloak} from "@react-keycloak/web";
import {paths} from "../utils/Paths";

export default function EditPatientForm({history, match}) {
    const {id} = match;
    const isAddMode = !id;
    const navigate = useNavigate();
    const [disableSubmit, setDisableSubmit] = useState(false);
    const [isPatientPresent, setIsPatientPresent] = useState(false);
    const {keycloak} = useKeycloak();

    function onSubmit(data) {
        setDisableSubmit(true)
        return isAddMode
            ? createUser(data)
            : updateUser(id, data);
    }

    function createUser(data) {
        return patientService.create(data, keycloak.token)
            .then(() => {
                setDisableSubmit(false)
                message.success('PatientHistory ajouté');
                history.push('.');
            })
            .catch(e => {
                message.error(e)
                setDisableSubmit(false)
            });
    }

    function updateUser(id, data) {
        return patientService.update(id, data, keycloak.token)
            .then(() => {
                setDisableSubmit(false)
                navigate(paths.list);
                message.success('PatientHistory modifié');
                // history.push('..');
            })
            .catch(e => {
                message.error(e)
                const fieldNames = ['id', 'given', 'family', 'dob', 'sex', 'address', 'phone'];
                fieldNames.forEach(fieldName => setError(fieldName, e[fieldName]));
                setDisableSubmit(false)
            });
    }

    useEffect(() => {
        if (!isAddMode) {
            // get patient and set form fields
            patientService.getById(id, keycloak.token).then(patient => {
                const fieldNames = ['id', 'given', 'family', 'dob', 'sex', 'address', 'phone'];
                setIsPatientPresent(true)
                fieldNames.forEach(fieldName => setValue(fieldName, patient[fieldName]));
            });
        }
    }, []);

    const validationSchema = Yup.object().shape({
        given: Yup.string().required('Veuillez saisir un prénom').min(2, '2 charactères minimum').max(50, '50 charactères maximum'),
        family: Yup.string().required('Veuillez saisir un nom').max(50, '50 charactères maximum'),
        sex: Yup.string().required('Veuillez sélectionner un genre').oneOf(['M', 'F'], 'Sélectionner un genre valide'),
        dob: Yup.date()
            // .max(Date.now(), "La date de naissance ne peut pas etre dans le futur")
            // .min(Date.parse("1900-01-01"), "La date de naissance ne peut pas etre avant le 01/01/1900")
            // .default(Date.now())
            .required("Veuillez renseigner une date de naissance"),
        address: Yup.string().max(150, '150 charactères maximum'),
        phone: Yup.string()
            .nullable().notRequired()
            .when('phone', {
                is: (value) => value?.length,
                then: (rule) => rule.matches(
                    '[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}',
                    'Le numéro de téléphone etre au format 100-000-0000',
                ),
            })
    },
        [
            // Add Cyclic deps here because when require itself
            ['phone', 'phone'],
        ]);

    const {
        register,
        handleSubmit,
        setValue,
        setError,
        formState
    } = useForm({
        mode: "onChange",
        resolver: yupResolver(validationSchema),
    });

    return (
        <div>
            {isPatientPresent &&
                <Form labelCol={{span: 4}}
                      wrapperCol={{span: 14}}
                      layout="vertical"
                      size='large' onFinish={handleSubmit(onSubmit)}>
                    <Form.Item label="Prénom">
                        <input autoFocus={true} type="text" className="mt-1 block w-full"
                               placeholder="Prénom"
                               {...register('given')}/>
                        <small className='text-red-500'>{formState.errors.given?.message}</small>
                    </Form.Item>
                    <label className="block">
                        <Form.Item label="Prénom">
                            <input type="text" className="mt-1 block w-full" {...register('family')}
                                   placeholder="Nom"/>
                            <small className='text-red-500'>{formState.errors.family?.message}</small>
                        </Form.Item>
                    </label>
                    <Form.Item label="Date de naissance">
                        <input type="date" className="mt-1 block w-full" {...register('dob')}/>
                        <small className='text-red-500'>{formState.errors.dob?.message}</small>
                    </Form.Item>
                    <Form.Item label="Sexe">
                        <select className="block w-full mt-1"
                                {...register('sex')}>
                            <option value=""></option>
                            <option value="M">Masculin</option>
                            <option value="F">Féminin</option>
                        </select>
                        <small className='text-red-500'>{formState.errors.sex?.message}</small>
                    </Form.Item>
                    <Form.Item label="Adresse (Optionnel)">
                        <input type="text" className="mt-1 block w-full"
                               {...register('address')}/>
                        <small className='text-red-500'>{formState.errors.address?.message}</small>
                    </Form.Item>
                        <Form.Item label="Numéro de téléphone (Optionnel)">
                        <input type="text" className="mt-1 block w-full" {...register('phone')}/>
                        {/*<Cleave type="text" className="mt-1 block w-full"*/}
                        {/*        options={{delimiters: ['-', '-', '-'],*/}
                        {/*            blocks: [3, 3, 4]}}*/}
                        {/*        {...register('phone')} />*/}
                        {/*<small className='text-red-500'>{formState.errors.phone?.message}</small>*/}
                        </Form.Item>
                    <input type='submit' disabled={!formState.isValid || disableSubmit === true} value='Mettre à jour'
                           className="cursor-pointer disabled:bg-red-200 disabled:cursor-not-allowed p-4 bg-teal-500 rounded-lg font-bold text-white mt-5 hover:bg-gray-600"/>
                </Form>
            }
        </div>
    )
}