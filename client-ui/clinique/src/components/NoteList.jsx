import {useEffect, useRef, useState} from "react";
import SkeletonNote from "./SkeletonNote";
import {BackTop, Comment, Divider, List} from "antd";
import EditableNote from "./EditableNote";
import DateSince from "./DateSince";

export default function NoteList({notes, patient}) {
    const [isLoading, setIsLoading] = useState(true);
    const mountedRef = useRef(true)

    useEffect(() => {
        if (notes) {
            setIsLoading(false);
        }
        return () => {
            mountedRef.current = false;
        }
    }, []);

    return (
        <>
            <BackTop visibilityHeight={0}/>
            <List
                className="comment-list"
                header={`${notes?.length} notes`}
                itemLayout="horizontal"
                dataSource={notes}
                renderItem={note => (
                    <>
                        <SkeletonNote isLoading={isLoading} note={note}/>
                        {!isLoading && <li>
                            <Comment
                                actions={[]}
                                author={note.practitioner ? note.practitioner : "Practitioner"}
                                avatar={note.avatar ? note.avatar : 'https://joeschmoe.io/api/v1/random'}
                                content={
                                    <EditableNote note={note} patient={patient}/>
                                }
                                datetime={note.updatedAt ? DateSince(note.updatedAt) : DateSince(note.createdAt)}
                            />
                        </li>}
                        <Divider/>
                    </>
                )}

            />
        </>
    )
}
