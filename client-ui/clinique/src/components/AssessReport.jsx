import {Component} from "react";
import {Card} from "antd";
import {assessService} from "../_services/assess.service";
import Meta from "antd/es/card/Meta";
import {withKeycloak} from "@react-keycloak/web";
import {withRouter} from "./Wrapper";

class AssessReport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            assess: {},
            loading: true,
            patient: this.props.patientId,
            token: this.props.keycloak.token
        }
    }

    getAssess = async () => {
        const response = await assessService.getByPatientId( this.props.patientId, this.props.keycloak.token);
        this.setState( {assess: response});
        this.setState({loading: false});
    }

    async componentDidMount() {
        this.getAssess();
    }

    interpretResult = (assessResult) => {
        let result = "-";
        switch (assessResult) {
            case "NONE":
                result = "Aucun risque détecté";
                break
            case "EARLY_ON_SET":
                result = "Risque d'apparition prématurée";
                break
            case "IN_DANGER":
                result = "Risque confirmé";
                break
            case "BORDERLINE":
                result = "Risque détecté";
                break
        }
        return result;
    }

    render() {
        return (
            <div className='p-20'>
                <Card style={{width: 300, marginTop: 16}} loading={this.state.loading}>
                    <Meta
                        title={`${this.state.assess?.patientFirstName} ${this.state.assess?.patientFamily}`}
                        description={`Age: ${this.state.assess?.age} an(s)`}
                    /><p>{this.interpretResult(this.state.assess?.assessResult)}</p>
                </Card>
            </div>
        )
    }
}

export default withRouter(withKeycloak(AssessReport));