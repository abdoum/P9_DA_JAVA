import {useForm} from 'react-hook-form';
import {useNavigate} from 'react-router-dom';
import * as Yup from 'yup';
import {yupResolver} from '@hookform/resolvers/yup';
import {message, PageHeader} from "antd";
import {useKeycloak} from "@react-keycloak/web";
import {patientService} from "../_services";
import {paths} from "../utils/Paths";

export default function AddPatientForm() {
    let validationSchema;
    const navigate = useNavigate();
    const {keycloak} = useKeycloak();

    validationSchema = Yup.object().shape({
        given: Yup.string().required('Veuillez saisir un prénom').min(2, '2 charactères minimum').max(50, '50 charactères maximum'),
        family: Yup.string().required('Veuillez saisir un nom').max(50, '50 charactères maximum'),
        sex: Yup.string().required('Veuillez sélectionner un genre').oneOf(['M', 'F'], 'Sélectionner un genre valide'),
        dob: Yup.date()
            // .max(Date.now(), "La date de naissance ne peut pas etre dans le futur")
            // .min(Date.parse("1900-01-01"), "La date de naissance ne peut pas etre avant le 01/01/1900")
            // .default(Date.now())
            .required("Veuillez renseigner une date de naissance"),
        address: Yup.string().max(150, '150 charactères maximum'),
        phone: Yup.string()
            .nullable().notRequired()
            .when('phone', {
                is: (value) => value?.length,
                then: (rule) => rule.matches(
                    '[1-9][0-9]{2}-[0-9]{3}-[0-9]{4}',
                    'Le numéro de téléphone etre au format 100-000-0000',
                ),
            })

    },
        [
            // Add Cyclic deps here because when require itself
            ['phone', 'phone'],
        ]);

    const {
        register,
        handleSubmit,
        setError,
        formState
    } = useForm({
        mode: "onTouched",
        resolver: yupResolver(validationSchema),
    });

    function handleError(error) {
        if (error?.data?.validationErrors === null) {
            message.error(error.data?.message)
            const fieldNames = ['given', 'family', 'dob'];
            fieldNames.forEach(fieldName => setError(fieldName, {
                type: 'manual',
                message: 'PatientHistory already exists'
            }));
        } else {
            message.error("Une erreur s'est produite lors de l'ajout du patient.");
            const fieldNames = ['id', 'given', 'family', 'dob', 'sex', 'address', 'phone'];
            fieldNames.forEach(fieldName => setError(fieldName, {
                type: 'manual',
                message:
                    error.data.validationErrors?.filter(e => e.field === fieldName).map(e => e.message)
            }));
        }
    }

    function handleSuccess(e) {
        e.target.reset();
        message.success('PatientHistory ajouté');
        navigate(paths.list)
    }

    function createPatient(data, e) {
        patientService.create(data, keycloak.token)
            .then(() => {
                handleSuccess(e);
            }).catch(error => {
            handleError(error);
        })
    }

    const onSubmit = async (data, e) => {
        createPatient(data, e);
    };

    return (
        <div>
            <PageHeader
                className="site-page-header"
                onBack={() => navigate(paths.list)}
                title="Nouveau patient"
            />
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="mt-8 max-w-md">
                    <div className="grid grid-cols-1 gap-6">
                        <label className="block">
                            <span className="text-gray-700">Prénom</span>
                            <input autoFocus={true} type="text" className="mt-1 block w-full"
                                   placeholder="Prénom"
                                   {...register('given')}/>
                            <small className='text-red-500'>{formState.errors.given?.message}</small>
                        </label>
                        <label className="block">
                            <span className="text-gray-700">Nom</span>
                            <input type="text" className="mt-1 block w-full" {...register('family')}
                                   placeholder="Nom"/>
                            <small className='text-red-500'>{formState.errors.family?.message}</small>
                        </label>
                        <label className="block">
                            <span className="text-gray-700">Date de naissance</span>
                            <input type="date" className="mt-1 block w-full" {...register('dob')}/>
                            <small className='text-red-500'>{formState.errors.dob?.message}</small>
                        </label>
                        <label className="block">
                            <span className="text-gray-700">Sex</span>
                            <select className="block w-full mt-1"
                                    {...register('sex')}>
                                <option value=""></option>
                                <option value="M">Masculin</option>
                                <option value="F">Féminin</option>
                            </select>
                            <small className='text-red-500'>{formState.errors.sex?.message}</small>
                        </label>
                        <label className="block">
                        <span className="text-gray-700">Adresse
                            <small><em> (Optionnel)</em></small>
                        </span>
                            <input type="text" className="mt-1 block w-full"
                                   {...register('address')}/>
                            <small className='text-red-500'>{formState.errors.address?.message}</small>
                        </label>
                        <div className="block">
                        <span className="text-gray-700">Numéro de téléphone
                            <small><em> (Optionnel)</em></small>
                        </span>
                            <input type="text" className="mt-1 block w-full" {...register('phone')}/>
                            <small className='text-red-500'>{formState.errors.phone?.message}</small>
                        </div>
                    </div>
                </div>
                <input type='submit' value='Ajouter' disabled={!formState.isValid}
                       className="cursor-pointer disabled:bg-red-200 disabled:cursor-not-allowed p-4 bg-teal-500 rounded-lg font-bold text-white mt-5 hover:bg-gray-600"/>
            </form>
        </div>
    );
}