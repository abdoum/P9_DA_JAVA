import {Component} from 'react';
import {Space, Table, Tag} from 'antd';
import {Link} from "react-router-dom";
import {motion} from "framer-motion";
import {DrawerContext,} from "../utils/context/DrawerProvider";
import {ProfileTwoTone, setTwoToneColor, HistoryOutlined} from "@ant-design/icons";
import {patientService} from "../_services";
import moment from "moment";
import {withRouter} from "./Wrapper";
import {withKeycloak} from "@react-keycloak/web";
import {paths} from "../utils/Paths";

class PatientTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            patients: [],
            patient: {},
            columns: [],
            loading: true
        }
    }

    static contextType = DrawerContext

    getPatients = async () => {
        setTwoToneColor('rgb(249 115 22)');
        const data = await patientService.getAll(this.props.keycloak.token)
        const foundPatients = data.map((singlePatient) => (
            {...singlePatient, key: singlePatient.id}
        ));
        this.setState({columns: this.definedColumns});
        this.setState({patients: foundPatients})
        this.setState({loading: false})
    }

    async componentDidMount() {
        this.getPatients();
    }

    definedColumns = [
        {
            title: 'Nom',
            dataIndex: 'given',
            key: 'given',
            render: (text, record) => <motion.button
                whileHover={{scale: 1.1}}
                whileTap={{scale: 0.9}}>
                <Link to={`/patients/${record.id}`} state={record}>
                    {text.substr(0, 1).toUpperCase()}{text.substr(1, text.length)} {record.family.toUpperCase()}</Link>
            </motion.button>,
        },
        {
            title: 'Age',
            dataIndex: 'dob',
            key: 'dob',
            render: (dob) => (
                <span>{moment().diff(dob, 'years')}</span>
            ),
        },
        {
            title: 'Addresse',
            dataIndex: 'address',
            key: 'address',
        },
        {
            title: 'Phone',
            dataIndex: 'phone',
            key: 'phone',
        },
        {
            title: 'Sexe',
            key: 'sex',
            dataIndex: 'sex',
            sorter: (a, b) => a.sex !== b.sex,
            render: sex => (
                <>
                    <Tag color={sex === 'M' ? 'geekblue' : 'volcano'} key={sex}>
                        {sex === 'M' ? 'Masculin' : 'Féminin'}
                    </Tag>
                </>
            ),
        },
        {
            title: 'Rapport',
            key: 'action',
            render: (record) => (
                <Space size="middle">
                    <span onClick={() => {
                        this.setState({patient: record});
                    }}><Link to={`/${paths.assess}/${record.id}?patientFamily=${record.family}`} state={record}>
                        <ProfileTwoTone/>
                        </Link> </span>
                </Space>
            ),
        },
        {
            title: 'Détails',
            key: 'action',
            render: (record) => (
                <Space size="middle">
                    <span onClick={() => {
                        this.setState({patient: record});
                    }}>
                    <Link to={`/${paths.history}/${record.id}`} state={record}><HistoryOutlined/> </Link>
                    </span>
                </Space>
            ),
        },
    ];

    render() {
        return (
            <div>
                <Table loading={this.state.loading} columns={this.state.columns} dataSource={this.state.patients}/>
            </div>
        );
    }
}

export default withRouter(withKeycloak(PatientTable));