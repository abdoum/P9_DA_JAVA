import {Avatar, List, Skeleton} from "antd";

export default function SkeletonNote({isLoading, note}) {
    return (
        <>
            {isLoading && <Skeleton loading={isLoading} active avatar>
                <List.Item.Meta
                    avatar={<Avatar src={note.avatar}/>}
                    title={<a href={note.href}>{note.title}</a>}
                    description={note.content}
                    rows={1}
                />
                {note.content}
            </Skeleton>}
        </>
    )
}