import {Tooltip} from "antd";
import moment from "moment";
import 'moment/locale/fr';

export default function DateSince(date) {
    return (
        <Tooltip title={moment(date).subtract(1, 'days').format('DD MMM YYYY à HH:mm')}>
            <span>{moment(date).locale('fr').subtract(1, 'days').fromNow()}</span>
        </Tooltip>
    )
}