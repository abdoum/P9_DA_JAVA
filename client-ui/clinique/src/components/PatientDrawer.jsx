import {Descriptions, Drawer, Tooltip} from 'antd';
import {DrawerContext} from "../utils/context/DrawerProvider";
import  {useContext} from "react";
import {Link} from "react-router-dom";
import {EditTwoTone} from "@ant-design/icons";

export default function PatientDrawer({patient}) {
    const {isVisible, toggleDrawer} = useContext(DrawerContext);
    const givenPatient = patient;

    const onClose = () => {
        toggleDrawer();
    };

    return (
        <>
            <Drawer
                width={640}
                placement="right"
                closable={false}
                onClose={onClose}
                visible={isVisible}
            >
                <h2 className="site-description-item-profile-p" style={{marginBottom: 24}}>
                    PatientHistory&apos;s Profile <Tooltip title="modifier">
                    <Link to={`/edit/${givenPatient.id}`}>
                        <EditTwoTone/></Link>
                </Tooltip>
                </h2>
                <Descriptions title="Informations PatientHistory" bordered>
                    <Descriptions.Item
                        label="First Name" span={4}> {givenPatient.given?.toUpperCase()}</Descriptions.Item>
                    <Descriptions.Item label="Family Name" span={4}> {givenPatient.family?.toUpperCase()} </Descriptions.Item>
                    <Descriptions.Item label="Birth Date" span={2}> {givenPatient.dob} </Descriptions.Item>
                    <Descriptions.Item label="Sex"> {givenPatient.sex} </Descriptions.Item>
                    <Descriptions.Item label="Address" span={4}> {givenPatient.address} </Descriptions.Item>
                    <Descriptions.Item label="Phone"> {givenPatient.phone} </Descriptions.Item>
                </Descriptions>
            </Drawer>
        </>
    );
}
