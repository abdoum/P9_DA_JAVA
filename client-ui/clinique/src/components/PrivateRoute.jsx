import {useKeycloak} from '@react-keycloak/web'

function PrivateRoute({children}) {
    const {keycloak} = useKeycloak()
    return (
        keycloak.authenticated ? children : keycloak.login()
    )
}

export default PrivateRoute