import {Avatar, Comment, message, Space, Spin} from 'antd';
import {useEffect, useRef, useState} from "react";
import {noteService, patientService} from "../_services";
import {Link, useParams} from "react-router-dom";
import Editor from "./Editor";
import NoteList from "./NoteList";
import {useKeycloak} from "@react-keycloak/web";
import {paths} from "../utils/Paths";
import {ProfileTwoTone} from "@ant-design/icons";

export default function NoteEditor() {

    const [submitting, setSubmitting] = useState(false);
    const initialFieldValue = 'Patient states that ';
    const [value, setValue] = useState(initialFieldValue);
    const [existingNotes, setNotes] = useState([]);
    const [loading, setLoading] = useState(true);
    const mountedRef = useRef(true)
    const [patient, setPatient] = useState({});
    const {keycloak} = useKeycloak();
    const params = useParams();

    const handleSubmit = () => {
        if (!value) {
            return;
        }
        setSubmitting(true);
        const note = {
            patId: params.id,
            patientFamily: patient.family,
            content: value,
        }
        noteService.create(note, keycloak.token)
            .then((data) => {
                setSubmitting(false);
                setValue(initialFieldValue)
                setNotes([...existingNotes, data]);
                message.success("Note ajoutée")
            }).catch(error => {
            message.error(error)
        });
    };

    const handleChange = e => {
        setValue(e.target.value)
    };

    function getPatient() {
        patientService.getById(params.id, keycloak.token)
            .then((data) => {
                setPatient(data);
            }).catch(error => {
            message.error(error)
        });
    }

    useEffect(() => {
        noteService.getByPatientId(params.id, keycloak.token)
            .then((data) => {
                setNotes(data);
                setLoading(false)
                getPatient();
            }).catch(error => {
            message.error(error)
        });
        return () => {
            mountedRef.current = false
        };
    }, []);

    useEffect(() => {
        return () => {
            mountedRef.current = false;
        };
    }, [existingNotes]);

    return (
        <>
            {loading && <div className="m-20 mb-20 pl-3 pr-5 text-center backdrop-blur">
                <Spin/>
            </div>}
            <Space size="middle">
                    <span onClick={() => {
                        this.setState({patient: patient});
                    }}><Link to={`/${paths.assess}/${patient.id}?patientFamily=${patient.family}`} state={patient}>
                       <ProfileTwoTone/>
                        </Link> </span>
            </Space>
            {!loading && <NoteList notes={existingNotes} patient={patient}/>}
            <Comment
                avatar={<Avatar src="https://joeschmoe.io/api/v1/random" alt="Han Solo"/>}
                content={
                    <Editor
                        onChange={handleChange}
                        onSubmit={handleSubmit}
                        submitting={submitting}
                        value={value}
                    />
                }
            />
        </>
    );
}
