import {Menu} from "antd";
import {useKeycloak} from "@react-keycloak/web";
import {Link} from "react-router-dom";
import {UserAddOutlined} from "@ant-design/icons";

import {paths} from "../utils/Paths";

export default function Nav(props) {

    const {keycloak} = useKeycloak()

    const handleLogInOut = () => {
        if (keycloak.authenticated) {
            props.history?.push(paths.landing)
            keycloak.logout();
        } else {
            keycloak.login()
        }
    }

    const checkAuthenticated = () => {
        if (!keycloak?.authenticated) {
            handleLogInOut()
        }
    }

    const getUsername = () => {
        return keycloak?.authenticated && keycloak?.tokenParsed && keycloak?.tokenParsed.given_name
    }

    const getLogInOutText = () => {
        return keycloak?.authenticated ? `Déconnexion` : "Se connecter"
    }

    return (
        <Menu
            style={{height: '100%'}}
            mode="inline"
        >
                <Menu.Item key="2">
                    {keycloak?.authenticated && <div className="pl-4 text-amber-900">{`Bonjour, ${getUsername()}`}
                    </div>}
                </Menu.Item>
            <Menu.Item key="1">
                {keycloak?.authenticated &&
                <Link to={`/add`} onClick={checkAuthenticated}
                      className='px-4 py-4 text-orange-500 hover:text-orange-900'>
                    <UserAddOutlined style={{fontSize: '22px'}}/>
                </Link>}
            </Menu.Item>
            <Menu.Item key="3">
                <Link className='px-4 py-4 text-gray-700 hover:text-gray-900'
                      exact to={paths.list} onClick={handleLogInOut}>{getLogInOutText()}</Link>
            </Menu.Item>
        </Menu>

    )
}
