import {useLocation, useNavigate, useParams} from "react-router-dom";

export const withRouter = (Component) => {
    const Wrapper = (props) => {
        const navigate = useNavigate();
        const {id, token, family} = useParams();
        const state = useLocation().state;
        return (
            <Component
                navigate={navigate}
                state={state}
                patientId={id}
                patientFamily={family}
                token={token}
                {...props}
            />
        );
    };
    return Wrapper;
};