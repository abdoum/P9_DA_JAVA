import {noteService} from "../_services";
import Paragraph from "antd/es/typography/Paragraph";
import {useEffect, useRef, useState} from "react";
import {message} from "antd";
import {useKeycloak} from "@react-keycloak/web";

export default function EditableNote({note, patient}) {
    const mountedRef = useRef(true)
    const [existingNote, setNote] = useState(null);
    const {keycloak} = useKeycloak();

    function handleError(errors) {
        message.error(errors)
        errors.forEach(e => {
            message.error(e.message)
        });
    }

    function handleSuccess(data) {
        setNote(data);
        message.success("Note modifiée")
    }

    const updateNote = async (e) => {
        note.content = e
        note.patientFamily = patient.family;
        noteService.update(note.id, note, keycloak.token).then(data => {
            handleSuccess(data);
        }).catch(errors => {
            handleError(errors);
        })
    };

    useEffect(() => {
        setNote(note)
        return () => {
            mountedRef.current = false;
        }
    }, [])

    useEffect(() => {
        return () => {
            mountedRef.current = false;
        }
    }, [existingNote])

    return (<Paragraph editable={{tooltip: false, onChange: updateNote}}>
        {existingNote?.content}
    </Paragraph>)
}