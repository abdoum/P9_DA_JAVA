export const paths = {
    landing: '/',
    list: '/patients',
    add: 'add',
    edit: 'edit',
    patient: 'patients',
    history: 'history',
    assess: 'reports',
};