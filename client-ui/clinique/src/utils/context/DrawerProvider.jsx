import {createContext, useState} from "react";

export const DrawerContext = createContext()

export const DrawerProvider = ({ children }) => {
    const [isVisible, setVisible] = useState(false)
    const toggleDrawer = () => {
        setVisible(!isVisible)
    }
    return (
        <DrawerContext.Provider value={{ isVisible, toggleDrawer }}>
            {children}
        </DrawerContext.Provider>
    )
}