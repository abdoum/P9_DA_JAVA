const CracoLessPlugin = require('craco-less');

module.exports = {
    plugins: [
        {
            plugin: CracoLessPlugin,
            options: {
                lessLoaderOptions: {
                    lessOptions: {
                        modifyVars: {
                            '@primary-color': 'rgb(249 115 22)',
                            '@link-color': 'rgb(249 115 22)',
                            '@heading-color':'rgb(249 115 22)',
                        },
                        javascriptEnabled: true,
                    },
                },
            },
        },
    ],
};