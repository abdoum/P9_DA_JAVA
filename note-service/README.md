![](https://img.shields.io/badge/Java-ED8B00?style=for-the-badge&logo=java&logoColor=white)
![](https://img.shields.io/badge/Spring-6DB33F?style=for-the-badge&logo=spring&logoColor=white)
![](/gitlab/last-commit/gitlab-org/gitlab-development-kit)
![](/maven/v/metadata-url/repo1.maven.org/maven2/com/google/code/gson/gson/maven-metadata.xml)

# Getting Started

### Reference Documentation

[[_TOC_]]

### Description

The patient micro-service handles all patient related operations.

![](/Users/dr/Documents/Formation/openclassrooms/projets/P9_DA_JAVA/code/clinique/patient/src/main/resources/Template+de+spécifications+API+REST+P9.pdf)

![](/Users/dr/Documents/Formation/openclassrooms/projets/P9_DA_JAVA/code/clinique/patient/src/main/resources/architecture.jpg)

### Requirements

----

- Unix or Windows OS
- [Docker engine](https://docs.docker.com/get-started/)
- Docker compose

[Spécificités de l'api rest](https://docs.google.com/document/d/e/2PACX-1vTf7aRMrVdc9C2uLRpyPjF3LZWjyq_yM_Ul_jFdSDynmdwiNxjbXGAWmLVJ61nDTzPalA7SoLCqrmYI/pub)

### Rétrospective sprint 2

---
Ce qui s’est bien passé :

* Écriture des tests unitaires
* Implémentation de la base de donnée NoSql mongodb
* Implémentation du microservice notes
* Implémentation du spring cloud gateway

Ce qui aurait pu aller mieux :

* Mise en place de swagger-ui via spring cloud gateway (erreurs cors lors des appels vers les micro-services)
* Organisation du repository git (les commits ne sont pas limités aux taches demandées)

Ce que j’aimerais faire différemment :

* Limiter les tâches accomplies å celles mentionnées sur l’issue
* Automatiser la génération de la documentation de l’api rest
* Faire des commits propres et concis