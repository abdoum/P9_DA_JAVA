package com.abernathe.note.bean;

/**
 * Sex types
 */
public enum GenreBean {
    M,
    F
}