package com.abernathe.note.proxy;

import com.abernathe.note.bean.PatientBean;
import com.abernathe.note.exception.EntityNotFoundException;
import org.hibernate.annotations.Proxy;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.util.List;
import java.util.Optional;

@Proxy
@FeignClient(name = "patient-service")
public interface IPatientProxy {

    /**
     * Find all patients.
     *
     * @return list of patients
     */
    @GetMapping(value = "/patients/list", produces = {MediaType.APPLICATION_JSON_VALUE})
    List<PatientBean> findAll();

    /**
     * Updates a patient.
     *
     * @param patient the patient
     * @param id      the patient's id
     * @return the updated patient
     * @throws EntityNotFoundException if the patient does not exist
     */
    @PutMapping(value = "/patients/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    PatientBean update(@Valid @RequestBody PatientBean patient, @PathVariable("id") @Positive Long id) throws
            EntityNotFoundException;

    /**
     * Find a patient by its id.
     *
     * @param patientId the patient id
     * @return ResponseEntity of an optional patient (the found patient)
     */
    @GetMapping(value = "/patients/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Optional<PatientBean>> findById(@PathVariable("id") @Positive Long patientId);

    /**
     * Find by family name.
     *
     * @param patientFamilyName the patient family name
     * @return the optional patient
     */
    @GetMapping(value = "/patients", produces = MediaType.APPLICATION_JSON_VALUE)
    Optional<List<PatientBean>> findByFamilyName(@RequestParam("family") @NotBlank String patientFamilyName);
}