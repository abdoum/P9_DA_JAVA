package com.abernathe.note.repository;

import com.abernathe.note.model.Note;
import com.mongodb.BasicDBObject;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Component;

import java.util.List;

import static org.springframework.data.domain.Sort.Direction.ASC;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@Component
public class NoteCustomDaoImpl implements INoteCustomDao {

    /**
     * Mongo template.
     */
    private final MongoTemplate mongoTemplate;

    public NoteCustomDaoImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    /**
     * Find all notes by patient id ordering by creation date asc list and grouping by date.
     *
     * @param patientId the patient id
     * @return the list of dates containing notes
     */
    @Override public List<Object> findAllByPatIdOrderByCreatedAtAsc(Long patientId) {

        Aggregation aggregation = newAggregation(
                match(Criteria.where("patId").is(patientId)),
                project("createdAt", "content", "patId")
                        .andExpression("createdAt").dateAsFormattedString("%Y-%m-%d").as("date"),
                group("date")
                        .push(new BasicDBObject
                                ("note", "$$ROOT")).as("list"),
                sort(ASC, "date"));
        AggregationResults<Object> groupResults = mongoTemplate.aggregate(
                aggregation, Note.class, Object.class);

        return groupResults.getMappedResults();
    }
}