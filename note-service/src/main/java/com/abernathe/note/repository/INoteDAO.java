package com.abernathe.note.repository;

import com.abernathe.note.model.Note;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface INoteDAO extends MongoRepository<Note, String> {

    /**
     * Find all notes by patient id order by creation date ascendant.
     *
     * @param patId the patient id
     * @return the list of notes
     */
    List<Note> findAllByPatIdOrderByCreatedAtAsc(Long patId);

    /**
     * Find all notes by patient's family name order by creation date ascendant.
     *
     * @param patientFamily the patient family name
     * @return the notes list
     */
    List<Note> findAllByPatientFamilyOrderByCreatedAtAsc(String patientFamily);
}