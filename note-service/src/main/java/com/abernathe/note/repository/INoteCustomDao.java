package com.abernathe.note.repository;

import java.util.List;

public interface INoteCustomDao {

    /**
     * Find notes by patient id by creation date in ascendant order.
     *
     * @param patientId the patient id
     * @return the list of notes
     */
    List<Object> findAllByPatIdOrderByCreatedAtAsc(Long patientId);
}