package com.abernathe.note.service;

import com.abernathe.note.exception.EntityNotFoundException;
import com.abernathe.note.model.Note;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Optional;


@Validated
public interface INoteService {

    /**
     * Save note.
     *
     * @param note the note
     * @return the note
     * @throws EntityNotFoundException the entity not found exception
     */
    Note save(@Valid Note note) throws EntityNotFoundException;

    /**
     * Update note.
     *
     * @param note the note
     * @return the note
     * @throws EntityNotFoundException the entity not found exception
     */
    Note update(@Valid Note note) throws EntityNotFoundException;

    /**
     * Find by id optional.
     *
     * @param noteId the note id
     * @return the optional
     */
    Optional<Note> findById(@NotBlank String noteId);

    /**
     * Gets patient history by id.
     *
     * @param patientId the patient id
     * @return the patient history by id
     */
    List<Note> getPatientHistoryById(@Positive Long patientId);

    /**
     * Gets patient history by family.
     *
     * @param patientFamily the patient family
     * @return the patient history by family
     */
    List<Note> getPatientHistoryByFamily(@Size(min = 2, max = 50) String patientFamily);

    /**
     * Gets patient history grouped by date.
     *
     * @param patientId the patient id
     * @return the patient history grouped by date
     */
    List<Object> getPatientHistoryGroupedByDate(@Positive Long patientId);
}