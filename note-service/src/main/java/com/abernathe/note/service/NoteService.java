package com.abernathe.note.service;

import com.abernathe.note.bean.PatientBean;
import com.abernathe.note.exception.EntityNotFoundException;
import com.abernathe.note.model.Note;
import com.abernathe.note.proxy.IPatientProxy;
import com.abernathe.note.repository.INoteCustomDao;
import com.abernathe.note.repository.INoteDAO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


@Service
@Slf4j
public class NoteService implements INoteService {

    private final INoteDAO iNoteDAO;

    private final INoteCustomDao iNoteCustomDao;

    private final IPatientProxy iPatientProxy;

    /**
     * Default constructor. Creates a instance of NoteService
     *
     * @param iNoteDAO       the note repository interface
     * @param iNoteCustomDao a custom repository interface
     * @param iPatientProxy  the patient proxy interface used to call the patient microservice
     */
    public NoteService(INoteDAO iNoteDAO, INoteCustomDao iNoteCustomDao, IPatientProxy iPatientProxy) {
        this.iNoteDAO       = iNoteDAO;
        this.iNoteCustomDao = iNoteCustomDao;
        this.iPatientProxy  = iPatientProxy;
    }

    /**
     * Saves a note to Database
     *
     * @param note the note to be saved
     * @return the saved note
     */
    @Override public Note save(Note note) throws EntityNotFoundException {
        Note noteToPersist = new Note();
        PatientBean patient =
                Objects.requireNonNull(iPatientProxy.findById(note.getPatId()).getBody())
                       .orElseThrow(() -> new EntityNotFoundException(
                               String.format("patient with id %s could not be found", note.getPatId())));
        noteToPersist.setContent(note.getContent());
        noteToPersist.setCreatedAt(LocalDateTime.now());
        noteToPersist.setPatId(note.getPatId());
        noteToPersist.setPatientFamily(patient.getFamily());
        return iNoteDAO.save(noteToPersist);
    }

    /**
     * Updates a note in the database
     *
     * @param note the note to be updated
     * @return the updated note
     * @throws EntityNotFoundException if the note could not be found with the given id
     */
    @Override public Note update(Note note) throws EntityNotFoundException {
        String noteId = note.getId();
        Optional<Note> existingNote = iNoteDAO.findById(noteId);
        if (existingNote.isEmpty()) {
            throw new EntityNotFoundException(String.format("Note with id %s could not be found",
                    noteId));
        }
        log.debug("Received note: {}", note);
        PatientBean patient =
                Objects.requireNonNull(iPatientProxy.findById(note.getPatId()).getBody())
                       .orElseThrow(() -> new EntityNotFoundException(
                               String.format("patient with id %s could not be found", note.getPatId())));
        Note noteToPersist = new Note();
        noteToPersist.setId(note.getId());
        noteToPersist.setContent(note.getContent());
        noteToPersist.setPatId(note.getPatId());
        noteToPersist.setPatientFamily(patient.getFamily());
        noteToPersist.setCreatedAt(note.getCreatedAt());
        noteToPersist.setUpdatedAt(LocalDateTime.now());
        log.debug("Note to persist: {}", noteToPersist);
        return iNoteDAO.save(noteToPersist);
    }

    /**
     * Finds a note by its id
     *
     * @param noteId the id of the note to be found
     * @return the found note or null if none found
     */
    @Override public Optional<Note> findById(String noteId) {
        return iNoteDAO.findById(noteId);
    }

    /**
     * Finds all notes for a given patient. The Notes are ordered by creation date ascendant
     *
     * @param patientId the patient id
     * @return a list of notes or an empty list if the patient does not have notes
     */
    @Override public List<Note> getPatientHistoryById(Long patientId) {
        return iNoteDAO.findAllByPatIdOrderByCreatedAtAsc(patientId);
    }

    /**
     * Gets a patient's history by family name
     *
     * @param patientFamily the patient family name
     * @return a list of notes for that patient or an empty list if the patient has no notes
     */
    @Override public List<Note> getPatientHistoryByFamily(String patientFamily) {
        return iNoteDAO.findAllByPatientFamilyOrderByCreatedAtAsc(patientFamily);
    }

    /**
     * Gets a list of patient's notes grouped by date
     *
     * @param patientId the patient's id
     * @return a list of dates containing a list of notes
     */
    @Override
    public List<Object> getPatientHistoryGroupedByDate(Long patientId) {
        return findAllByPatIDGroupByDate(patientId);
    }

    /**
     * Invokes a mongodb template method to get a list of patient's notes grouped by date
     *
     * @param patientId the patient's id
     * @return a list of dates containing a list of notes
     * @see #iNoteCustomDao
     */
    private List<Object> findAllByPatIDGroupByDate(Long patientId) {
        List<Object> allByPatIdOrderByCreatedAtAsc = iNoteCustomDao.findAllByPatIdOrderByCreatedAtAsc(patientId);
        log.debug("Found notes: {}", allByPatIdOrderByCreatedAtAsc);
        return allByPatIdOrderByCreatedAtAsc;
    }
}