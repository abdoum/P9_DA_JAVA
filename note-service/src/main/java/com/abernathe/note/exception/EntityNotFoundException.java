package com.abernathe.note.exception;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EntityNotFoundException extends Exception {

    private static final Logger log = LoggerFactory.getLogger(EntityNotFoundException.class);

    /**
     * Constructs a new exception with {@code null} as its detail message.
     * The cause is not initialized, and may subsequently be initialized by a
     * call to {@link #initCause}.
     */
    public EntityNotFoundException() {
        super();
    }

    /**
     * Constructs a new exception with the specified detail message.  The
     * cause is not initialized, and may subsequently be initialized by
     * a call to {@link #initCause}.
     *
     * @param message the detail message. The detail message is saved for
     *                later retrieval by the {@link #getMessage()} method.
     */
    public EntityNotFoundException(String message) {
        super(message);
        log.error(message);
    }
}