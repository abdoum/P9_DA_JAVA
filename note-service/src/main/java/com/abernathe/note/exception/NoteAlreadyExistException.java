package com.abernathe.note.exception;

public class NoteAlreadyExistException extends Exception {

    private static final long serialVersionUID = 1L;

    public NoteAlreadyExistException() {
        super();
    }

    public NoteAlreadyExistException(String errorMessage) {
        super(errorMessage);
    }
}