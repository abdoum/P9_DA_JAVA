package com.abernathe.note.config.dbmigrations;

import com.abernathe.note.model.Note;
import com.abernathe.note.repository.INoteDAO;
import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import org.springframework.context.annotation.Profile;

import java.time.LocalDateTime;

@Profile("development")
@ChangeLog(order = "001")
public class DatabaseInitChangeLog {

    public static final String FAMILY = "TestEarlyOnset";

    public static final String BORDERLINE = "TestBorderline";

    public static final String IN_DANGER = "TestInDanger";

    @ChangeSet(order = "001", id = "init_notes", author = "abdoum")
    public void initNotes(INoteDAO iNoteDAO) {
        iNoteDAO.save(new Note(1L, "TestNone",
                "Patient states that they are 'feeling terrific' Weight at or below recommended level",
                LocalDateTime.now(),
                null
        ));
        iNoteDAO.save(new Note(2L, BORDERLINE, "Patient states that they are feeling a great deal of stress at " +
                                               "work Patient also complains that their hearing seems Abnormal as of late"
                , LocalDateTime.now(),
                null));
        iNoteDAO.save(new Note(2L, BORDERLINE, "Patient states that they have had a Reaction to medication " +
                                               "within last 3 months Patient also complains that their hearing continues to be problematic",
                LocalDateTime.now(),
                null));
        iNoteDAO.save(new Note(3L, IN_DANGER, "Patient states that they are short term Smoker ",
                LocalDateTime.now()
                , null));
        iNoteDAO.save(new Note(3L, IN_DANGER,
                "Patient states that they quit within last year Patient also complains that of " +
                "Abnormal breathing spells Lab reports Cholesterol LDL high",
                LocalDateTime.now()
                , null));
        iNoteDAO.save(new Note(4L, FAMILY,
                "Patient states that walking up stairs has become difficult Patient also complains that they are having shortness of breath Lab results indicate Antibodies present elevated Reaction to medication",
                LocalDateTime.now()
                , null));
        iNoteDAO.save(
                new Note(4L, FAMILY, "Patient states that they are experiencing back pain when seated for a long time",
                        LocalDateTime.now()
                        , null));
        iNoteDAO.save(new Note(4L, FAMILY, "Patient states that they are a short term Smoker Hemoglobin A1C above " +
                                           "recommended level",
                LocalDateTime.now()
                , null));
        iNoteDAO.save(new Note(4L, FAMILY, "Patient states that Body Height, Body Weight, Cholesterol, Dizziness and " +
                                           "Reaction",
                LocalDateTime.now()
                , null));
    }
}