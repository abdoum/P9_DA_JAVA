package com.abernathe.note.controller;

import com.abernathe.note.exception.EntityNotFoundException;
import com.abernathe.note.model.Note;
import com.abernathe.note.service.INoteService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;

@RestController
@Validated
@CrossOrigin(allowedHeaders = "*", origins = "*")
@RequestMapping("notes")
@Slf4j
public class NoteController {

    private final INoteService iNoteService;

    public NoteController(INoteService iNoteService) {
        this.iNoteService = iNoteService;
    }

    /**
     * Saves a note. Returns the saved note on success.
     *
     * @param note the note to be saved
     * @return the saved note
     */
    @Operation(summary = "Adds a note.",
               description = "Adds a note. Returns the updated note on success. Displays validations errors otherwise.")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "The updated note."),
                    @ApiResponse(responseCode = "400", description = "If the submitted note contains validation " +
                                                                     "errors.", content = @Content)
            }
    )
    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Note addNote(@Valid @RequestBody Note note) throws EntityNotFoundException {
        log.info("Add note request: {}", note);
        Note savedNote = iNoteService.save(note);
        log.info("Saved note: {}", savedNote);
        return savedNote;
    }

    /**
     * Updates a note. Returns the updated note on success.
     *
     * @param note the updated note
     * @return the updated note
     */
    //region swagger documentation
    @Operation(summary = "Updates a note.",
               description = "Updates a note. Returns the updated note on success. Displays validations errors otherwise.")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "The updated note.")
            }
    )
    //endregion
    @PutMapping(value = "/{noteId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Note updateNote(@Valid @RequestBody Note note,
                           @PathVariable("noteId") @NotEmpty @Pattern(regexp = "[[a-f]\\d]{24}", message =
                                   "Please provide a valid id") String noteId) throws
            EntityNotFoundException {
        log.info("note: {}", note);
        log.info("Note id: {}", noteId);
        note.setId(noteId);
        Note updatedNote = iNoteService.update(note);
        log.info("Updated note: {}", updatedNote);
        return updatedNote;
    }

    /**
     * Find all notes for a patient. Returns the found notes on success, or an empty list if no notes were found.
     * The notes are ordered by creation date.
     *
     * @param patientId the patient id
     * @return Note  the found patient or an empty list if no notes were found
     */
    @Operation(summary = "Finds notes for a patient id.",
               description = "Finds notes for a patient id. Returns the found notes on success, or an empty list otherwise.")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "The list of notes or an empty list if no notes " +
                                                                     "were found")
            }
    )
    @Validated
    @GetMapping(value = "/{patientId}", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public List<Note> getHistoryByPatientId(@PathVariable("patientId") @NotNull @Positive Long patientId) {
        log.info("patient id: {}", patientId);
        List<Note> noteList = iNoteService.getPatientHistoryById(patientId);
        log.info("note list: {}", noteList);
        return noteList;
    }

    /**
     * Find all notes for a patient. Returns the found notes on success, or an empty list if no notes were found.
     * The notes are ordered by creation date.
     *
     * @param patientFamily the patient family name
     * @return Note  the found patient or an empty list if no notes were found
     */
    @Operation(summary = "Finds notes for a patient by family name.",
               description = "Finds notes for a patient's family name. Returns the found notes on success, or an " +
                             "empty list otherwise.")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "The list of notes or an empty list if no notes " +
                                                                     "were found")
            }
    )
    @Validated
    @GetMapping(value = "", produces = {
            MediaType.APPLICATION_JSON_VALUE,
            MediaType.APPLICATION_XML_VALUE
    })
    public List<Note> getHistoryByPatientFamily(
            @RequestParam("family") @NotBlank @Size(min = 2, max = 50) String patientFamily) {
        log.info("Patient family name: {}", patientFamily);
        List<Note> noteList = iNoteService.getPatientHistoryByFamily(patientFamily);
        log.info("Note list: {}", noteList);
        return noteList;
    }
}