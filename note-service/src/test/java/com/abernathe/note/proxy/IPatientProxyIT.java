package com.abernathe.note.proxy;

import com.abernathe.note.bean.PatientBean;
import com.abernathe.note.model.Note;
import com.abernathe.note.repository.INoteDAO;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class IPatientProxyIT {

    @Autowired
    IPatientProxy iPatientProxy;

    @Autowired
    INoteDAO iNoteDAO;

    @ParameterizedTest(name = "[{index}] {arguments}")
    @CsvFileSource(resources = "/db/data/notes.csv", useHeadersInDisplayName = true)
    void testWithCsvFileSourceAndHeaders(String family, String content) {
        assertNotNull(family);
        assertNotNull(content);
        Optional<List<PatientBean>> patientBean = iPatientProxy.findByFamilyName(family);
        patientBean.ifPresent(patient -> {
            Note note = new Note(
                    patientBean.get().get(0).getId(),
                    content
            );
            iNoteDAO.save(note);
        });
    }

}