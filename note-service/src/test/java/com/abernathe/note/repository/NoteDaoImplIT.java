package com.abernathe.note.repository;

import com.abernathe.note.model.Note;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class NoteDaoImplIT {

    @Autowired
    NoteCustomDaoImpl noteCustomDaoImpl;

    @Autowired
    INoteDAO iNoteDAO;

    @Autowired
    MongoTemplate mongoTemplate;

    @BeforeAll
    static void beforeAll(@Autowired INoteDAO iNoteDAO) {
        iNoteDAO.deleteAll();
    }

    @Order(2)
    @Test
    void test_findAllByPatIdOrderByCreatedAtAsc_shouldReturnAListOfDatesContainingNotes_forAValidPatientId() {
        List<Object> noteList = noteCustomDaoImpl.findAllByPatIdOrderByCreatedAtAsc(4L);
        System.out.println(noteList.toString());
        assertThat(noteList).isNotEmpty();
    }

    @Order(1)
    @ParameterizedTest
    @CsvSource({
            "1,TestNone, 'Patient states that they are ''feeling terrific'' Weight at or below recommended level'",
            "2,TestBorderline, 'Patient states that they are feeling a great deal of stress at work Patient also " +
            "complains that their hearing seems Abnormal as of late'",
            "2, TestBorderline, 'Patient states that they have had a Reaction to medication within last 3 months " +
            "Patient also complains that their hearing continues to be problematic'",
            "3, TestInDanger, 'Patient states that they are short term Smoker '",
            "3, TestInDanger, 'Patient states that they quit within last year Patient also complains that of Abnormal breathing spells Lab reports Cholesterol LDL high'",
            "4, TestEarlyOnset, 'Patient states that walking up stairs has become difficult Patient also complains that they are having shortness of breath Lab results indicate Antibodies present elevated Reaction to medication'",
            "4, TestEarlyOnset, 'Patient states that they are experiencing back pain when seated for a long time'",
            "4, TestEarlyOnset, 'Patient states that they are a short term Smoker Hemoglobin A1C above recommended level'",
            "4, TestEarlyOnset, 'Patient states that Body Height, Body Weight, Cholesterol, Dizziness and Reaction'",
    })
    void testSave_shouldSaveTheNoteToDatabase_forAValidNote(
            ArgumentsAccessor arguments) {
        Note note = new Note(
                arguments.getLong(0),
                arguments.getString(1),
                arguments.getString(2),
                LocalDateTime.now(),
                null
        );
        Note savedNote = iNoteDAO.save(note);
        assertNotNull(savedNote);
        assertEquals(arguments.getString(1), iNoteDAO.findById(savedNote.getId()).get().getPatientFamily());
    }
}