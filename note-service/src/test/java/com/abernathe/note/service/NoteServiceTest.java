package com.abernathe.note.service;

import com.abernathe.note.bean.GenreBean;
import com.abernathe.note.bean.PatientBean;
import com.abernathe.note.exception.EntityNotFoundException;
import com.abernathe.note.model.Note;
import com.abernathe.note.proxy.IPatientProxy;
import com.abernathe.note.repository.INoteCustomDao;
import com.abernathe.note.repository.INoteDAO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
class NoteServiceTest {

    @MockBean
    INoteCustomDao iNoteCustomDao;

    @MockBean
    INoteDAO iNoteDAO;

    @Autowired
    INoteService iNoteService;

    @MockBean
    IPatientProxy iPatientProxy;

    @Test
    void testSave_shouldReturnTheSavedNote_forAValidNote() throws EntityNotFoundException {
        // Arrange
        Note note = new Note(1L, "TestNone", "Patient states that they are short term Smoker",
                LocalDateTime.of(2021, 3, 23, 6, 54),
                null);
        Note expectedNote = new Note(1L, "TestNone", "Patient states that they are short term Smoker",
                LocalDateTime.of(2021, 3, 23, 6, 54),
                null);
        PatientBean patient = new PatientBean("which", "woman", LocalDate.of(2000, 02, 22), GenreBean.F, "2332-342-867",
                "ul nedt St.");
        expectedNote.setId("B9we");
        Optional<PatientBean> patientOptional = Optional.of(patient);
        ResponseEntity responseEntity = new ResponseEntity(patientOptional, HttpStatus.OK);
        when(iPatientProxy.findById(anyLong())).thenReturn(responseEntity);
        when(iNoteDAO.save(any())).thenReturn(expectedNote);
        // Act and Assert
        Note actualResult = iNoteService.save(note);
        assertNotNull(actualResult.getId());
        verify(iPatientProxy, times(1)).findById(anyLong());
        verify(iNoteDAO, times(1)).save(any());
    }

    @Test
    void testUpdate_shouldReturnTheUpdatedNote_forAValidNote() throws EntityNotFoundException {
        // Arrange
        Note note = new Note(1L, "TestNone", "Patient states that they are short term Smoker",
                LocalDateTime.of(2020, 3, 23, 6, 54),
                null);
        PatientBean patient = new PatientBean("which", "woman", LocalDate.of(2000, 02, 22), GenreBean.F, "2332-342-867",
                "ul nedt St.");
        note.setId("EmF25VV");
        when(iNoteDAO.findById(anyString())).thenReturn(Optional.of(note));
        note.setPatId(2L);
        when(iNoteDAO.save(any())).thenReturn(note);
        Optional<PatientBean> patientOptional = Optional.of(patient);
        ResponseEntity responseEntity = new ResponseEntity(patientOptional, HttpStatus.OK);
        when(iPatientProxy.findById(anyLong())).thenReturn(responseEntity);
        // Act and Assert
        Note actualResult = iNoteService.update(note);
        assertEquals(2L, actualResult.getPatId());
        verify(iPatientProxy, times(1)).findById(anyLong());
        verify(iNoteDAO, times(1)).save(any());
        verify(iNoteDAO, times(1)).findById("EmF25VV");
    }

    @Test
    void testUpdate_shouldThrowEntityNotFoundException_forANonExistingNote() throws EntityNotFoundException {
        // Arrange
        Note note = new Note(1L, "TestNone", "Patient states that they are short term Smoker",
                LocalDateTime.of(2021, 3, 23, 6, 54),
                null);
        note.setId("EmF25VV");
        when(iNoteDAO.findById(any())).thenReturn(Optional.empty());
        // Act and Assert
        Exception exception = assertThrows(EntityNotFoundException.class,
                () -> iNoteService.update(note));
        assertEquals(String.format("Note with id %s could not be found", "EmF25VV"), exception.getMessage());
        verify(iNoteDAO, times(0)).save(any());
        verify(iNoteDAO, times(1)).findById("EmF25VV");
    }

    @Test
    void testFindById_shouldReturnTheFoundNote_forAValidNoteId() {
        // Arrange
        Note note = new Note(1L, "TestNone", "Patient states that they are short term Smoker",
                LocalDateTime.of(2021, 3, 23, 6, 54),
                null);
        note.setId("EmF25VV");
        when(iNoteDAO.findById(any())).thenReturn(Optional.of(note));
        // Act and Assert
        Optional<Note> actualResult = iNoteService.findById("EmF25VV");
        assertTrue(actualResult.isPresent());
        assertEquals(1L, actualResult.get().getPatId());
        verify(iNoteDAO, times(1)).findById("EmF25VV");
    }

    @Test
    void testGetPatientHistory_shouldReturnAListOfNotes_forAPatientIdWithExistingNotes() {
        // Arrange
        Note note1 = new Note(1L, "TestNone", "Patient states that they are short term Smoker",
                LocalDateTime.of(2020, 3, 23, 6, 54),
                null);
        note1.setId("rS56");
        Note note2 = new Note(1L, "TestNone", "Patient states that they are short term Smoker",
                LocalDateTime.of(2020, 3, 23, 6, 54),
                null);
        note2.setId("B9we");
        List<Note> noteList = List.of(note1, note2);
        when(iNoteDAO.findAllByPatIdOrderByCreatedAtAsc(any())).thenReturn(noteList);
        // Act and Assert
        var actualResult = iNoteService.getPatientHistoryById(1L);
        assertTrue(actualResult.size() > 0);
        assertEquals(note1, noteList.get(0));
        verify(iNoteDAO, times(1)).findAllByPatIdOrderByCreatedAtAsc(1L);
    }

    @Test
    void testGetPatientHistoryGroupedByDate_shouldReturnAListOfDatesContainingPatientsNotes_forAPatientIdWithExistingNotes() {
        // Arrange
        Note note1 = new Note(1L, "TestNone", "Patient states that they are short term Smoker",
                LocalDateTime.of(2020, 3, 23, 6, 54),
                null);
        note1.setId("rS56");
        Note note2 = new Note(1L, "TestNone", "Patient states that they are short term Smoker",
                LocalDateTime.of(2020, 3, 23, 6, 54),
                null);
        note2.setId("B9we");
        Map<String, List<Note>> listMap = new HashMap<>();
        listMap.put("2022-03-23", List.of(note1, note2));

        when(iNoteCustomDao.findAllByPatIdOrderByCreatedAtAsc(any())).thenReturn(Collections.singletonList(listMap));
        // Act and Assert
        var actualResult = iNoteService.getPatientHistoryGroupedByDate(1L);
        assertTrue(actualResult.size() > 0);
        assertNotNull(actualResult.get(0));
        verify(iNoteCustomDao, times(1)).findAllByPatIdOrderByCreatedAtAsc(1L);
    }

    @Test
    void testGetPatientHistoryByFamily_shouldReturnAListOfNotes_forAPatientIdWithExistingNotes() {
        // Arrange
        Note note1 = new Note(1L, "TestNone", "Patient states that they are short term Smoker",
                LocalDateTime.of(2020, 3, 23, 6, 54),
                null);
        note1.setId("rS56");
        Note note2 = new Note(1L, "TestNone", "Patient states that they are short term Smoker",
                LocalDateTime.of(2020, 3, 23, 6, 54),
                null);
        note2.setId("B9we");
        List<Note> noteList = List.of(note1, note2);
        when(iNoteDAO.findAllByPatientFamilyOrderByCreatedAtAsc(anyString())).thenReturn(noteList);
        // Act and Assert(
        var actualResult = iNoteService.getPatientHistoryByFamily("TestNone");
        assertTrue(actualResult.size() > 0);
        assertEquals(note1, noteList.get(0));
        verify(iNoteDAO, times(1)).findAllByPatientFamilyOrderByCreatedAtAsc("TestNone");
    }
}