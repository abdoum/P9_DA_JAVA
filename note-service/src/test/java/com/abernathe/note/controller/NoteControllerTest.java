package com.abernathe.note.controller;

import com.abernathe.note.model.Note;
import com.abernathe.note.service.INoteService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(NoteController.class)
@ActiveProfiles("test")
class NoteControllerTest {

    private static ObjectMapper mapper;

    @Autowired
    MockMvc mockMvc;

    @MockBean
    INoteService iNoteService;

    @BeforeAll
    static void beforeAll() {
        mapper = new ObjectMapper()
                .registerModule(new JavaTimeModule());
    }

    @Test
    void testAddNote_shouldReturnTheSavedNote_forAValidNote() throws Exception {
        // Arrange
        Note note = new Note(1L,"TestNone", "Patient states that they are short term Smoker",
                LocalDateTime.of(2021, 3, 23, 6, 54),
                null);
        when(iNoteService.save(any())).thenReturn(note);
        note.setId("tonight");
        // Act and Assert
        mockMvc.perform(post("/notes")
                       .contentType(MediaType.APPLICATION_JSON)
                       .accept("application/json")
                       .content(mapper.writeValueAsString(note)))
               .andExpect(jsonPath("$.patId", is(1)))
               .andExpect(jsonPath("$.id", is("tonight")))
               .andExpect(jsonPath("$.content", is(note.getContent())))
               .andExpect(status().isOk());
    }

    @Test
    void testUpdateNote_shouldReturnTheUpdatedNote_forAValidNote() throws Exception {
        // Arrange
        Note note = new Note(1L, "TestNone","Patient states that they are short term Smoker",
                LocalDateTime.of(2021, 3, 23, 6, 54),
                null);
        String noteId = "61d5e48f9af05370a549f9ea";
        note.setId(noteId);
        when(iNoteService.update(any())).thenReturn(note);
        // Act and Assert
        mockMvc.perform(put("/notes/{noteId}", noteId)
                       .contentType(MediaType.APPLICATION_JSON)
                       .accept("application/json")
                       .content(mapper.writeValueAsString(note)))
               .andExpect(jsonPath("$.patId", is(1)))
               .andExpect(jsonPath("$.id", is(noteId)))
               .andExpect(jsonPath("$.content", is(note.getContent())))
               .andExpect(status().isOk());
    }

    @Test
    void testeGetPatientHistory_shouldReturnAListOfNotes_forAPatientWithExistingNotes() throws Exception {
        // Arrange
        Note note1 = new Note(1L, "TestNone","Patient states that they are short term Smoker",
                LocalDateTime.of(2020, 3, 23, 6, 54),
                null);
        note1.setId("rS56");
        Note note2 = new Note(1L,"TestNone", "Patient states that they are short term Smoker",
                LocalDateTime.of(2021, 3, 23, 6, 54),
                null);
        note2.setId("B9we");
        List<Note> noteList = List.of(note1, note2);
        when(iNoteService.getPatientHistoryById(any())).thenReturn(noteList);
        // Act and Assert
        mockMvc.perform(get("/notes/{patientId}", "1")
                       .accept("application/json"))
               .andExpect(jsonPath("$[0].patId", is(1)))
               .andExpect(jsonPath("$[0].id", is("rS56")))
               .andExpect(jsonPath("$[0].content", is(noteList.get(0).getContent())))
               .andExpect(status().isOk());
    }
}