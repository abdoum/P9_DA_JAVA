package com.abernathe.gateway.config;

import org.springframework.cloud.client.discovery.ReactiveDiscoveryClient;
import org.springframework.cloud.gateway.discovery.DiscoveryClientRouteDefinitionLocator;
import org.springframework.cloud.gateway.discovery.DiscoveryLocatorProperties;
import org.springframework.cloud.gateway.handler.RoutePredicateHandlerMapping;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.web.cors.CorsConfiguration;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


@Configuration
public class CorsConfig {

    /**
     * Enables routes discovery for microservice exposed by the discovery service
     *
     * @param reactiveDiscoveryClient
     * @param discoveryLocatorProperties
     * @return DiscoveryClientRouteDefinitionLocator
     */
    @Bean
    DiscoveryClientRouteDefinitionLocator dynamicRoutes(ReactiveDiscoveryClient reactiveDiscoveryClient,
                                                        DiscoveryLocatorProperties discoveryLocatorProperties) {
        return new DiscoveryClientRouteDefinitionLocator(reactiveDiscoveryClient, discoveryLocatorProperties);
    }

    /**
     * Configures cors for all routes
     *
     * @param routePredicateHandlerMapping the route predicate to match
     * @return CorsConfiguration the configuration
     */
    @Bean
    public CorsConfiguration corsConfiguration(RoutePredicateHandlerMapping routePredicateHandlerMapping) {
        CorsConfiguration corsConfiguration = new CorsConfiguration().applyPermitDefaultValues();
        Arrays.asList(HttpMethod.values())
              .forEach(corsConfiguration::addAllowedMethod);
        corsConfiguration.setAllowedOrigins(Collections.singletonList(CorsConfiguration.ALL));
        Map<String, CorsConfiguration> configurationMap = new HashMap<>();
        configurationMap.put("/**", corsConfiguration);
        routePredicateHandlerMapping.setCorsConfigurations(configurationMap);
        return corsConfiguration;
    }

}