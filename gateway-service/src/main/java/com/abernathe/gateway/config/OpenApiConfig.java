package com.abernathe.gateway.config;

import lombok.Generated;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.cloud.gateway.route.RouteDefinitionLocator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
@Generated
public class OpenApiConfig {

    /**
     * Swagger ui configuration
     *
     * @param locator the RouteDefinitionLocator
     * @return a list of the swagger config for each microservice declaring the springdoc dependency
     */
    @Bean
    public List<GroupedOpenApi> apis(RouteDefinitionLocator locator) {
        List<GroupedOpenApi> groups = new ArrayList<>();
        List<RouteDefinition> definitions = locator.getRouteDefinitions().collectList().block();
        assert definitions != null;
        definitions.stream()
                   .filter(routeDefinition -> routeDefinition.getId().matches(".*-service"))
                   .forEach(routeDefinition -> {
                       String name = routeDefinition.getId().replace("-service", "");
                       groups.add(GroupedOpenApi.builder().pathsToMatch("/" + name + "/**").group(name).build());
                   });
        return groups;
    }

}