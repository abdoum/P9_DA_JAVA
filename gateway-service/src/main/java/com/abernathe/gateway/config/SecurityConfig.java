package com.abernathe.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.ReactiveJwtAuthenticationConverterAdapter;
import org.springframework.security.web.server.SecurityWebFilterChain;
import reactor.core.publisher.Mono;

@Configuration
@EnableWebFluxSecurity
public class SecurityConfig {

    public static final String ADMIN_ROLE = "ADMIN";

    public static final String PRACTITIONER_ROLE = "PRACTITIONER";

    /**
     * Applies a custom converter for jwt.
     *
     * @return ReactiveJwtAuthenticationConverterAdapter
     */
    Converter<Jwt, Mono<AbstractAuthenticationToken>> customConverter() {
        JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(new KeycloakRealmRoleConverter());
        return new ReactiveJwtAuthenticationConverterAdapter(jwtAuthenticationConverter);
    }

    /**
     * Applies security filters on http requests.
     *
     * @param http the SeverHttpSecurity object
     * @return the SecurityWebFilterChain
     */

    @Bean()
    public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
        http
                .authorizeExchange()
                .pathMatchers(
                        "/actuator/**",
                        "/assess/**",
                        "/webjars/**",
                        "/swagger-ui/**",
                        "/swagger-ui.html",
                        "/*/v3/api-docs/**",
                        "/v3/api-docs/**")
                .permitAll()
                .pathMatchers(HttpMethod.OPTIONS, "/**").permitAll()
//                .pathMatchers("*/actuator/**").hasRole(ADMIN_ROLE)
                .pathMatchers("/patients/**").hasRole(PRACTITIONER_ROLE)
                .pathMatchers("/reports/**").hasRole(PRACTITIONER_ROLE)
                .pathMatchers("/notes/**").hasRole(PRACTITIONER_ROLE)
                .anyExchange().authenticated()
                .and()
                .oauth2ResourceServer()
                .jwt().jwtAuthenticationConverter(customConverter());
        http
                .csrf().disable();

        return http.build();
    }
}