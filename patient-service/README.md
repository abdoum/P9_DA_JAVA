![](https://img.shields.io/badge/Java-ED8B00?style=for-the-badge&logo=java&logoColor=white)
![](https://img.shields.io/badge/Spring-6DB33F?style=for-the-badge&logo=spring&logoColor=white)
![](/gitlab/last-commit/gitlab-org/gitlab-development-kit)
![](/maven/v/metadata-url/repo1.maven.org/maven2/com/google/code/gson/gson/maven-metadata.xml)

# Getting Started

### Reference Documentation

[[_TOC_]]

### Description

The patient micro-service handles all patient related operations.

![](/Users/dr/Documents/Formation/openclassrooms/projets/P9_DA_JAVA/code/clinique/patient/src/main/resources/Template+de+spécifications+API+REST+P9.pdf)

![](/Users/dr/Documents/Formation/openclassrooms/projets/P9_DA_JAVA/code/clinique/patient/src/main/resources/architecture.jpg)

### Requirements

----

- Unix or Windows OS
- [Docker engine](https://docs.docker.com/get-started/)

[Spécifités de l'api rest](https://docs.google.com/document/d/e/2PACX-1vRxBtkfkj3x0t0TzDqq3AxFiK5UL_96jBbpBbZQ_d7heV4bXCtNXWy8AzwDwaPEBwr2MZIYyKgAimiO/pub)

### Rétrospective sprint 1

---
Ce qui s’est bien passé :

* Implémentation de la base de donnée
* Création des endpoints

Ce qui aurait pu aller mieux :

* Mise en place du gateway utilisant Zuul (la bibliothèque nécessite une ancienne version de spring boot)
* Validation des tests curl fournis (les tests curl utilisent url_encoded pour la sérialisation des données, ceci
  nécessite une implémentation spécifique, car le format accepté par défaut est json)

Ce que j’aimerais faire différemment :

* Limiter les tâches accomplies å celles mentionnées sur l’issue
* Automatiser la génération de la documentation de l’api rest