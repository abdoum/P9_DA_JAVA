package com.abernathe.patient.repository;

import com.abernathe.patient.model.Genre;
import com.abernathe.patient.model.Patient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

@SpringBootTest
@ActiveProfiles("test")
class IPatientDaoIT {

    @Autowired
    IPatientDAO iPatientDAO;

    @Test
    void testFindById_shouldReturnTheFoundPatient_forAnExistingPatient() {
        iPatientDAO.deleteAll();
        Patient patient = new Patient("punctual", "rivalry", LocalDate.parse("1998-03-23"), Genre.F,
                "876-523-5832"
                , "12 Albatro's St.");
        var expectedResult = iPatientDAO.save(patient);
        var actualResult = iPatientDAO.findById(expectedResult.getId());
        assertEquals(expectedResult.getId(), actualResult.get().getId());
        assertSame(expectedResult, patient);
        iPatientDAO.deleteAll();
    }

    @ParameterizedTest
    @CsvSource({
            "TestNone, Test, 1966-12-31, F, 100-222-3333, '1 Brookside St', 1",
            "TestBorderline, Test, 1945-06-24, M, 200-333-4444, '2 High St', 2",
            "TestInDanger, Test, 2004-06-18, M, 300-444-5555, '3 Club Road', 3",
            "TestEarlyOnset, Test, 2002-06-28, F, 400-555-6666, '4 Valley Dr', 4",
    })
    void testSave_shouldSaveThePatientToDatabase_forAValidPatient(String expectedFamily,
                                                                  ArgumentsAccessor arguments) {
        Patient patient = new Patient(
                arguments.getString(1),
                arguments.getString(0),
                arguments.get(2, LocalDate.class),
                Genre.valueOf(arguments.getString(3)),
                arguments.getString(4),
                arguments.getString(5)
        );
        patient.setId(arguments.getLong(6));
        Patient savedPatient = iPatientDAO.save(patient);
        assertEquals(expectedFamily, iPatientDAO.findById(savedPatient.getId()).get().getFamily());
    }

    @Test
    void testUpdate_shouldReturnTheUpdatedPatientFromDatabase_forAValidPatient() {
        iPatientDAO.deleteAll();
        Patient patient = new Patient("punctual", "rivalry", LocalDate.parse("1998-03-23"), Genre.F,
                "876-523-5832"
                , "12 Albatro's St.");

        Patient savedPatient = iPatientDAO.save(patient);
        savedPatient.setFamily("modified");
        iPatientDAO.save(savedPatient);
        assertEquals("modified", iPatientDAO.findById(savedPatient.getId()).get().getFamily());
    }
}