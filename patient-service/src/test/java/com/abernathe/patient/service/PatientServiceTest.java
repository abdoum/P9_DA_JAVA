package com.abernathe.patient.service;

import com.abernathe.patient.exception.EntityNotFoundException;
import com.abernathe.patient.exception.PatientAlreadyExistException;
import com.abernathe.patient.model.Genre;
import com.abernathe.patient.model.Patient;
import com.abernathe.patient.repository.IPatientDAO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {PatientService.class})
@ExtendWith(SpringExtension.class)
class PatientServiceTest {

    @MockBean
    IPatientDAO iPatientDAO;

    @Autowired
    IPatientService classUnderTest;


    @Test
    void testSave_shouldReturnThePatientWithAnId_forANonExistingPatient() throws PatientAlreadyExistException {
        // Arrange
        Patient expectedPatient = new Patient("Prenom", "Nom", LocalDate.ofEpochDay(1L), Genre.M,
                "410-555-1212", "12 red St");
        expectedPatient.setId(1L);
        when(this.iPatientDAO.save((Patient) any())).thenReturn(expectedPatient);
        when(this.iPatientDAO.findById((Long) any())).thenReturn(Optional.empty());
        Patient patientToBeSaved = new Patient("Prenom", "Nom", LocalDate.ofEpochDay(1L), Genre.M,
                "410-555-1212", "12 red St");
        patientToBeSaved.setId(1L);
        // Act and Assert
        assertSame(expectedPatient, this.classUnderTest.save(patientToBeSaved));
        verify(this.iPatientDAO).save((Patient) any());
    }

    @Test
    void testUpdate_shouldReturnTheUpdatedPatient_forAValidPatient() throws
            PatientAlreadyExistException,
            EntityNotFoundException {
        // Arrange
        Patient patient = new Patient("punctual", "rivalry", LocalDate.parse("1998-03-23"), Genre.F,
                "876-523-5832"
                , "12 Albatro's St.");
        when(this.iPatientDAO.save((Patient) any())).thenReturn(patient);
        when(this.iPatientDAO.findById((Long) any())).thenReturn(Optional.of(patient));
        // Act and Assert
        var actualResult = classUnderTest.update(patient);
        assertEquals("rivalry", actualResult.getFamily());
        verify(this.iPatientDAO, times(1)).save((Patient) any());
    }

    @Test
    void testUpdate_shouldThrowAnEntityNotFoundException_ForAnNonExistingPatient() throws EntityNotFoundException {
        // Arrange
        Patient patient = new Patient("Prenom", "Nom", LocalDate.ofEpochDay(1L), Genre.M,
                "410-555-1212", "12 red St");
        patient.setId(1L);
        when(iPatientDAO.findById((Long) any())).thenReturn(Optional.empty());
        // Act and Assert
        var exception = assertThrows(EntityNotFoundException.class,
                () -> this.classUnderTest.update(patient));
        verify(iPatientDAO, times(1)).findById(1L);
        assertEquals(MessageFormat.format("Patient with id {0} was not found.", patient.getId()),
                exception.getMessage());
    }

    @Test
    void testFindById_shouldReturnTheFoundPatient_ForAnExistingPatient() throws
            EntityNotFoundException {
        // Arrange
        Patient patient = new Patient("punctual", "rivalry", LocalDate.parse("1998-03-23"), Genre.F,
                "876-523-5832"
                , "12 Albatro's St.");
        patient.setId(1L);
        when(this.iPatientDAO.findById((Long) any())).thenReturn(Optional.of(patient));
        // Act and Assert
        var actualResult = classUnderTest.findById(1L);
        assertEquals("rivalry", actualResult.get().getFamily());
        verify(this.iPatientDAO).findById((Long) any());
    }

    @Test
    void testFindByFamily_shouldReturnTheFoundPatient_ForAnExistingPatient() throws
            EntityNotFoundException {
        // Arrange
        Patient patient = new Patient("punctual", "rivalry", LocalDate.parse("1998-03-23"), Genre.F,
                "876-523-5832"
                , "12 Albatro's St.");
        patient.setId(1L);
        when(this.iPatientDAO.findByFamilyIgnoreCase(anyString())).thenReturn(Optional.of(List.of(patient)));
        // Act and Assert
        Optional<List<Patient>> actualResult = classUnderTest.findByFamily("rivalry");
        assertEquals("rivalry", actualResult.get().get(0).getFamily());
        verify(this.iPatientDAO).findByFamilyIgnoreCase("rivalry");
    }


    @Test
    void testFindAll_shouldReturnAListOfPatients_forExistingPatients() {
        // Arrange
        Patient patient = new Patient("punctual", "rivalry", LocalDate.parse("1998-03-23"), Genre.F,
                "876-523-5832"
                , "12 Albatro's St.");
        patient.setId(1L);
        when(this.iPatientDAO.findAll()).thenReturn(Collections.singletonList(patient));
        // Act and Assert
        var actualResult = classUnderTest.findAll();
        assertTrue(actualResult.size() > 0);
        verify(this.iPatientDAO).findAll();
    }

}