package com.abernathe.patient.controller;

import com.abernathe.patient.model.Genre;
import com.abernathe.patient.model.Patient;
import com.abernathe.patient.service.IPatientService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(PatientController.class)
@ActiveProfiles("test")
class PatientControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IPatientService iPatientService;

    private ObjectMapper mapper;

    @BeforeEach
    void setUp() {
        mapper = new ObjectMapper()
                .registerModule(new ParameterNamesModule())
                .registerModule(new Jdk8Module())
                .registerModule(new JavaTimeModule());
    }

    @Test
    void testFindAll_shouldReturnAListOPatients() throws Exception {
        // Arrange
        List<Patient> patientList = Collections.singletonList(
                new Patient("punctual", "rivalry", LocalDate.parse("1998-03-23"), Genre.F, "876-523-5832"
                        , "12 Albatro's St."));
        when(iPatientService.findAll()).thenReturn(patientList);
        // Act and Assert
        mockMvc.perform(get("/patients/list"))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$").isArray())
               .andExpect(jsonPath("$[0].given", is("punctual")));
    }

    @Test
    void testUpdate_shouldReturnTheUpdatedPatient_forAValidAndExistingPatient() throws Exception {
        // Arrange
        Patient patient = new Patient("punctual", "rivalry", LocalDate.parse("1998-03-23"), Genre.F,
                "876-523-5832"
                , "12 Albatro's St.");
        patient.setId(2L);
        when(iPatientService.update(patient)).thenReturn(patient);
        // Act and Assert
        mockMvc.perform(put("/patients/{patientId}", patient.getId())
                       .contentType(MediaType.APPLICATION_JSON)
                       .content(mapper.writeValueAsString(patient)))
               .andExpect(status().isOk());
        verify(iPatientService, times(1)).update((Patient) any());
    }

    @Test
    void testSave_shouldReturnTheSavedPatient_forAValidJsonEncodedPatient() throws Exception {
        // Arrange
        Patient patient = new Patient("punctual", "rivalry", LocalDate.parse("1998-03-23"), Genre.F,
                "876-523-5832"
                , "12 Albatro's St.");
        when(iPatientService.save(patient)).thenReturn(patient);
        // Act and Assert
        mockMvc.perform(post("/patients")
                       .contentType(MediaType.APPLICATION_JSON)
                       .content(mapper.writeValueAsString(patient)))
               .andExpect(status().isCreated());
        verify(iPatientService, times(1)).save((Patient) any());
    }


    @Test
    void testFindById_shouldReturnTheFoundPatient_forAnExistingId() throws Exception {
        // Arrange
        Patient patient = new Patient("punctual", "rivalry", LocalDate.parse("1998-03-23"), Genre.F,
                "876-523-5832"
                , "12 Albatro's St.");
        patient.setId(2L);
        when(iPatientService.findById(2L)).thenReturn(java.util.Optional.of(patient));
        // Act and Assert
        mockMvc.perform(get("/patients/{id}", 2L))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.given", is("punctual")));
        verify(iPatientService, times(1)).findById((Long) any());
    }

    @Test
    void testFindByFamilyName_shouldReturnTheFoundPatient_forAnExistingPatient() throws Exception {
        // Arrange
        Patient patient = new Patient("punctual", "rivalry", LocalDate.parse("1998-03-23"), Genre.F,
                "876-523-5832"
                , "12 Albatro's St.");
        patient.setId(2L);
        when(iPatientService.findByFamily("rivalry")).thenReturn(Optional.of(List.of(patient)));
        // Act and Assert
        mockMvc.perform(get("/patients", 2L)
                       .param("family", "rivalry"))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$[0].given", is("punctual")));
        verify(iPatientService, times(1)).findByFamily("rivalry");
    }
}