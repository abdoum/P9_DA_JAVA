package com.abernathe.patient.repository;

import com.abernathe.patient.model.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface IPatientDAO extends JpaRepository<Patient, Integer> {

    Optional<Patient> findById(Long id);

    @Query(value = "SELECT * FROM Patient p WHERE lower(p.given) = lower(:given) and lower(p.family) = lower(:family) and p.dob = :dob limit 1",
            nativeQuery = true)
    Optional<Patient> findPatientByGivenAndFamilyAndDobNamedParamsNative(
            @Param("given") String given, @Param("family") String family, @Param("dob") LocalDate dob);

    Optional<List<Patient>> findByFamilyIgnoreCase(String patientFamilyName);
}