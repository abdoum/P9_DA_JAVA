package com.abernathe.patient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients("com.abernathe.patient")
@EnableDiscoveryClient
public class PatientApp {

    public static void main(String[] args) {
        SpringApplication.run(PatientApp.class, args);
    }

}