package com.abernathe.patient.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.Length;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.util.Objects;


@Table(name = "patient")
@Entity
@Getter
@ToString
@RequiredArgsConstructor
@DynamicUpdate
public class Patient {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Length(min = 2, max = 50)
    @NotBlank(message = "Given name cannot be empty")
    @Column(name = "given", nullable = false, length = 50)
    private String given;

    @Length(min = 2, max = 50)
    @NotBlank(message = "Family name cannot be empty")
    @Column(name = "family", nullable = false, length = 50)
    private String family;

    @PastOrPresent(message = "Date of birth cannot be in the future")
    @Column(name = "dob", nullable = false)
    private LocalDate dob;

    @NotNull(message = "Sex must be specified")
    @Enumerated
    @Column(name = "sex", nullable = false)
    private Genre sex;

    @Pattern(message = "Phone must be a valid phone number in the format 100-000-0000",
             regexp = "([1-9][0-9]{2}-[0-9]{3}-[0-9]{4})*")
    @Column(name = "phone", length = 20)
    @Nullable
    private String phone;

    @Pattern(message = "Address must only contain alphanumerical characters only.",
             regexp = "[\\p{Space}*\\p{Alnum}*\\p{Digit}.']{0,150}")
    @Nullable
    @Column(name = "address", length = 150)
    private String address;

    /**
     * Instantiates a new Patient.
     *
     * @param given   the given
     * @param family  the family
     * @param dob     the dob
     * @param sex     the sex
     * @param phone   the phone
     * @param address the address
     */
    public Patient(String given, String family, LocalDate dob, Genre sex, String phone, String address) {
        this.given   = given;
        this.family  = family;
        this.dob     = dob;
        this.sex     = sex;
        this.phone   = phone;
        this.address = address;
    }

    /**
     * Sets phone.
     *
     * @param phoneNumber the phone number
     */
    public void setPhone(String phoneNumber) {
        this.phone = phoneNumber;
    }

    /**
     * Sets sex.
     *
     * @param genre the genre
     */
    public void setSex(Genre genre) {
        this.sex = genre;
    }

    /**
     * Sets dob.
     *
     * @param birthDate the birth date
     */
    public void setDob(LocalDate birthDate) {
        this.dob = birthDate;
    }

    /**
     * Sets family.
     *
     * @param familyName the family name
     */
    public void setFamily(String familyName) {
        this.family = familyName;
    }

    /**
     * Sets given.
     *
     * @param givenName the given name
     */
    public void setGiven(String givenName) {
        this.given = givenName;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Patient)) {
            return false;
        }
        Patient patient = (Patient) obj;
        return Objects.equals(getId(), patient.getId()) && getGiven().equals(patient.getGiven()) &&
               getFamily().equals(patient.getFamily()) && getDob().equals(patient.getDob()) &&
               getSex() == patient.getSex() && Objects.equals(getPhone(), patient.getPhone()) &&
               Objects.equals(getAddress(), patient.getAddress());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getGiven(), getFamily(), getDob(), getSex(), getPhone(), getAddress());
    }
}