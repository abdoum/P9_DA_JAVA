package com.abernathe.patient.controller;

import com.abernathe.patient.exception.EntityNotFoundException;
import com.abernathe.patient.exception.PatientAlreadyExistException;
import com.abernathe.patient.model.Patient;
import com.abernathe.patient.service.IPatientService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("patients")
@Validated
@Slf4j
@CrossOrigin(allowedHeaders = "*", origins = "*")
public class PatientController {

    private final IPatientService iPatientService;

    public PatientController(IPatientService iPatientService) {
        this.iPatientService = iPatientService;
    }

    /**
     * Returns all the patients.
     *
     * @return A list of all the patients
     */
    @Operation(summary = "Get all the patients",
            description = "Get all the patients", security = {@SecurityRequirement(name = "bearer-key")})
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "A list of all the patients or a list of patient")
            }
    )
    @GetMapping(value = "/list", produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<Patient> findAll() {
        log.info("received get request to /list");
        var patients = iPatientService.findAll();
        log.info(patients.toString());
        return patients;
    }

    /**
     * Updates a patient. Returns the updated patient on success. Displays validations errors otherwise.
     *
     * @param patient the updated patient
     * @return the html markup
     */
    @Operation(summary = "Updates a patient.",
            description = "Updates a patient. Returns the updated patient on success. Displays validations errors otherwise.")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "The updated patient.")
            }
    )
    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Patient update(@Valid @RequestBody Patient patient, @PathVariable("id") @Positive Long id) throws
            EntityNotFoundException {
        log.info("Patient: {}", patient);
        log.info("id: {}", id);
        patient.setId(id);
        Patient updatedPatient = iPatientService.update(patient);
        log.info("Updated patient: {}", updatedPatient);
        return updatedPatient;
    }

    /**
     * Saves a patient. Returns the added patient on success. Accepts only urlencoded form data.
     *
     * @param patient the patient
     * @return Patient  the added patient
     * @throws PatientAlreadyExistException if the patient already exists
     */
    @Operation(summary = "Saves a patient.",
            description = "Saves a patient. Returns the added patient on success. Accepts only urlencoded form data.")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Patient  the added patient")
            }
    )
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Patient addPatient(@Valid @RequestBody Patient patient) throws PatientAlreadyExistException {
        log.info(String.valueOf(patient));
        Patient savedPatient = iPatientService.save(patient);
        log.info(savedPatient.toString());
        return savedPatient;
    }

    /**
     * Find a patient by its id. Returns the found patient on success, null otherwise.
     *
     * @param patientId the patient id
     * @return Patient  the found patient or null if no patient was found
     */
    @Operation(summary = "Find a patient by id.",
            description = "Find a patient. Returns the found patient on success, null otherwise.")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200",
                            description = "Patient  the found patient or null if no patient was found")
            }
    )
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Optional<Patient>> findById(@PathVariable("id") @Positive Long patientId) {
        log.info(String.valueOf(patientId));
        Optional<Patient> patient = iPatientService.findById(patientId);
        return ResponseEntity.ok(patient);
    }

    /**
     * Find a patient by family name. Returns the found patient on success, null otherwise.
     *
     * @param patientFamilyName the patient id
     * @return Patient  the found patient or null if no patient was found
     */
    @Operation(summary = "Find a patient by family name.",
            description = "Find a patient by family name. Returns the found patient on success, null otherwise.")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200",
                            description = "Patient  the found patient or null if no patient was found")
            }
    )
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Optional<List<Patient>>> findByFamilyName(
            @RequestParam("family") @NotBlank String patientFamilyName) {
        log.info(patientFamilyName);
        Optional<List<Patient>> patient = iPatientService.findByFamily(patientFamilyName);
        patient.ifPresent(foundPatient -> log.info(foundPatient.toString()));
        return ResponseEntity.ok(patient);
    }

}