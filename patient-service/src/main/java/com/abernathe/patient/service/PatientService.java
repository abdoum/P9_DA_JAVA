package com.abernathe.patient.service;

import com.abernathe.patient.exception.EntityNotFoundException;
import com.abernathe.patient.exception.PatientAlreadyExistException;
import com.abernathe.patient.model.Patient;
import com.abernathe.patient.repository.IPatientDAO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class PatientService implements IPatientService {

    private final IPatientDAO iPatientDAO;

    public PatientService(IPatientDAO iPatientDAO) {
        this.iPatientDAO = iPatientDAO;
    }

    /**
     * Saves a new patient. Checks if patient already exists using the id or first, last names along with birthdate.
     *
     * @param patientToBeSaved the patient to save
     * @return the new added patient
     * @throws PatientAlreadyExistException if the patient already exists
     */
    @Override
    public Patient save(Patient patientToBeSaved) throws PatientAlreadyExistException {
        checkIfPatientExists(patientToBeSaved);
        var patientToPersist = new Patient(
                patientToBeSaved.getGiven(),
                patientToBeSaved.getFamily(),
                patientToBeSaved.getDob(),
                patientToBeSaved.getSex(),
                patientToBeSaved.getPhone(),
                patientToBeSaved.getAddress());
        log.debug(patientToPersist.toString());
        return iPatientDAO.save(patientToPersist);
    }

    /**
     * Checks if a patient already exists or has a matching given, family, and date of birth.
     *
     * @param patientToBeSaved the new patient to be saved
     * @throws PatientAlreadyExistException if a patient already exists or a match is found.
     */
    private void checkIfPatientExists(Patient patientToBeSaved)
            throws PatientAlreadyExistException {
        if (isPatientByGivenFamilyAndDobExists(patientToBeSaved)) {
            throw new PatientAlreadyExistException(
                    MessageFormat.format("Patient {0} {1} born in {2} already exist.",
                            patientToBeSaved.getGiven(),
                            patientToBeSaved.getFamily(),
                            patientToBeSaved.getDob()));
        }
    }

    /**
     * Updates a patient using its id
     *
     * @param patient the modified patient to be updated
     * @return the updated patient
     * @throws EntityNotFoundException if the patient could not be found with the provided id
     */
    @Override
    public Patient update(Patient patient) throws EntityNotFoundException {
        Patient existingPatient = checkIfPatientExistsById(patient.getId());
        Patient patientToPersist = new Patient(
                patient.getGiven(),
                patient.getFamily(),
                patient.getDob(),
                patient.getSex(),
                patient.getPhone(),
                patient.getAddress());
        patientToPersist.setId(existingPatient.getId());
        log.debug(patientToPersist.toString());
        return iPatientDAO.save(patient);
    }

    /**
     * Checks if a patient exists using its id
     *
     * @param patientId Long the patient id to look for
     * @return the found patient or null if none found
     * @throws EntityNotFoundException if the patient could not be found
     */
    private Patient checkIfPatientExistsById(Long patientId) throws EntityNotFoundException {
        return iPatientDAO.findById(patientId)
                .orElseThrow(() -> new EntityNotFoundException(
                        MessageFormat.format("Patient with id {0} was not found.", patientId)));
    }

    /**
     * Finds a patient by its id
     *
     * @param patientId the id of the patient to retrieve
     * @return the found patient or null if no patient is found
     */
    @Override
    public Optional<Patient> findById(Long patientId) {
        return iPatientDAO.findById(patientId);
    }

    /**
     * Finds a matching patient by its given name, family name, date of birth.
     * Returns null if no matches are found.
     *
     * @param patient the patient
     * @return the found match or null if no matches were found.
     */
    @Override
    public boolean isPatientByGivenFamilyAndDobExists(Patient patient) {
        return iPatientDAO.findPatientByGivenAndFamilyAndDobNamedParamsNative(
                patient.getGiven(),
                patient.getFamily(),
                patient.getDob())
                .isPresent();
    }

    /**
     * Finds all the patients
     *
     * @return a list of all the patients or an empty list if no patients are found
     */
    @Override
    public List<Patient> findAll() {
        return iPatientDAO.findAll();
    }

    /**
     * Finds a patient by its family name.
     *
     * @param patientFamilyName the patient's family name.
     * @return the found patient or null if none found.
     */
    @Override
    public Optional<List<Patient>> findByFamily(String patientFamilyName) {
        return iPatientDAO.findByFamilyIgnoreCase(patientFamilyName.trim());
    }
}