package com.abernathe.patient.service;

import com.abernathe.patient.exception.EntityNotFoundException;
import com.abernathe.patient.exception.PatientAlreadyExistException;
import com.abernathe.patient.model.Patient;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;
import java.util.Optional;

@Validated
public interface IPatientService {

    Patient save(@Valid Patient patient) throws PatientAlreadyExistException;

    Patient update(@Valid Patient patient) throws EntityNotFoundException;

    Optional<Patient> findById(@NotNull @Positive Long patient);

    boolean isPatientByGivenFamilyAndDobExists(@Valid Patient patient);

    List<Patient> findAll();

    Optional<List<Patient>> findByFamily(@NotBlank String patientFamilyName);
}