package com.abernathe.patient.exception;

public class PatientAlreadyExistException extends Exception {

    private static final long serialVersionUID = 1L;

    public PatientAlreadyExistException() {
        super();
    }

    public PatientAlreadyExistException(String errorMessage) {
        super(errorMessage);
    }
}