#
# drop table if exists patient;
#
# CREATE TABLE patient
# (
#     id      INT PRIMARY KEY,
#     given   VARCHAR(50) NOT NULL,
#     family  VARCHAR(50) NOT NULL,
#     dob     DATE        NOT NULL,
#     sex     int         NOT NULL,
#     phone   VARCHAR(20)  DEFAULT NULL,
#     address VARCHAR(150) DEFAULT NULL
# );
#
# insert into patient (id, dob, sex, family, phone, given, address)
# VALUES (1, '2009-02-22', 0, 'noel', '00297637826', 'jean', '1 Brookside St'),
#        (2, '1987-02-22', 1, 'tom', '3378628673', 'marc', '4 tpoiy St');