package com.abernathe.springbootadmin;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAdminServer
public class SpringbootAdminApp {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootAdminApp.class, args);
    }

}