
![](https://img.shields.io/badge/Java-ED8B00?style=for-the-badge&logo=java&logoColor=white)
![](https://img.shields.io/badge/Spring-6DB33F?style=for-the-badge&logo=spring&logoColor=white)
![](/gitlab/last-commit/gitlab-org/gitlab-development-kit)
![](/maven/v/metadata-url/repo1.maven.org/maven2/com/google/code/gson/gson/maven-metadata.xml)

<img alt="Mediscreen logo image" src="/Users/dr/Documents/Formation/openclassrooms/projets/P9_DA_JAVA/code/clinique/src/main/resources/static/logo.png" title="Mediscreen logo" width="160"/>

# Mediscreen

### Reference Documentation

[[_TOC_]]

### Description

Mediscreen est une application basée sur spring boot et react.

Elle est composée d'une architecture en microservices pour le back end et est déployée dans des conteneurs dockers.


### Technologies

---

<img src="https://imgs.search.brave.com/hwkl-0xczbS_GCezpdIP2GqTZWn8ztiLzm0ekQKXWsU/rs:fit:1652:225:1/g:ce/aHR0cHM6Ly90c2Ux/Lm1tLmJpbmcubmV0/L3RoP2lkPU9JUC5m/SzdVNTZRLU1SekVr/dVdIbU1SbjFRSGFD/SSZwaWQ9QXBp" width="120"/>
<img src="https://imgs.search.brave.com/OLuwytQqLy6T5IByVdQZ3TjnfLgL3lDMgI_p8rcFQJI/rs:fit:718:206:1/g:ce/aHR0cHM6Ly9taXJv/Lm1lZGl1bS5jb20v/bWF4LzE0MzYvMSpj/VVVKczktekFjYldW/ck82a2lLR3B3LnBu/Zw" width="120"/>
<img src="https://imgs.search.brave.com/JpU630jzcREj3eZc0sojGAtNJ2IE5LcN32dORZDc7ic/rs:fit:1118:494:1/g:ce/aHR0cHM6Ly93d3cu/cG9waXQua3Ivd3At/Y29udGVudC91cGxv/YWRzLzIwMjAvMTEv/c3ByaW5nX2Nsb3Vk/X2dhdGV3YXkuanBn" width="120"/>
<img src="https://imgs.search.brave.com/vY9NYA2Oeuc-mgLveqWskvMGixo07bkSQknInjiQDHc/rs:fit:700:832:1/g:ce/aHR0cHM6Ly96aXBr/aW4uaW8vcHVibGlj/L2ltZy9sb2dvX3Bu/Zy96aXBraW5fdmVy/dGljYWxfZ3JleV9n/Yi5wbmc" width="120"/>
<img src="https://imgs.search.brave.com/yJfutfOyA5zirUEFBanDH_CS0l1rU7zVLk5U9I7ZY_o/rs:fit:1200:1200:1/g:ce/aHR0cHM6Ly9kb3du/bG9hZC5sb2dvLndp/bmUvbG9nby9Nb25n/b0RCL01vbmdvREIt/TG9nby53aW5lLnBu/Zw" width="120"/>
<img src="https://imgs.search.brave.com/mIeqEoWlpXbIFsF7g56qG-SQljZSQ9PgYqNUspl_ZAo/rs:fit:910:555:1/g:ce/aHR0cHM6Ly9mMC5w/bmdmdWVsLmNvbS9w/bmcvMzU4Lzg0OS9w/b3N0Z3Jlc3FsLWRh/dGFiYXNlLWxvZ28t/ZGF0YWJhc2Utc3lt/Ym9sLXBuZy1jbGlw/LWFydC5wbmc" width="120"/>
<img src="https://imgs.search.brave.com/aIQfJNcyjtD66ejoNc_b2zTu6F4NZpkkVro94pVuSMQ/rs:fit:400:400:1/g:ce/aHR0cHM6Ly9hdmF0/YXJzMy5naXRodWJ1/c2VyY29udGVudC5j/b20vdS8xMjEwMTUz/Nj9zPTQwMCZ2PTQ" width="120"/>
<img src="https://imgs.search.brave.com/LlrNNx6xfdbxaztqXLrJzVuJiy0et668gicjHZJXtXE/rs:fit:1200:889:1/g:ce/aHR0cHM6Ly9xdWlu/dGFncm91cC5jb20v/Y21zL3RlY2hub2xv/Z3kvSW1hZ2VzL2Rv/Y2tlci1jb21wb3Nl/LWJ1dHRvbi5qcGc" width="120"/>
<img src="https://imgs.search.brave.com/TLS6tVWPyhKYKzDTYqOHvHmdfFMom4q7OsK2rr36-gY/rs:fit:1200:1200:1/g:ce/aHR0cHM6Ly9sb2dv/cy1kb3dubG9hZC5j/b20vd3AtY29udGVu/dC91cGxvYWRzLzIw/MTYvMDUvTXlTUUxf/bG9nb19sb2dvdHlw/ZS5wbmc" width="120"/>
<img src="https://imgs.search.brave.com/YZgeWSeISTNtIUvuaeiNNb0GV8p1K6BxoDZfmJQ2YFo/rs:fit:1200:1080:1/g:ce/aHR0cDovL3RvdXMt/bG9nb3MuY29tL3dw/LWNvbnRlbnQvdXBs/b2Fkcy8yMDE4LzA0/L0xvZ28tRG9ja2Vy/LnBuZw" width="120"/>
<img src="https://imgs.search.brave.com/1CC1-sPoaPmkl2RZP7nbdO2a2ft99Ht1mONaSL9IqX4/rs:fit:509:219:1/g:ce/aHR0cHM6Ly93d3cu/c2NydW1kZXNrLmNv/bS93cC1jb250ZW50/L3VwbG9hZHMvamly/YS1sb2dvLWdyYWRp/ZW50LWJsdWVAMngt/MS5wbmc" width="120"/>
<img src="https://imgs.search.brave.com/vsXisHS1ps4atdgb8PTFAth6kT62rxfVJrvSErRku-Y/rs:fit:300:300:1/g:ce/aHR0cHM6Ly9kYXZp/c2Vmb3JkLmNvbS9i/bG9nL3B1YmxpYy9p/bWcvdGh1bWJuYWls/cy9taXNjL3JlYWN0/LWxvZ28ucG5n" width="120"/>

|                                            Technology                                            | Description                             |
|:------------------------------------------------------------------------------------------------:|-----------------------------------------|
|                            [React](https://github.com/facebook/react)                            | Frontend library                        |
|                         [Keycloak](https://github.com/keycloak/keycloak)                         | Oauth2 server                           |
|                                [Ant design](https://ant.design/)                                 | React components library                |
|          [Spring boot admin](https://codecentric.github.io/spring-boot-admin/current/)           | Monitoring user interface               |
|                        [Docker compose](https://docs.docker.com/compose/)                        | Docker container orchestration          |
| [Spring cloud gateway](https://docs.spring.io/spring-cloud-gateway/docs/current/reference/html/) | Micro services proxy and authentication |
|                               [Mongo db](https://www.mongodb.com/)                               | Patient's notes storage                 |
|                            [PostgreSQL](https://www.postgresql.org/)                             | Patient's, rules and keywords storage   |
|                        [Jira](https://www.atlassian.com/fr/software/jira)                        | Project management                      |
|                               [Liquibase](https://liquibase.org/)                                | Postgresql database versioning          |
|                                  [Mongock](https://mongock.io/)                                  | Mongodb database versioning             |

### Architecture

![](./src/main/resources/static/architecture.jpg)
---


### Exécuter le projet en local :

Pré-requis:

- Unix or Windows OS
- docker engine
- docker compose

Dans un terminal
```shell
git clone https://gitlab.com/abdoum/P9_DA_JAVA.git
cd P9_DA_JAVA
docker-compose up
```
![](src/main/resources/static/run_app.gif)

#### Points d'accès des differents services

|     Service      | Point d'accès                                                                                              | pseudo       | mot de passe |
|:----------------:|------------------------------------------------------------------------------------------------------------|--------------|--------------|
| Springboot admin | [http://localhost:8082](http://localhost:8082)                                                             | admin        | admin        |
|      Zipkin      | [http://localhost:9411](http://localhost:9411)                                                             |              |              |
|    Swagger ui    | [http://localhost:8081/webjars/swagger-ui/index.html](http://localhost:8081/webjars/swagger-ui/index.html) |              |              |
|      Eureka      | [http://localhost:9102](http://localhost:9102)                                                             |              |              |
|     Keycloak     | [http://localhost:8080](http://localhost:8080)                                                             | practitioner | practitioner |
|     Frontend     | [http://localhost:3000](http://localhost:3000)                                                             | admin        |   admin      |

### Bonus

---

#### Algorithme de traitement des règles définies par l'utilisateur
![](./src/main/resources/static/tree.jpg)

#### Générer des rapport de couverture de tests
![](src/main/resources/static/test_coverage.gif)

#### Modifier un patient
![](src/main/resources/static/edit_patient.gif)