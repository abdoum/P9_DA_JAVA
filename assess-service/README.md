![](https://img.shields.io/badge/Java-ED8B00?style=for-the-badge&logo=java&logoColor=white)
![](https://img.shields.io/badge/Spring-6DB33F?style=for-the-badge&logo=spring&logoColor=white)
![](/gitlab/last-commit/gitlab-org/gitlab-development-kit)
![](/maven/v/metadata-url/repo1.maven.org/maven2/com/google/code/gson/gson/maven-metadata.xml)

# Getting Started

### Reference Documentation

[[_TOC_]]

### Description

Le microservice _Assess_ prends en charge la génération des rapports.

![](/Users/dr/Documents/Formation/openclassrooms/projets/P9_DA_JAVA/code/clinique/patient/src/main/resources/Template+de+spécifications+API+REST+P9.pdf)

![](/Users/dr/Documents/Formation/openclassrooms/projets/P9_DA_JAVA/code/clinique/patient/src/main/resources/architecture.jpg)

### Pré-requis

----

- Unix or Windows OS
- [Docker engine](https://docs.docker.com/get-started/)
- Docker compose

[Spécificités de l'api rest](https://docs.google.com/document/d/e/2PACX-1vQcbPv70DRCSISpQ8rJ4hqp5wTlkPz8oem8RU-qPT8xrTDBawMGSlXbit1TlOImftsT0kxQ6Uex3b5y/pub)

### Rétrospective sprint 3

---
Ce qui s’est bien passé :

* Écriture des tests unitaires
* Implémentation de la base de donnée Postgresql
* Implémentation de l'algorithme de détermination du résultat
* Implémentation du microservice assess

Ce qui aurait pu aller mieux :

* La Dockerisation présentait un problème lié à une limite de mémoire du docker engine,
* crash aléatoires
* Organisation du repository git (les commits ne sont pas limités aux taches demandées)

Ce que j’aimerais faire différemment :

* Limiter les tâches accomplies å celles mentionnées sur l’issue
* Automatiser la génération de la documentation de l’api rest (utiliser swagger ui)
* Revenir systématiquement vers les tests unitaire après chaque modification dans le code pour ne pas introduire de
  régressions