package com.abernathe.assess.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EntityAlreadyExistsException extends Exception {

    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new Entity already exists exception.
     */
    public EntityAlreadyExistsException() {
        super();
    }

    /**
     * Instantiates a new Entity already exists exception.
     *
     * @param errorMessage the error message
     */
    public EntityAlreadyExistsException(String errorMessage) {
        super(errorMessage);
        log.error("errorMessage = " + errorMessage);
    }
}