package com.abernathe.assess.model;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Assess {

    /**
     * The Patient first name.
     */
    @NotBlank
    @Length(min = 2, max = 50)
    String patientFirstName;

    /**
     * The Patient family.
     */
    @NotBlank
    @Length(min = 2, max = 50)
    String patientFamily;

    /**
     * The Age.
     */
    @PositiveOrZero(message = "Age must be equal or greater than zero")
    int age;

    /**
     * The Assess result.
     */
    @NotNull
    AssessResult assessResult;
}