package com.abernathe.assess.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

import static com.abernathe.assess.model.AssessResult.NONE;

@SuppressWarnings("unchecked")
public class Tree<T extends Comparable<T>> {

    private Node<T> root;

    /**
     * Instantiates a new Tree.
     *
     * @param value the value
     */
    public Tree(T value) {
        root = new Node<>(value);
    }

    /**
     * Instantiates a new Tree.
     *
     * @param rootValue the root value
     * @param minAlerts the min alerts
     * @param maxAlerts the max alerts
     */
    public Tree(T rootValue, T minAlerts, T maxAlerts) {
        root = new Node<>(rootValue);
        generateTree(minAlerts, maxAlerts);
    }

    /**
     * Sets root.
     *
     * @param root the root
     */
    public void setRoot(Node<T> root) {
        this.root = root;
    }

    /**
     * Gets root.
     *
     * @return the root
     */
    public Node<T> getRoot() {
        return root;
    }

    /**
     * Generates a tree with default values
     *
     * @param minAlerts the minimum alert value
     * @param maxAlerts the maximum alert value
     */
    private void generateTree(T minAlerts, T maxAlerts) {
        IntStream.rangeClosed((Integer) minAlerts, (Integer) maxAlerts).forEach(i -> {
            Node<T> alertNode = addAgeNodes(root, i);
            boolean isChildrenEmpty = alertNode.getChildren().isEmpty();
            //region Add age condition nodes
            Node<T> ageAboveLimitNode = addAgeNodes((isChildrenEmpty ? alertNode :
                    alertNode.getLastChild()), 1);
            Node<T> equalToLimitNode = addAgeNodes((isChildrenEmpty ? alertNode :
                    alertNode.getLastChild()), 0);
            Node<T> ageBelowLimitNode = addAgeNodes((isChildrenEmpty ? alertNode :
                    alertNode.getLastChild()), -1);
            //endregion
            //region Add genre condition nodes
            List<Node<T>> nodesList = List.of(ageAboveLimitNode, equalToLimitNode, ageBelowLimitNode);
            addGenreNodes(nodesList);
            //endregion
            //region Set default results
            addDefaultResultNodes(nodesList);
            //endregion
        });
    }

    /**
     * Adds age node to the tree
     *
     * @param from  the node where to start the insert
     * @param value the node value
     * @return the inserted node
     */
    private Node<T> addAgeNodes(Node<T> from, int value) {
        return add(from, (T) (Integer) value);
    }

    /**
     * Generates the default result nodes
     *
     * @param ageAboveLimitNode the age above limit node
     * @param ageBelowLimitNode the age below limit node
     */
    private void addDefaultResultNodes(List<Node<T>> nodeList) {
        nodeList.forEach(n -> n.getChildren().forEach(node -> add(node, (T) (Integer) NONE.ordinal())));
    }

    /**
     * Generates 0 and 1 nodes corresponding to Male and Female ordinal values
     *
     * @param nodeList the node list to generate children for
     */
    private void addGenreNodes(List<Node<T>> nodeList) {
        nodeList.forEach(node -> node.getChildren().addAll(List.of((Node<T>) new Node<>(0), (Node<T>) new Node<>(1))));
    }

    /**
     * Gets the lowest node in the tree
     *
     * @return the lowest node
     */
    public T getMin() {
        return this.getRoot().getChildren().get(0).getValue();
    }

    /**
     * Adds a node to the tree
     *
     * @param from from where to insert the given node
     * @param node the node to be inserted
     * @return the addded node
     */
    public Node<T> add(Node<T> from, T node) {
        if (root == null) {
            return root = new Node<>(node);
        }
        from.getChildren().add(new Node<>(node));
        return from.getChildren().get(from.getChildren().size() - 1);
    }

    /**
     * Finds a node with a given value
     *
     * @param from  from wich node to start the search
     * @param value the value to search for
     * @return the found node
     */
    public Node<T> find(Node<T> from, T value) {
        if (from == null) {
            return null;
        }
        else if (from.getChildren().isEmpty()) {
            return from;
        }
        else {
            return from.getChildren().stream()
                       .filter(node -> node.getValue().equals(value))
                       .findFirst()
                       .orElse(from.getLastChild());
        }
    }

    /**
     * Gets the result value for a list of criteria
     *
     * @param args the list of criteria corresponding to the processed patient
     * @return assess result ordinal value
     */
    public T getResult(List<T> args) {
        Node<T> result = root;
        for (T i : args) {
            result = find(result, i);
        }
        return result.getLastChild().getValue();
    }

    /**
     * Gets children for case.
     *
     * @param args the args
     * @param max  the max
     * @return the children for case
     */
    public Set<Node<T>> getChildrenForCase(ArrayList<Integer> args, Integer max) {
        Set<Node<T>> nodeList = new HashSet<>();
        var i = args.get(0);
        while (i.compareTo(max) < 1) {
            Node<T> currentNode = root;
            args.set(0, i);
            for (Integer arg : args) {
                currentNode = find(currentNode, (T) arg);
            }
            if (currentNode != null) {
                nodeList.addAll(currentNode.getAllLeafNodes());
            }
            i++;
        }
        return nodeList;
    }

    /**
     * Retrieves the result nodes using a cases and a range extracted from a ruleSuite
     *
     * @param args  the case value
     * @param range the range extracted from the ruleSuite
     * @return the set of corresponding nodes
     */
    public Set<Node<T>> getChildrenForCaseAndRange(ArrayList<Integer> args, IntStream range) {
        Set<Node<T>> nodeList = new HashSet<>();
        range.forEach(el -> {
            Node<T> currentNode = root;
            args.set(0, el);
            for (Integer arg : args) {
                currentNode = find(currentNode, (T) arg);
            }
            if (currentNode != null) {
                nodeList.addAll(currentNode.getAllLeafNodes());
            }
        });
        return nodeList;
    }

    /**
     * Gets the greatest tree node
     *
     * @return the maximum node
     */
    public T getTreeMax() {
        return getRoot().getLastChild().getValue();
    }

    @Override public String toString() {
        return "Tree{" +
               "root=" + root +
               '}';
    }
}