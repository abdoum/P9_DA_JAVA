package com.abernathe.assess.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Table(name = "rule")
@Entity
@RequiredArgsConstructor
@Data
public class RuleSuite {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Length(min = 32, max = 150)
    @NotBlank(message = "Rule content cannot be empty")
    @Column(name = "content")
    private String content;

    public RuleSuite(String content) {
        this.content = content;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setContent(String content) {
        this.content = content.trim();
    }
}