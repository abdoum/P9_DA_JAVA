package com.abernathe.assess.model;

public enum
AssessResult {
    NONE,
    BORDERLINE,
    IN_DANGER,
    EARLY_ON_SET
}