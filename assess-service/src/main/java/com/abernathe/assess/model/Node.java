package com.abernathe.assess.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Node<T extends Comparable<T>> {

    private T value;

    private List<Node<T>> children = new ArrayList<>();

    /**
     * Gets value.
     *
     * @return the value
     */
    public T getValue() {
        return value;
    }

    /**
     * Sets value.
     *
     * @param value the value
     */
    public void setValue(T value) {
        this.value = value;
    }

    /**
     * Sets children.
     *
     * @param children the children
     */
    public void setChildren(List<Node<T>> children) {
        this.children = children;
    }

    /**
     * Instantiates a new Node.
     *
     * @param value the value
     */
    public Node(T value) {
        this.value = value;
    }

    /**
     * Gets children.
     *
     * @return the children
     */
    public List<Node<T>> getChildren() {
        return children;
    }

    /**
     * Gets all leaf nodes.
     *
     * @return the all leaf nodes
     */
    public Set<Node<T>> getAllLeafNodes() {
        Set<Node<T>> leafNodes = new HashSet<>();
        if (this.getChildren().isEmpty()) {
            leafNodes.add(this);
        }
        else {
            leafNodes = this.getChildren()
                            .stream()
                            .flatMap(child -> child.getAllLeafNodes().stream())
                            .collect(Collectors.toSet());
        }
        return leafNodes;
    }

    /**
     * Gets last child.
     *
     * @return the last child
     */
    public Node<T> getLastChild() {
        if (children.isEmpty()) {
            return null;
        }
        return children.get(children.size() - 1);
    }

    @Override public String toString() {
        if (children.isEmpty()) {
            return "Node{" +
                   "value=" + value +
                   '}';
        }
        else {
            return "Node{" +
                   "value=" + value +
                   ", children=" + children +
                   '}';
        }
    }
}