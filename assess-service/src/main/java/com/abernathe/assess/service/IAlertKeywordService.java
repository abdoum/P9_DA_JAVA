package com.abernathe.assess.service;

import com.abernathe.assess.exception.EntityAlreadyExistsException;
import com.abernathe.assess.exception.EntityNotFoundException;
import com.abernathe.assess.model.AlertKeyword;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;

@Validated
public interface IAlertKeywordService {

    /**
     * Save alert keyword.
     *
     * @param alertKeyword the alert keyword
     * @return the alert keyword
     * @throws EntityAlreadyExistsException the entity already exists exception
     */
    AlertKeyword save(@Valid AlertKeyword alertKeyword) throws EntityAlreadyExistsException;

    /**
     * Deletes an alertKeyword.
     *
     * @param id the id
     * @throws EntityNotFoundException the entity not found exception
     */
    void delete(Integer id) throws EntityNotFoundException;

    /**
     * Update alert keyword.
     *
     * @param alertKeyword the alert keyword
     * @return the alert keyword
     * @throws EntityNotFoundException the entity not found exception
     */
    AlertKeyword update(@Valid AlertKeyword alertKeyword) throws EntityNotFoundException;

    /**
     * Find all alertKeywords.
     *
     * @return the list
     */
    List<AlertKeyword> findAll();
}