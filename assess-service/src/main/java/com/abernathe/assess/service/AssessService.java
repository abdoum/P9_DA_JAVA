package com.abernathe.assess.service;

import com.abernathe.assess.bean.GenreBean;
import com.abernathe.assess.bean.NoteBean;
import com.abernathe.assess.bean.PatientBean;
import com.abernathe.assess.exception.EntityNotFoundException;
import com.abernathe.assess.model.AlertKeyword;
import com.abernathe.assess.model.Assess;
import com.abernathe.assess.model.AssessResult;
import com.abernathe.assess.proxy.INoteProxy;
import com.abernathe.assess.proxy.IPatientProxy;
import com.abernathe.assess.repository.IAlertKeywordDAO;
import com.abernathe.assess.util.IRuleOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@Slf4j
public class AssessService implements IAssessService {

    private final INoteProxy iNoteProxy;

    private final IPatientProxy iPatientProxy;

    private final IAlertKeywordDAO iAlertKeywordDAO;

    private final IRuleOperation iRuleOperation;

    public AssessService(INoteProxy iNoteProxy,
                         IPatientProxy iPatientProxy,
                         IAlertKeywordDAO iAlertKeywordDAO,
                         IRuleOperation iRuleOperation) {
        this.iNoteProxy       = iNoteProxy;
        this.iPatientProxy    = iPatientProxy;
        this.iAlertKeywordDAO = iAlertKeywordDAO;
        this.iRuleOperation   = iRuleOperation;
    }

    /**
     * Generates assess for a patient based on his family name
     *
     * @param patientId the patient's id
     * @return the generated assess
     * @throws EntityNotFoundException if the patient is not found with given id
     */
    @Override public Assess generateByPatientId(long patientId) throws EntityNotFoundException {
        var notes = iNoteProxy.getHistoryByPatientId(patientId);
        log.debug(notes.toString());
        Optional<PatientBean> body = iPatientProxy.findById(patientId)
                                                  .getBody();
        var patient = Objects.requireNonNull(body)
                             .orElseThrow(() -> new EntityNotFoundException(String.format("patient with " +
                                                                                          "id %s" +
                                                                                          " " +
                                                                                          "could not " +
                                                                                          "be found",
                                     patientId)));

        return getAssess(notes, patient);
    }

    private Assess getAssess(List<NoteBean> notes, PatientBean patient) {
        int alertsCount = getAlertCount(notes);
        log.debug(String.valueOf(alertsCount));
        AssessResult assessResult = getAssessResult(patient, alertsCount);
        return new Assess(patient.getGiven(), patient.getFamily(), patient.getAge(), assessResult);
    }

    @Override
    public Assess getResult(Long patientId) throws EntityNotFoundException {
        Optional<PatientBean> body = iPatientProxy.findById(patientId)
                                                  .getBody();
        var patient = Objects.requireNonNull(body)
                             .orElseThrow(() -> new EntityNotFoundException(String.format("patient with " +
                                                                                          "id %s" +
                                                                                          " " +
                                                                                          "could not " +
                                                                                          "be found",
                                     patientId)));
        List<NoteBean> notes = iNoteProxy.getHistoryByPatientId(patientId);
        int alertCount = this.getAlertCount(notes);
        log.debug("alertCount = " + alertCount);
        int resultForPatient = iRuleOperation.getResultForPatient(patient, alertCount);
        log.debug("resultForPatient = " + resultForPatient);
        AssessResult result = AssessResult.values()[resultForPatient];
        return new Assess(patient.getGiven(), patient.getFamily(), patient.getAge(), result);
    }

    /**
     * Count the keywords in the patients notes
     *
     * @param notes the patient's notes
     * @return int the keywords count
     */
    public int getAlertCount(List<NoteBean> notes) {

        String alertKeyWords = iAlertKeywordDAO.findAll()
                                               .stream()
                                               .map(AlertKeyword::getContent)
                                               .collect(Collectors.joining("|"));
        log.info("alertKeyWords = " + alertKeyWords);
        Pattern pattern = Pattern.compile(
                alertKeyWords,
                Pattern.UNICODE_CASE | Pattern.CANON_EQ | Pattern.CASE_INSENSITIVE);
        return notes.parallelStream()
                    .map(note -> {
                        Matcher matches = pattern.matcher(note.getContent());
                        int foundMatches = 0;
                        while (matches.find()) {
                            foundMatches += 1;
                        }
                        return foundMatches;
                    }).mapToInt(Integer::intValue).sum();
    }

    /**
     * Determines asses result based on patient age, sex and number of alert keywords
     *
     * @param patient     the patient
     * @param alertsCount the number of alert keywords
     * @return assessResult the resulting asses type
     */
    private AssessResult getAssessResult(PatientBean patient, Integer alertsCount) {
        int age = patient.getAge();
        AssessResult assessResult = AssessResult.NONE;
        GenreBean sex = patient.getSex();
        if (age < 30) {
            assessResult = getAssessResultForAgeLessThan30(alertsCount, sex);
        }
        else if (age > 30) {
            assessResult = getAssessResultForAgeGreaterThan30(alertsCount);
        }
        log.debug(assessResult.toString());
        return assessResult;
    }

    /**
     * Return assess result based on keyword alerts count
     *
     * @param alertsCount number of detected keywords inside the patient's notes
     * @return assessResult assess result
     * @see AssessResult
     */
    private AssessResult getAssessResultForAgeGreaterThan30(Integer alertsCount) {
        AssessResult assessResult = AssessResult.NONE;
        if (alertsCount >= 6 && alertsCount < 8) {
            assessResult = AssessResult.IN_DANGER;
        }
        else if (alertsCount >= 8) {
            assessResult = AssessResult.EARLY_ON_SET;
        }
        return assessResult;
    }

    /**
     * Return assess result based on keyword alerts count for a patient with age less than 30 years
     *
     * @param alertsCount number of detected keywords inside the patient's notes
     * @param sex         the sex of the patient
     * @return assessResult assess result
     * @see AssessResult
     */
    private AssessResult getAssessResultForAgeLessThan30(Integer alertsCount, GenreBean sex) {
        AssessResult assessResult = AssessResult.NONE;
        switch (alertsCount) {
            case 2 -> assessResult = AssessResult.BORDERLINE;
            case 3 -> {
                assessResult = AssessResult.BORDERLINE;
                if (sex == GenreBean.M) {
                    assessResult = AssessResult.IN_DANGER;
                }
            }
            case 4 -> assessResult = AssessResult.IN_DANGER;
            case 5, 6 -> {
                assessResult = AssessResult.IN_DANGER;
                if (sex == GenreBean.M) {
                    assessResult = AssessResult.EARLY_ON_SET;
                }
            }
            case 7 -> assessResult = AssessResult.EARLY_ON_SET;
            default -> log.info("default case");
        }
        return assessResult;
    }

    /**
     * Generates assess for a patient based on his family name. If multiple patients have the same family name, it
     * will use the first matching patient.
     *
     * @param family the patient's family name
     * @return assess the generated assess
     * @throws EntityNotFoundException if the patient is not found with given id
     */
    @Override public Assess generateByPatientFamily(String family) throws EntityNotFoundException {
        var notes = iNoteProxy.getHistoryByPatientFamily(family);
        log.debug(notes.toString());
        List<PatientBean> patientBeanList =
                iPatientProxy.findByFamilyName(family)
                             .getBody()
                             .orElseThrow(() -> new EntityNotFoundException(String.format("patient with " +
                                                                                          "family name %s" +
                                                                                          " " +
                                                                                          "could not " +
                                                                                          "be found", family)));
        log.debug("body = " + patientBeanList);
        if (patientBeanList.isEmpty()) {
            throw new EntityNotFoundException(String.format("patient with " +
                                                            "family name %s" +
                                                            " " +
                                                            "could not " +
                                                            "be found", family));
        }
        else {
            return getAssess(notes, patientBeanList.get(0));
        }
    }
}