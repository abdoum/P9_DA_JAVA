package com.abernathe.assess.service;

import com.abernathe.assess.exception.EntityAlreadyExistsException;
import com.abernathe.assess.exception.EntityNotFoundException;
import com.abernathe.assess.model.AlertKeyword;
import com.abernathe.assess.repository.IAlertKeywordDAO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class AlertKeywordServiceImpl implements IAlertKeywordService {

    private final IAlertKeywordDAO iAlertKeywordDAO;

    public AlertKeywordServiceImpl(IAlertKeywordDAO iAlertKeywordDAO) {
        this.iAlertKeywordDAO = iAlertKeywordDAO;
    }

    /**
     * Saves an alert keyword to database
     *
     * @param alertKeyword the alert keyword
     * @return the saved alert keyword
     */
    @Override
    public AlertKeyword save(AlertKeyword alertKeyword) {
        Optional<AlertKeyword> isExisting = iAlertKeywordDAO.getByContent(alertKeyword.getContent());
        isExisting.ifPresent(alertKeyword1 -> {
            try {
                throw new EntityAlreadyExistsException(
                        String.format("AlertKeyWordAlready " + "exists %s", alertKeyword));
            }
            catch (EntityAlreadyExistsException e) {
                e.printStackTrace();
            }
        });

        AlertKeyword alertKeywordToBePersisted = new AlertKeyword();
        alertKeywordToBePersisted.setContent(alertKeyword.getContent());
        return iAlertKeywordDAO.save(alertKeywordToBePersisted);
    }

    /**
     * Deletes an alert keyword from database
     *
     * @param id the alert keyword id to be deleted
     * @throws EntityNotFoundException if the keyword could not be found
     */
    @Override
    public void delete(Integer id) throws EntityNotFoundException {
        Optional<AlertKeyword> foundAlertKeyword = iAlertKeywordDAO.findById(id);
        log.debug("{}", foundAlertKeyword);
        if (foundAlertKeyword.isEmpty()) {
            throw new EntityNotFoundException(
                    String.format("Alert keyword with id %d could not be found", id));
        }

        foundAlertKeyword.ifPresent(keyword -> iAlertKeywordDAO.deleteById(id));
    }

    /**
     * Updates an alert keyword in database
     *
     * @param alertKeyword the alert keyword to update
     * @return the updated alert keyword
     * @throws EntityNotFoundException if the alert keyword does not exist already
     */
    @Transactional
    @Override
    public AlertKeyword update(AlertKeyword alertKeyword) throws EntityNotFoundException {
        Optional<AlertKeyword> existingAlertKeyword = iAlertKeywordDAO.findById(alertKeyword.getId());
        log.debug("{}", existingAlertKeyword);
        if (existingAlertKeyword.isEmpty()) {
            throw new EntityNotFoundException(
                    String.format("Alert keyword with id %d could not be found", alertKeyword.getId()));
        }
        AlertKeyword alertKeywordToBePersisted = new AlertKeyword(alertKeyword.getContent());
        log.debug("{}", alertKeywordToBePersisted);
        return iAlertKeywordDAO.save(alertKeywordToBePersisted);
    }

    /**
     * Finds all the alert keywords
     *
     * @return List of AlertKeyword
     */
    @Override
    public List<AlertKeyword> findAll() {
        return iAlertKeywordDAO.findAll();
    }
}