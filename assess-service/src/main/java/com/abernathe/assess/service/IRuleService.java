package com.abernathe.assess.service;

import com.abernathe.assess.exception.EntityAlreadyExistsException;
import com.abernathe.assess.exception.EntityNotFoundException;
import com.abernathe.assess.model.RuleSuite;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;


@Validated
public interface IRuleService {

    /**
     * Save rule suite.
     *
     * @param ruleSuite the rule suite
     * @return the rule suite
     */
    RuleSuite save(@Valid RuleSuite ruleSuite) throws EntityAlreadyExistsException;

    /**
     * Delete.
     *
     * @param id the id
     * @throws EntityNotFoundException the entity not found exception
     */
    void delete(Integer id) throws EntityNotFoundException;

    /**
     * Find all list.
     *
     * @return the list
     */
    List<RuleSuite> findAll();
}