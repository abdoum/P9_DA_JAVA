package com.abernathe.assess.service;

import com.abernathe.assess.bean.NoteBean;
import com.abernathe.assess.exception.EntityNotFoundException;
import com.abernathe.assess.model.Assess;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;

@Validated
public interface IAssessService {

    Assess generateByPatientId(@Valid long patientId) throws EntityNotFoundException;

    Assess generateByPatientFamily(@Valid String family) throws EntityNotFoundException;

    Assess getResult(Long patientId) throws EntityNotFoundException;

    int getAlertCount(List<NoteBean> notes);
}