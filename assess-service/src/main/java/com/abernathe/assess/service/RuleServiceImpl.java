package com.abernathe.assess.service;

import com.abernathe.assess.exception.EntityAlreadyExistsException;
import com.abernathe.assess.exception.EntityNotFoundException;
import com.abernathe.assess.model.RuleSuite;
import com.abernathe.assess.repository.IRuleSuiteDAO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static java.lang.String.format;

@Service
public class RuleServiceImpl implements IRuleService {

    private final IRuleSuiteDAO iRuleSuiteDAO;

    public RuleServiceImpl(IRuleSuiteDAO iRuleSuiteDAO) {
        this.iRuleSuiteDAO = iRuleSuiteDAO;
    }

    /**
     * Saves a rule suite to database
     *
     * @param ruleSuite the rule suite to be saved
     * @return the saved rule suite
     */
    @Override
    public RuleSuite save(RuleSuite ruleSuite) throws EntityAlreadyExistsException {
        RuleSuite ruleSuiteToBePersisted = new RuleSuite();
        if (iRuleSuiteDAO.findByContent(ruleSuite.getContent()).isPresent()) {
            throw new EntityAlreadyExistsException("This rule suite has already been added");
        }
        ruleSuiteToBePersisted.setContent(ruleSuite.getContent());
        return iRuleSuiteDAO.save(ruleSuiteToBePersisted);
    }

    /**
     * Deletes a rule suite from database
     *
     * @param id the id of the rule suite to be deleted
     * @throws EntityNotFoundException if the rule suite could nod be found in the database
     */
    @Override
    public void delete(Integer id) throws EntityNotFoundException {
        Optional<RuleSuite> existingRuleSuite = iRuleSuiteDAO.findById(id);
        if (existingRuleSuite.isEmpty()) {
            throw new EntityNotFoundException(format("Rule suite with id %d was not found", id));
        }
        existingRuleSuite.ifPresent(ruleSuite -> iRuleSuiteDAO.deleteById(id));
    }

    /**
     * Finds all the rule suites in database
     *
     * @return List of RuleSuite or an empty list if none found
     */
    @Override
    public List<RuleSuite> findAll() {
        return iRuleSuiteDAO.findAll();
    }
}