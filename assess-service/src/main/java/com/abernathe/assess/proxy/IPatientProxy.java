package com.abernathe.assess.proxy;

import com.abernathe.assess.bean.PatientBean;
import com.abernathe.assess.exception.EntityNotFoundException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.util.List;
import java.util.Optional;

@Component
@FeignClient(name = "patient-service")
public interface IPatientProxy {

    @GetMapping(value = "/patients/list", produces = {MediaType.APPLICATION_JSON_VALUE})
    List<PatientBean> findAll();

    @PutMapping(value = "/patients/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    PatientBean update(@Valid @RequestBody PatientBean patient, @PathVariable("id") @Positive Long id) throws
            EntityNotFoundException;

    @GetMapping(value = "/patients/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Optional<PatientBean>> findById(@PathVariable("id") @Positive Long patientId);

    @GetMapping(value = "/patients", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Optional<List<PatientBean>>> findByFamilyName(
            @RequestParam("family") @NotBlank String patientFamilyName);
}