package com.abernathe.assess.proxy;

import com.abernathe.assess.bean.NoteBean;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.List;


@Component
@FeignClient(name = "note-service")
public interface INoteProxy {

    /**
     * Gets history by patient id.
     *
     * @param patientId the patient id
     * @return the history by patient id
     */
    @Validated
    @GetMapping(value = "/notes/{patientId}", produces = {
            MediaType.APPLICATION_JSON_VALUE,
            MediaType.APPLICATION_XML_VALUE
    })
    List<NoteBean> getHistoryByPatientId(@PathVariable("patientId") @NotNull @Positive Long patientId);

    /**
     * Gets history by patient family.
     *
     * @param patientFamily the patient family
     * @return the history by patient family
     */
    @Validated
    @GetMapping(value = "/notes", produces = {
            MediaType.APPLICATION_JSON_VALUE,
            MediaType.APPLICATION_XML_VALUE
    })
    List<NoteBean> getHistoryByPatientFamily(
            @RequestParam("family") @NotBlank @Size(min = 2, max = 50) String patientFamily);

}