package com.abernathe.assess.util;

import com.abernathe.assess.bean.PatientBean;
import com.abernathe.assess.model.RuleSuite;
import com.abernathe.assess.model.Tree;

import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.IntStream;

public interface IRuleOperation {

    /**
     * Gets all the ruleSuites from the database
     *
     * @return the found ruleSuites
     */
    List<RuleSuite> findAllRules();

    /**
     * Gets rules.
     *
     * @return the rules
     */
    List<RuleSuite> getRules();

    /**
     * Sets rules.
     *
     * @param rules the rules
     */
    void setRules(List<RuleSuite> rules);

    /**
     * Add rule to tree.
     *
     * @param rule the rule
     * @param tree the tree
     */
    void addRuleToTree(String rule, Tree<Integer> tree);

    /**
     * Gets result for patient.
     *
     * @param patientBean the patient bean
     * @param alertCount  the alert count
     * @return the result for patient
     */
    int getResultForPatient(PatientBean patientBean, int alertCount);

    /**
     * Add rules to tree tree.
     *
     * @return the tree
     */
    Tree<Integer> addRulesToTree();

    /**
     * Converts a patient's alerts keywords, age compared to age limit and sex to numerical values.
     *
     * @param patient    the patient
     * @param alertCount the alert count
     * @return the list of alertCount, age limit and sex ordinal value
     */
    List<Integer> processPatient(PatientBean patient, int alertCount);

    /**
     * Gets age limit.
     *
     * @return the age limit
     */
    int getAgeLimit();

    /**
     * Gets rule range.
     *
     * @param rule      the rule
     * @param maxAlerts the max alerts
     * @return the rule range
     */
    IntStream getRuleRange(String rule, int maxAlerts);

    /**
     * Sort rules by alerts list.
     *
     * @return the list
     */
    List<String> sortRulesByAlerts();

    /**
     * Gets rules range.
     *
     * @return the rules range
     */
    IntSummaryStatistics getRulesRange();

    /**
     * Convert rule to int list.
     *
     * @param rule the rule
     * @return the list
     */
    List<Integer> convertRuleToInt(String rule);
}