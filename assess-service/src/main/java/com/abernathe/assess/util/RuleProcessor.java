package com.abernathe.assess.util;

import com.abernathe.assess.bean.GenreBean;
import com.abernathe.assess.model.AssessResult;
import com.abernathe.assess.model.RuleSuite;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

@Component
record RuleProcessor(List<RuleSuite> ruleSuiteList) implements IRuleProcessor {

    private static final String BEAN_PACKAGE = "com.abernathe.assess.bean";

    private static final String MODEL_PACKAGE = "com.abernathe.assess.model";

    private static final String RULE_SUITE_PATTERN = "^(?<rule>(?<ruleClass>\\w+)\\.(?<ruleField>\\w+)" +
                                                     "(?<ruleOperator>[\\>\\=\\<]{1,2})?(?<ruleValue>\\w+))?" +
                                                     "(?>\\-\\>)?" +
                                                     "(?<ruleResult>\\w+)?";

    public static final String RULE_VALUE = "ruleValue";

    public static final String RULE_CLASS = "ruleClass";

    public static final String RULE_FIELD = "ruleField";

    public static final String RULE_RESULT = "ruleResult";

    public static final String RULE_OPERATOR = "ruleOperator";

    public static final String ASSESS_RESULT = "AssessResult";

    public static final String NOTE = "Note";

    public static final String PATIENT = "Patient";

    /**
     * Processes a single rule extracting the corresponding number value
     *
     * @param rule the rule to be processed
     * @return the number value
     */
    @Override public Integer processRule(String rule) {
        Matcher matcher = Pattern.compile(RULE_SUITE_PATTERN).matcher(rule);
        while (matcher.find()) {
            //region Extract rule parts
            String completeClassName = matcher.group(RULE_CLASS);
            String ruleValue = matcher.group(RULE_VALUE);
            String ruleOperator = matcher.group(RULE_OPERATOR);
            String methodName = matcher.group(RULE_FIELD);
            String ruleResult = matcher.group(RULE_RESULT);
            //endregion
            //region Find the corresponding class
            Class<?> currentClass = null;
            try {
                currentClass = getExistingClass(completeClassName == null ? ASSESS_RESULT : completeClassName);
                //region Process rule depending on type
                if (ruleResult != null) {
                    return processRuleResult(ruleResult, currentClass);
                }
                else if (isAgeRule(currentClass, methodName)) {
                    return processPatientAge(ruleOperator);
                }
                else if (completeClassName != null && completeClassName.contains(NOTE)) {
                    return Integer.parseInt(ruleValue);
                }
                else {
                    return processGenre(ruleValue);
                }
                //endregion
            }
            catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            //endregion
        }
        return null;
    }

    /**
     * Checks if the rule is an age rule
     *
     * @param currentClass the current class instance
     * @param methodName   the method called by the rule
     * @return true if it is an age rule
     */
    private boolean isAgeRule(Class<?> currentClass, String methodName) {
        return currentClass.getSimpleName().contains(PATIENT) &&
               methodName.contains("Age");
    }

    /**
     * Gets the ordinal value of the patient's sex
     *
     * @param ruleValue the patient's sex
     * @return the ordinal value
     */
    @Override public int processGenre(String ruleValue) {
        return GenreBean.valueOf(ruleValue).ordinal();
    }

    /**
     * Converts the patient age to a number returning -1 if the patient's age is under the age limit, 0 if the patient's
     * age is equal to the age limit, 1 if the patient's age is above the age limit.
     *
     * @param ruleOperator the rule's operator sign
     * @return -1, 0 or 1
     * @see Operators
     */
    private int processPatientAge(String ruleOperator) {
        Operators operator = getOperator(ruleOperator);
        return switch (operator) {
            case GREATER, GREATER_OR_EQUAL -> 1;
            case LESS, LESS_OR_EQUAL -> -1;
            case EQUAL -> 0;
        };
    }

    /**
     * Converts a rule result to its ordinal value
     *
     * @param ruleValue    the rule Result
     * @param currentClass the current class instance
     * @return the ordinal value
     * @see AssessResult
     */
    private Integer processRuleResult(String ruleValue, Class<?> currentClass) {
        return
                Arrays.stream(currentClass.getEnumConstants())
                      .filter(constant -> constant.equals(
                              AssessResult.valueOf(ruleValue)))
                      .map(o -> getAssessOrdinal(currentClass, ruleValue))
                      .findFirst()
                      .orElseThrow();
    }

    private Integer getAssessOrdinal(Class<?> finalCurrentClass, String assessRuleValue) {
        try {
            return (int) finalCurrentClass.getMethod("ordinal")
                                          .invoke(AssessResult.valueOf(assessRuleValue));
        }
        catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Gets the existing operator constant for a string operator
     *
     * @param operator the string operator value
     * @return the operator constant
     * @see Operators
     */
    @Override public Operators getOperator(String operator) {
        return Arrays.stream(Operators.values())
                     .filter(operators -> operators.getValue().equals(operator))
                     .findFirst()
                     .orElseThrow();
    }

    @Override public int getAlertsRuleValue(String rule) {
        Matcher ruleValuePattern = Pattern.compile(RULE_SUITE_PATTERN).matcher(rule);
        return ruleValuePattern.find() ? Integer.parseInt(ruleValuePattern.group(RULE_VALUE)) : 0;
    }

    @Override public String getAlertsRuleOperator(String rule) {
        Matcher ruleOperatorPattern = Pattern.compile(RULE_SUITE_PATTERN).matcher(rule);
        String operator = ">";
        if (ruleOperatorPattern.find()) {
            operator = ruleOperatorPattern.group(RULE_OPERATOR);
        }
        return operator;
    }

    /**
     * Retrieves all alerts values from a given list of ruleSuites.
     * The method uses regex to extract the values.
     *
     * @return array of int values
     */
    @Override public int[] getAlertRulesValues() {
        return ruleSuiteList.stream()
                            .map(RuleSuite::getContent)
                            .flatMapToInt(this::getAlertValue)
                            .toArray();
    }

    /**
     * Extracts a single value from alerts count rule.
     * This method uses regex.
     *
     * @param ruleSuite a list of rule suites
     * @return IntStream containing the found value
     */
    @Override public IntStream getAlertValue(String ruleSuite) {
        Matcher pattern = Pattern.compile(RULE_SUITE_PATTERN).matcher(ruleSuite);
        int alertRuleValue = 0;
        while (pattern.find()) {
            alertRuleValue = Integer.parseInt(pattern.group(RULE_VALUE));
        }
        return IntStream.of(alertRuleValue);
    }

    /**
     * Gets alerts range looking at the min and max values from the rules
     *
     * @return IntStream for the min and max range
     */
    @Override public IntStream getAlertsRange() {
        IntSummaryStatistics stats =
                ruleSuiteList.stream().map(RuleSuite::getContent).flatMapToInt(this::getAlertValue).summaryStatistics();
        return IntStream.rangeClosed(stats.getMin(), stats.getMax());
    }

    private Class<?> getExistingClass(String completeClassName) throws ClassNotFoundException {
        try {
            return Class.forName(BEAN_PACKAGE + '.' + completeClassName);
        }
        catch (ClassNotFoundException e) {
            return Class.forName(MODEL_PACKAGE + '.' + completeClassName);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        var that = (RuleProcessor) obj;
        return Objects.equals(this.ruleSuiteList, that.ruleSuiteList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ruleSuiteList);
    }

    @Override
    public String toString() {
        return "RuleProcessor[" +
               "ruleSuiteList=" + ruleSuiteList + ']';
    }

}