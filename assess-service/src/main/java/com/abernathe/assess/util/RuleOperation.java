package com.abernathe.assess.util;

import com.abernathe.assess.bean.PatientBean;
import com.abernathe.assess.model.Node;
import com.abernathe.assess.model.RuleSuite;
import com.abernathe.assess.model.Tree;
import com.abernathe.assess.service.IRuleService;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toSet;

@Component
public class RuleOperation implements IRuleOperation {

    public static final String RULE_VALUE = "ruleValue";

    public static final String RULE = "rule";

    private static final String RULE_SUITE_PATTERN = "^(?<rule>(?<ruleClass>\\w+)\\.(?<ruleField>\\w+)" +
                                                     "(?<ruleOperator>[\\>\\=\\<]{1,2})?(?<ruleValue>\\w+))?" +
                                                     "(?>\\-\\>)?" +
                                                     "(?<ruleResult>\\w+)?";

    public static final String RULE_VALUE_PATTERN = "^(?<rule>(?>\\w+)\\.(?>\\w+)+(?>[\\>\\=\\<]{1,2})" +
                                                    "(?<ruleValue>\\w+).+)";

    public static final String AGE_LIMIT_PATTERN = "^(?<rule>.+age(?>[\\>\\=\\<]{1,2})?(?<ruleValue>\\w+))?(?>\\-\\>)" +
                                                   "?" +
                                                   "(?<ruleResult>\\w+)?";

    private static final String RULE_CHAIN_SEPARATOR_PATTERN = "->|\\^";

    private List<RuleSuite> rules;

    private final RuleProcessor ruleProcessor;

    private final RuleFormatter ruleFormatter;

    private final IRuleService iRuleService;

    public RuleOperation(List<RuleSuite> rules,
                         RuleProcessor ruleProcessor,
                         RuleFormatter ruleFormatter,
                         IRuleService iRuleService) {
        this.ruleProcessor = ruleProcessor;
        this.ruleFormatter = ruleFormatter;
        this.iRuleService  = iRuleService;
        if (rules.isEmpty()) {
            this.setRules(findAllRules());
        }
        else {
            this.setRules(rules);
        }
    }

    /**
     * Gets all the ruleSuites from the database
     *
     * @return the found ruleSuites
     */
    @Override public List<RuleSuite> findAllRules() {
        return iRuleService.findAll();
    }

    /**
     * Gets the rules for the instance. If no rules have be explicitly set, then the rules stored in the database are
     * used by default
     *
     * @return the list of rules
     */
    @Override public List<RuleSuite> getRules() {
        return rules;
    }

    /**
     * Sets the rules field
     *
     * @param rules the rules to set
     */
    @Override public void setRules(List<RuleSuite> rules) {
        this.rules = rules;
    }

    /**
     * Adds a single rule to tree
     *
     * @param rule the rule to add
     * @param tree the tree
     */
    @Override public void addRuleToTree(String rule, Tree<Integer> tree) {
        //region Convert rule to int values
        List<Integer> numberValues = convertRuleToInt(rule);
        //endregion
        IntStream ruleRange = getRuleRange(rule, tree.getTreeMax());
        //region Add new values to tree
        Integer ruleValue = numberValues.get(numberValues.size() - 1);

        Set<Node<Integer>> childrenForCaseAndRange = tree.getChildrenForCaseAndRange(new ArrayList<>(numberValues),
                ruleRange);
        childrenForCaseAndRange.forEach(node -> node.setValue(ruleValue));
        //endregion
    }

    /**
     * Gets the assess value for a given patient
     *
     * @param patientBean the given patient
     * @param alertCount  alert keywords count
     * @return the assess result
     */
    @Override public int getResultForPatient(PatientBean patientBean, int alertCount) {
        List<Integer> patientValues = processPatient(patientBean, alertCount);
        Tree<Integer> tree = addRulesToTree();
        return tree.getResult(patientValues);
    }

    /**
     * Adds the ruleSuites from database to the tree
     *
     * @return the updated tree
     */
    @Override public Tree<Integer> addRulesToTree() {
        int min = getRulesRange().getMin();
        int max = getRulesRange().getMax();
        getAgeLimit();
        Tree<Integer> tree = new Tree<>(0, 0, max);
        List<String> sortedRules = sortRulesByAlerts();
        sortedRules.forEach(rule -> addRuleToTree(rule, tree));
        return tree;
    }

    /**
     * Converts patient to int values matching rules suite
     *
     * @param patient the patient to process
     * @return the number values of the patient processing result
     */
    @Override public List<Integer> processPatient(PatientBean patient, int alertCount) {
        Integer age = patient.getAge();
        Integer ageLimit = getAgeLimit();
        int ageToAgeLimit = age.compareTo(ageLimit);
        int sex = ruleProcessor.processGenre(patient.getSex().toString());
        return List.of(alertCount, ageToAgeLimit, sex);
    }

    /**
     * Finds the age limit in the rules suite
     *
     * @return the age limit found
     */
    @Override public int getAgeLimit() {
        Set<Integer> result = getRules().stream()
                                        .map(RuleSuite::getContent)
                                        .map(rule -> Pattern.compile(AGE_LIMIT_PATTERN).matcher(rule))
                                        .takeWhile(Matcher::find)
                                        .map(matcher -> Integer.parseInt(matcher.group(RULE_VALUE)))
                                        .collect(toSet());
        if (result.size() > 1) {
            throw new IllegalArgumentException("There can only be one age limit value");
        }
        return result.iterator().next();
    }

    /**
     * Get the range for a single rule
     *
     * @param rule      a rule to extract the values from
     * @param maxAlerts the maximum number of alerts in the rule suite
     * @return IntStream of the calculated range
     */
    @Override public IntStream getRuleRange(String rule, int maxAlerts) {
        Operators operatorConstant = ruleProcessor.getOperator(ruleProcessor.getAlertsRuleOperator(rule));
        int ruleValue = ruleProcessor.getAlertsRuleValue(rule);
        if (ruleValue > maxAlerts) {
            ruleValue = maxAlerts;
        }
        return switch (operatorConstant) {
            case LESS -> IntStream.range(0, ruleValue);
            case GREATER -> IntStream.rangeClosed(ruleValue + 1, maxAlerts);
            case EQUAL -> IntStream.rangeClosed(ruleValue, ruleValue);
            case GREATER_OR_EQUAL -> IntStream.rangeClosed(ruleValue, maxAlerts);
            case LESS_OR_EQUAL -> IntStream.rangeClosed(0, ruleValue);
        };
    }


    /**
     * Sorts rules by alerts value ascending
     *
     * @return List<String> The sorted list of rules
     */
    @Override public List<String> sortRulesByAlerts() {
        return getRules().stream()
                         .map(RuleSuite::getContent)
                         .map(s -> Pattern.compile(RULE_VALUE_PATTERN).matcher(s))
                         .filter(Matcher::find)
                         .sorted(Comparator.comparing(matcher -> matcher.group(RULE_VALUE)))
                         .map(matcher -> matcher.group(RULE))
                         .toList();
    }

    /**
     * Gets the min and max alerts values from the rules suite
     *
     * @return a list containing the min and max alert values respectively
     */
    @Override public IntSummaryStatistics getRulesRange() {
        return
                this.getRules().stream()
                    .map(RuleSuite::getContent)
                    .map(s -> Pattern.compile(RULE_VALUE_PATTERN)
                                     .matcher(s))
                    .filter(Matcher::find)
                    .mapToInt(matcher -> Integer.parseInt(
                            matcher.group(RULE_VALUE)))
                    .summaryStatistics();

    }

    /**
     * Converts a rule to number values
     *
     * @param rule the rule to be converted
     * @return a list of number values
     */
    @Override public List<Integer> convertRuleToInt(String rule) {
        return
                Arrays.stream(rule.split(RULE_CHAIN_SEPARATOR_PATTERN))
                      .map(singleRule -> Pattern.compile(RULE_SUITE_PATTERN).matcher(singleRule))
                      .filter(Matcher::find)
                      .flatMap(ruleFormatter::formatRule)
                      .map(ruleProcessor::processRule)
                      .toList();
    }

}