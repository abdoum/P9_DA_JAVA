package com.abernathe.assess.util;

import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.stream.Stream;

/**
 * Offers formatting methods for rules.
 */
@Component
class RuleFormatter implements IRuleFormatter {

    public static final String RULE_RESULT = "ruleResult";

    public static final String RULE_FIELD = "ruleField";

    public static final String RULE_CLASS = "ruleClass";

    public static final String RULE = "rule";

    private static final Set<String> classNames = Set.of("PatientBean", "NoteBean", "AssessResult");

    /**
     * Formats a rule
     *
     * @param matcher the pattern matcher
     * @return Stream of the formatted rule
     */
    @Override public Stream<String> formatRule(Matcher matcher) {
        String classPrefix = matcher.group(RULE_CLASS);
        String field = matcher.group(RULE_FIELD);
        if (classPrefix != null) {
            String completeClassName = getCompleteClassName(classPrefix);
            String rule;
            rule = matcher.group(RULE).replaceFirst(classPrefix, completeClassName);
            rule = rule.replaceFirst(field, getMethodName(field));
            return Stream.of(rule);
        }
        else {
            return Stream.of(matcher.group(RULE_RESULT));
        }
    }

    /**
     * Gets the complete class name from an abbreviation
     *
     * @param className the abbreviated class name
     * @return the complete class name
     */
    @Override public String getCompleteClassName(String className) {
        return classNames.stream()
                         .filter(startsWith(className))
                         .findFirst()
                         .orElse("");
    }

    /**
     * Checks if one of the class names starts with the given class prefix name
     *
     * @param prefix the class prefix to check against
     * @return true if the class name starts with the given prefix
     */
    private Predicate<String> startsWith(String prefix) {
        return className -> className.startsWith(prefix);
    }

    /**
     * Formats the method name
     *
     * @param field the field
     * @return the complete method name
     */
    @Override public String getMethodName(String field) {
        return "get" + Character.toUpperCase(field.charAt(0)) + field.substring(1);
    }
}