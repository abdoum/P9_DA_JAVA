package com.abernathe.assess.util;

import java.util.stream.IntStream;

public interface IRuleProcessor {

    Integer processRule(String rule);

    int processGenre(String ruleValue);

    Operators getOperator(String operator);

    int getAlertsRuleValue(String rule);

    String getAlertsRuleOperator(String rule);

    int[] getAlertRulesValues();

    IntStream getAlertValue(String ruleSuite);

    IntStream getAlertsRange();
}