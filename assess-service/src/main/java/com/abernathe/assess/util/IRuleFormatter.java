package com.abernathe.assess.util;

import java.util.regex.Matcher;
import java.util.stream.Stream;

public interface IRuleFormatter {

    Stream<String> formatRule(Matcher matcher);

    String getCompleteClassName(String className);

    String getMethodName(String field);
}