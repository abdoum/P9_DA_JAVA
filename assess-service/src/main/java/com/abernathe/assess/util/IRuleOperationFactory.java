package com.abernathe.assess.util;

import com.abernathe.assess.bean.PatientBean;
import com.abernathe.assess.model.RuleSuite;
import com.abernathe.assess.model.Tree;

import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.IntStream;

public interface IRuleOperationFactory {

    List<RuleSuite> getRules();

    void setRules(List<RuleSuite> rules);

    void addRuleToTree(String rule, Tree<Integer> tree);

    int getResultForPatient(PatientBean patientBean, int alertCount);

    Tree<Integer> addRulesToTree();

    List<Integer> processPatient(PatientBean patient, int alertCount);

    int getAgeLimit();

    IntStream getRuleRange(String rule, int maxAlerts);

    List<String> sortRulesByAlerts();

    IntSummaryStatistics getRulesRange();

    List<Integer> convertRuleToInt(String rule);
}