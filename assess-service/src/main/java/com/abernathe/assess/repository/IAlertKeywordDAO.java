package com.abernathe.assess.repository;

import com.abernathe.assess.model.AlertKeyword;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IAlertKeywordDAO extends JpaRepository<AlertKeyword, Integer> {

    Optional<AlertKeyword> getByContent(String content);
}