package com.abernathe.assess.repository;

import com.abernathe.assess.model.RuleSuite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IRuleSuiteDAO extends JpaRepository<RuleSuite, Integer> {

    Optional<RuleSuite> findByContent(String content);
}