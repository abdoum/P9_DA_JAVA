package com.abernathe.assess.controller;

import com.abernathe.assess.exception.EntityAlreadyExistsException;
import com.abernathe.assess.exception.EntityNotFoundException;
import com.abernathe.assess.model.AlertKeyword;
import com.abernathe.assess.service.IAlertKeywordService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Validated
@CrossOrigin(allowedHeaders = "*", origins = "*")
@RequestMapping("/api/v1/alert-keywords")
@Slf4j
public class AlertKeywordController {

    private final IAlertKeywordService iAlertKeywordService;

    public AlertKeywordController(IAlertKeywordService iAlertKeywordService) {
        this.iAlertKeywordService = iAlertKeywordService;
    }

    /**
     * Gets all the alerts keywords from database
     *
     * @return List of alert keywords or an empty list if no alert keywords found
     */
    @Operation(summary = "Finds all the alert keywords.",
               description = "Finds all the existing alert keywords.")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "The list of alert keywords")
            }
    )
    @GetMapping("")
    List<AlertKeyword> findAll() {
        log.info("get all alert keywords request");
        List<AlertKeyword> alertKeywords = iAlertKeywordService.findAll();
        log.debug("alertKeywords = " + alertKeywords);
        return alertKeywords;
    }

    /**
     * Saves an alert keyword to database
     *
     * @param keyword the alert keyword to be saved
     * @return the saved alert keyword
     */

    @Operation(summary = "Save an alert keywords.",
               description = "Adds a new alert keyword")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "The added alert keywords")
            }
    ) @PostMapping("")
    AlertKeyword save(@RequestBody AlertKeyword keyword) throws EntityAlreadyExistsException {
        log.info("keyword = " + keyword);
        AlertKeyword savedAlertKeyword = iAlertKeywordService.save(keyword);
        log.info("savedAlertKeyword = " + savedAlertKeyword);
        return savedAlertKeyword;
    }

    /**
     * Deletes an alert keyword from database
     *
     * @param id the id of the alert keyword to be deleted
     * @throws EntityNotFoundException if the alert keyword does not exist in the database
     */
    @Operation(summary = "Deletes an alert keywords.",
               description = "Deletes an alert keywords.")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Empty response")
            }
    )
    @DeleteMapping("/{id}")
    void delete(@PathVariable("id") Integer id) throws EntityNotFoundException {
        log.info("delete request for id = " + id);
        iAlertKeywordService.delete(id);
    }
}