package com.abernathe.assess.controller;

import com.abernathe.assess.exception.EntityNotFoundException;
import com.abernathe.assess.model.Assess;
import com.abernathe.assess.service.IAssessService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@RestController
@Validated
@CrossOrigin(allowedHeaders = "*", origins = "*")
@RequestMapping("reports")
@Slf4j
public class AssessController {


    private final IAssessService iAssessService;

    public AssessController(IAssessService iAssessService) {
        this.iAssessService = iAssessService;
    }

    /**
     * Generates diabetes assess for a patient. Returns the found assess on success.
     *
     * @param patientId the patient id
     * @return Assess  the generated assess
     */
    @Operation(summary = "Generates diabetes assess for a patient id.",
               description = "Generates assess for a patient id. Returns the generated assess on success.")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "The generated assess")
            }
    )
    @Validated
    @GetMapping(value = "/{patientId}")
    public Assess getAssesByPatientId(@PathVariable("patientId") @NotNull @Positive Long patientId) throws
            EntityNotFoundException {
        log.info(String.valueOf(patientId));
        Assess assess = iAssessService.getResult(patientId);
        if (assess != null) {
            log.info(assess.toString());
        }
        else {
            log.info("Assess could not be generated for patient id " + patientId);
        }
        return assess;
    }

    /**
     * Generates diabetes assess for a patient. Returns the found assess on success.
     *
     * @param patientFamily the patient's family name
     * @return Assess  the generated assess
     */
    @Operation(summary = "Generates diabetes assess for a patient's family name.",
               description = "Generates assess for a patient id. Returns the generated assess on success.")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "The generated assess")
            }
    )
    @Validated
    @GetMapping(value = "")
    public Assess getAssesByPatientFamilyName(
            @RequestParam("patientFamily") @NotBlank @Size(min = 2, max = 50) String patientFamily) throws
            EntityNotFoundException {
        log.info(String.valueOf(patientFamily));
        Assess assess = iAssessService.generateByPatientFamily(patientFamily);
        log.info(assess.toString());
        return assess;
    }


}