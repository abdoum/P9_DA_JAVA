package com.abernathe.assess.controller;

import com.abernathe.assess.exception.EntityAlreadyExistsException;
import com.abernathe.assess.exception.EntityNotFoundException;
import com.abernathe.assess.model.RuleSuite;
import com.abernathe.assess.service.IRuleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Validated
@CrossOrigin(allowedHeaders = "*", origins = "*")
@RequestMapping("/api/v1/rules")
@Slf4j
public class RuleSuiteController {

    private final IRuleService iRuleService;

    /**
     * Default constructor
     *
     * @param iRuleService
     */
    public RuleSuiteController(IRuleService iRuleService) {
        this.iRuleService = iRuleService;
    }

    /**
     * Gets all the rule suite from database
     *
     * @return a list of rule suites or an empty list if none found
     */
    @Operation(summary = "Finds all the rule suites.",
               description = "Finds all the rule suites.")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "A list of rule suites")
            }
    )
    @GetMapping("")
    List<RuleSuite> findAll() {
        return iRuleService.findAll();
    }

    /**
     * Saves a ruleSuite to database
     *
     * @param ruleSuite the ruleSuite to be saved
     * @return the saved rule suite
     */
    @Operation(summary = "Saves a rule suite.",
               description = "Adds a new rules suite.")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "The added rule suite")
            }
    )
    @PostMapping("")
    RuleSuite save(@Valid @RequestBody RuleSuite ruleSuite) throws EntityAlreadyExistsException {
        log.info("save : {}", ruleSuite);
        RuleSuite savedRuleSuite = iRuleService.save(ruleSuite);
        log.info("saved rule id : {}", savedRuleSuite.getId());
        return savedRuleSuite;
    }

    /**
     * Deletes a rule from database
     *
     * @param id the rule suite id to be deleted
     * @throws EntityNotFoundException if the rule suite could not be found
     */
    @Operation(summary = "Delete a rule suite.",
               description = "Delete a saved rules suite.")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Empty response")
            }
    )
    @DeleteMapping("/{id}")
    void delete(@PathVariable("id") Integer id) throws EntityNotFoundException {
        log.info("delete request for: {}", id);
        iRuleService.delete(id);
    }
}