package com.abernathe.assess.bean;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.Nullable;

import javax.validation.constraints.*;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * The type Note bean.
 */
@Data
public class NoteBean {

    private String id;

    @NotNull
    @Positive
    private Long patId;

    @NotBlank
    @Pattern(regexp = "Patient states that [\\p{Alnum}\\p{Punct}\\p{Space}]{4,}", message = "La note doit respecter " +
                                                                                            "le format : 'Patient " +
                                                                                            "states that ...'")
    private String content;

    @PastOrPresent
    private LocalDateTime createdAt;

    @PastOrPresent
    private LocalDateTime updatedAt;

    /**
     * Instantiates a new Note bean.
     */
    public NoteBean() {
    }

    /**
     * Instantiates a new Note bean.
     *
     * @param patId   the pat id
     * @param content the content
     */
    public NoteBean(Long patId, String content) {
        this.setPatId(patId);
        this.setContent(content);
    }

    /**
     * Instantiates a new Note bean.
     *
     * @param patId     the pat id
     * @param content   the content
     * @param createdAt the created at
     * @param updatedAt the updated at
     */
    public NoteBean(Long patId, String content, LocalDateTime createdAt, @Nullable LocalDateTime updatedAt) {
        this.setPatId(patId);
        this.setContent(content);
        this.setCreatedAt(createdAt);
        this.setUpdatedAt(updatedAt);
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets pat id.
     *
     * @return the pat id
     */
    public Long getPatId() {
        return patId;
    }

    /**
     * Sets pat id.
     *
     * @param patId the pat id
     */
    public void setPatId(@Positive Long patId) {
        this.patId = patId;
    }

    /**
     * Gets content.
     *
     * @return the content
     */
    public String getContent() {
        return content;
    }


    /**
     * Sets content.
     *
     * @param content the content
     */
    public void setContent(@Pattern(regexp = "Patient states that [\\p{Alnum}\\p{Punct}\\p{Space}]{4,}",
                                    message = "La note doit respecter " +
                                              "le format : 'Patient " +
                                              "states that ...'") String content) {
        this.content = StringUtils.normalizeSpace(content.trim());
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    /**
     * Sets created at.
     *
     * @param createdAt the created at
     */
    public void setCreatedAt(@PastOrPresent LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * Gets updated at.
     *
     * @return the updated at
     */
    @Nullable public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    /**
     * Sets updated at.
     *
     * @param updatedAt the updated at
     */
    public void setUpdatedAt(@PastOrPresent LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NoteBean note = (NoteBean) o;
        return Objects.equals(id, note.id) && Objects.equals(patId, note.patId) &&
               Objects.equals(content, note.content) && Objects.equals(createdAt, note.createdAt) &&
               Objects.equals(updatedAt, note.updatedAt);
    }

    @Override public int hashCode() {
        return Objects.hash(id, patId, content, createdAt, updatedAt);
    }

    @Override public String toString() {
        return "Note{" +
               "id='" + id + '\'' +
               ", patId=" + patId +
               ", content='" + content + '\'' +
               ", createdAt=" + createdAt +
               ", updatedAt=" + updatedAt +
               '}';
    }
}