package com.abernathe.assess.bean;

import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.Period;
import java.util.Objects;


@NoArgsConstructor
public class PatientBean {

    private Long id;

    private String given;

    private String family;

    private LocalDate dob;

    private GenreBean sex;

    private String phone;

    private String address;

    /**
     * Instantiates a new Patient bean.
     *
     * @param given   the given
     * @param family  the family
     * @param dob     the dob
     * @param sex     the sex
     * @param phone   the phone
     * @param address the address
     */
    public PatientBean(String given, String family, LocalDate dob, GenreBean sex, String phone, String address) {
        this.given   = given;
        this.family  = family;
        this.dob     = dob;
        this.sex     = sex;
        this.phone   = phone;
        this.address = address;
    }

    /**
     * Gets given.
     *
     * @return the given
     */
    public String getGiven() {
        return given;
    }

    /**
     * Gets family.
     *
     * @return the family
     */
    public String getFamily() {
        return family;
    }

    /**
     * Gets dob.
     *
     * @return the dob
     */
    public LocalDate getDob() {
        return dob;
    }

    /**
     * Gets sex.
     *
     * @return the sex
     */
    public GenreBean getSex() {
        return sex;
    }

    /**
     * Gets phone.
     *
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Gets address.
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Gets age.
     *
     * @return the age
     */
    public int getAge() {
        return Period.between(this.getDob(), LocalDate.now()).getYears();
    }

    /**
     * Sets phone.
     *
     * @param numeroTelephone the numero telephone
     */
    public void setPhone(String numeroTelephone) {
        this.phone = numeroTelephone;
    }

    /**
     * Sets sex.
     *
     * @param genre the genre
     */
    public void setSex(GenreBean genre) {
        this.sex = genre;
    }

    /**
     * Sets dob.
     *
     * @param dateNaissance the date naissance
     */
    public void setDob(LocalDate dateNaissance) {
        this.dob = dateNaissance;
    }

    /**
     * Sets family.
     *
     * @param nom the nom
     */
    public void setFamily(String nom) {
        this.family = nom;
    }

    /**
     * Sets given.
     *
     * @param prenom the prenom
     */
    public void setGiven(String prenom) {
        this.given = prenom;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    @Override public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PatientBean that = (PatientBean) o;
        return Objects.equals(id, that.id) && Objects.equals(given, that.given) &&
               Objects.equals(family, that.family) && Objects.equals(dob, that.dob) &&
               sex == that.sex && Objects.equals(phone, that.phone) &&
               Objects.equals(address, that.address);
    }

    @Override public int hashCode() {
        return Objects.hash(id, given, family, dob, sex, phone, address);
    }

    @Override public String toString() {
        return "PatientBean{" +
               "id=" + id +
               ", given='" + given + '\'' +
               ", family='" + family + '\'' +
               ", dob=" + dob +
               ", sex=" + sex +
               ", phone='" + phone + '\'' +
               ", address='" + address + '\'' +
               '}';
    }
}