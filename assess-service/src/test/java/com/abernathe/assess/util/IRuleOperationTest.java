package com.abernathe.assess.util;

import com.abernathe.assess.bean.GenreBean;
import com.abernathe.assess.bean.PatientBean;
import com.abernathe.assess.model.AssessResult;
import com.abernathe.assess.model.Node;
import com.abernathe.assess.model.RuleSuite;
import com.abernathe.assess.model.Tree;
import com.abernathe.assess.service.IRuleService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
@ActiveProfiles("test")
class IRuleOperationTest {

    final RuleSuite rule1 = new RuleSuite("N.alerts>=3^P.age<30^P.sex=F->IN_DANGER");

    final RuleSuite rule2 = new RuleSuite("N.alerts>=3^P.age<30^P.sex=M->IN_DANGER");

    final RuleSuite rule3 = new RuleSuite("N.alerts>=6^P.age>30^P.sex=M->IN_DANGER");

    final RuleSuite rule4 = new RuleSuite("N.alerts>=6^P.age>30^P.sex=F->IN_DANGER");

    final RuleSuite rule5 = new RuleSuite("N.alerts>=2^P.age>30^P.sex=M->BORDERLINE");

    final RuleSuite rule6 = new RuleSuite("N.alerts>=2^P.age>30^P.sex=F->BORDERLINE");

    final RuleSuite rule7 = new RuleSuite("N.alerts>=5^P.age<30^P.sex=M->EARLY_ON_SET");

    final RuleSuite rule8 = new RuleSuite("N.alerts>=7^P.age<30^P.sex=M->NONE");

    List<RuleSuite> ruleList = new ArrayList<>(List.of(rule1, rule2, rule4, rule5, rule3, rule6, rule7, rule8));

    @Mock
    private IRuleService iRuleService;

    IRuleOperation classUnderTest;

    IRuleProcessor iRuleProcessor;

    @BeforeEach
    void setUp() {
        classUnderTest = new RuleOperation(ruleList,
                new RuleProcessor(ruleList),
                new RuleFormatter(),
                iRuleService);
    }

    @Test void testAssessResultEnum_shouldReturn2_forInDangerValue() {
        assertEquals(2, AssessResult.IN_DANGER.ordinal());
    }

    @Test void testSetNodeValue_shouldAddTheValueToTheNode_forAValidNode() {
        Node<Integer> node = new Node<>(1);
        node.setValue(2);
        assertEquals(2, node.getValue());
    }

    @Test void testAddRuleToTree_shouldReturnTheUpdatedTree_forAValidAddedRuleSuite() {
        final String rule = "N.alerts>=3^P.age<30^P.sex=F->IN_DANGER";
        Tree<Integer> tree = new Tree<>(0, 2, 4);
        classUnderTest.addRuleToTree(rule, tree);
        Integer result = tree.getResult(List.of(3, -1, 1));
        Integer result2 = tree.getResult(List.of(4, -1, 1));   // Check if superior nodes have been updated as well
        assertEquals(2, result);
        assertEquals(2, result2);
    }

    @Test
    void testSortRulesByAlertsValue_shouldReturnTheSortedRules_forRulesWithDifferentAlertsValues() {
        List<String> sortedRules = classUnderTest.sortRulesByAlerts();
        assertEquals(rule5.getContent(), sortedRules.get(0));
    }

    @Test
    void testGetRulesRange_shouldReturnARangeFromMinToMaxAlertValue() {
        IntSummaryStatistics rulesRange = classUnderTest.getRulesRange();
        assertEquals(2, rulesRange.getMin());
        assertEquals(7, rulesRange.getMax());
    }

    @Test
    void testAddRulesToTree_shouldAddTheGivenRulesToTheTree_ForAListOfValidRules() {
        // Arrange
        Tree<Integer> tree = classUnderTest.addRulesToTree();
        // Act and Assert
        Integer result = tree.getResult(List.of(4, -1, 1));
        assertEquals(2, result);
    }

    @Test
    void testGetRuleRange_shouldReturnTheCorrectRange_forARuleWithAnOperator() {
        // Arrange
        Tree<Integer> tree = new Tree<>(0, 2, 7);
        // Act and Assert
        IntStream actualResult = classUnderTest.getRuleRange(rule1.getContent(), tree.getTreeMax());
        assertEquals(IntStream.rangeClosed(3, 7).count(), actualResult.count());
    }

    @Test
    @DisplayName("Adds multiple rules to tree")
    void testAddRulesToTree_shouldReturnTheModifiedTree_forAValidRuleList() {
    }

    @Test void testConvertRuleToInt_shouldReturnTheConverted_forAValidRule() {
        List<Integer> actualResult = classUnderTest.convertRuleToInt(rule1.getContent());
        assertEquals(List.of(3, -1, 1, 2), actualResult);
    }

    @Test
    void testGetAgeLimit_shouldReturn30_forARuleListWithAnAgeLimitOf30() {
        int ageLimit = classUnderTest.getAgeLimit();
        assertEquals(30, ageLimit);
    }

    @Test
    void testProcessPatient_shouldReturnAListOf311_forAValidPatientWithANoteHistory() {
        // Arrange
        PatientBean patient = new PatientBean("punctual", "rivalry", LocalDate.parse("1978-03-23"), GenreBean.F,
                "876-523-5832", "12 Albatro's St.");
        patient.setId(1L);
        // Act and Assert
        List<Integer> intValues = classUnderTest.processPatient(patient, 3);
        assertEquals(List.of(3, 1, 1), intValues);
    }

    @Test
    void testGetResultForPatient_shouldReturn1_forAPatientWithValues311() {
        // Act and Assert
        Tree<Integer> tree = classUnderTest.addRulesToTree();
        Integer actualResult = tree.getResult(List.of(3, 1, 1));
        verify(iRuleService, times(0)).findAll();
        assertEquals(1, actualResult);
    }

    @Test
    void testGenerateTree_shouldGenerateATreeWithDefaultRules() {
        // Arrange
        RuleSuite rule = new RuleSuite("N.alerts>=3^P.age<30^P.sex=F->IN_DANGER");
        RuleSuite rule2 = new RuleSuite("N.alerts>=3^P.age<30^P.sex=M->IN_DANGER");
        RuleSuite rule3 = new RuleSuite("N.alerts>=6^P.age>30^P.sex=M->IN_DANGER");
        RuleSuite rule4 = new RuleSuite("N.alerts>=6^P.age>30^P.sex=F->IN_DANGER");
        RuleSuite rule5 = new RuleSuite("N.alerts>=2^P.age>30^P.sex=M->BORDERLINE");
        RuleSuite rule6 = new RuleSuite("N.alerts>=5^P.age<30^P.sex=M->EARLY_ON_SET");
        RuleSuite rule7 = new RuleSuite("N.alerts>=7^P.age<30^P.sex=M->NONE");
        List<RuleSuite> ruleList = new ArrayList<>(List.of(rule, rule2, rule4, rule5, rule3, rule6, rule7));
        List<String> stringList = ruleList.stream().map(RuleSuite::getContent).collect(Collectors.toList());
        Tree<Integer> tree = new Tree<>(0, 2, 8);
        iRuleProcessor = new RuleProcessor(ruleList);
        // Act and Assert
        stringList.sort(Comparator.comparingInt(iRuleProcessor::getAlertsRuleValue));
        classUnderTest.addRuleToTree(rule.getContent(), tree);
        Integer actualResult = tree.getResult(List.of(5, -1, 1));
        assertEquals(2, actualResult);
    }

    @Test
    void testGenerateTree_shouldGenerateATreeWithDefaultValues() {
        // Arrange
        RuleSuite rule = new RuleSuite("N.alerts>=3^P.age<30^P.sex=F->IN_DANGER");
        List<RuleSuite> ruleList = new ArrayList<>(List.of(rule));
        List<String> stringList = ruleList.stream().map(RuleSuite::getContent).collect(Collectors.toList());
        Tree<Integer> tree = new Tree<>(0, 0, 8);
        iRuleProcessor = new RuleProcessor(ruleList);
        // Act and Assert
        stringList.sort(Comparator.comparingInt(iRuleProcessor::getAlertsRuleValue));
        classUnderTest.addRuleToTree(rule.getContent(), tree);
        Integer actualResult = tree.getResult(List.of(1, -1, 1));
        assertEquals(0, actualResult);
    }

    @Test
    void testGetResultForPatient_shouldReturn2_forAPatientWithIN_DANGER() {
        // Arrange
        RuleSuite rule2 = new RuleSuite("N.alerts>=3^P.age<30^P.sex=M->IN_DANGER");
        RuleSuite rule3 = new RuleSuite("N.alerts>=6^P.age>30^P.sex=M->IN_DANGER");
        List<RuleSuite> rules = List.of(rule2, rule3);
        classUnderTest = new RuleOperation(rules,
                new RuleProcessor(rules), new RuleFormatter(), iRuleService);
        Tree<Integer> tree = new Tree<>(3, 3, 6);
        // Act and Assert
        rules.stream().map(RuleSuite::getContent).forEach(rule -> classUnderTest.addRuleToTree(rule, tree));
        Integer actualResult = tree.getResult(List.of(6, 1, 0));
        assertEquals(2, actualResult);
    }

    @Test
    void testGetResultForPatient_shouldReturn0_forAPatientWithNoAlertKeywords() {
        // Arrange
        PatientBean patient = new PatientBean("SIMILIQUE", "ANIM", LocalDate.parse("1972-11-05"), GenreBean.F,
                "876-523-5832", "12 Albatro's St.");
        patient.setId(1L);
        // Act and Assert
        int actualResult = classUnderTest.getResultForPatient(patient, 0);
        assertEquals(0, actualResult);
    }

}