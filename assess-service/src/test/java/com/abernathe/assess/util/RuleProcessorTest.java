package com.abernathe.assess.util;

import com.abernathe.assess.model.RuleSuite;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
class RuleProcessorTest {

    private IRuleProcessor classUnderTest;

    @BeforeEach
    void setUp() {
        RuleSuite rule = new RuleSuite("N.alerts>=3^P.age<30^P.sex=F->IN_DANGER");
        RuleSuite rule2 = new RuleSuite("N.alerts>=3^P.age<30^P.sex=M->IN_DANGER");
        RuleSuite rule3 = new RuleSuite("N.alerts>=6^P.age>30^P.sex=M->IN_DANGER");
        RuleSuite rule4 = new RuleSuite("N.alerts>=6^P.age>30^P.sex=F->IN_DANGER");
        RuleSuite rule5 = new RuleSuite("N.alerts>=2^P.age>30^P.sex=M->BORDERLINE");
        RuleSuite rule6 = new RuleSuite("N.alerts>=5^P.age<30^P.sex=M->EARLY_ON_SET");
        RuleSuite rule7 = new RuleSuite("N.alerts>=7^P.age<30^P.sex=M->NONE");

        List<RuleSuite> ruleList = new ArrayList<>(List.of(rule, rule2, rule4, rule5, rule3, rule6, rule7));
        classUnderTest = new RuleProcessor(ruleList);
    }

    @Test
    void testProcessRule_shouldReturnTheRuleAlertValue_forAValidRuleFormat() {
        RuleSuite rule = new RuleSuite("NoteBean.getAlerts>=3^PatientBean.getAge<30^PatientBean.getSex=F->IN_DANGER");
        Integer actualResult = classUnderTest.processRule(rule.getContent());
        assertEquals(3, actualResult);
    }

    @Test
    void testProcessGenre_shouldReturn0_forAMGenreConstant() {
        int actualResult = classUnderTest.processGenre("M");
        assertEquals(0, actualResult);
    }


    @DisplayName("Get the alerts values from multiple rules")
    @Test
    void testGetAlertsRuleValues_shouldReturn3675_forAListContaining3675AsAlertValues() {
        int[] alertsRuleValues = classUnderTest.getAlertRulesValues();
        assertThat(alertsRuleValues).isNotEmpty().contains(3, 6, 7, 5);
    }

    @Test
    void testGetAlertsRuleOperator_shouldReturnGreaterThanOrEqual_forARuleContainingTheMatchingOperator() {
        RuleSuite rule = new RuleSuite("N.alerts>=3^P.age<30^P.sex=F->IN_DANGER");
        String actualResult = classUnderTest.getAlertsRuleOperator(rule.getContent());
        assertEquals(">=", actualResult);
    }

    @Test
    void testGetAlertRulesValues_shouldReturn3_forARuleWith3Alerts() {
        RuleSuite rule = new RuleSuite("N.alerts>=3^P.age<30^P.sex=F->IN_DANGER");
        int actualResult = classUnderTest.getAlertsRuleValue(rule.getContent());
        assertEquals(3, actualResult);
    }

    @DisplayName("Get the alerts range from multiple rules")
    @Test
    void testGetAlertsRange_shouldReturnAClosedRangeFrom2To7_forARuleListFrom2To7() {
        IntStream alertsRuleValues = classUnderTest.getAlertsRange();
        int[] rangeValues = alertsRuleValues.toArray();
        assertThat(rangeValues[0]).isEqualTo(2);
        assertThat(rangeValues[rangeValues.length - 1]).isEqualTo(7);
    }

    @DisplayName("Returns the min and max alerts values from a list of rule suites")
    @Test
    void testGetAlertRangeFromRules_shouldReturnARangeBetweenMinAndMax() {
        IntStream alertsRange = classUnderTest.getAlertsRange();
        assertEquals(IntStream.rangeClosed(2, 7).count(), alertsRange.count());
    }

    @Test
    void testGetOperatorOrdinal_shouldReturnLessOrEqualOperator_forTheLessOrEqualSign() {
        Operators foundOperator = classUnderTest.getOperator("<=");
        assertEquals(Operators.LESS_OR_EQUAL, foundOperator);
    }

}