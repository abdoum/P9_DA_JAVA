package com.abernathe.assess.util;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.abernathe.assess.util.Operators.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
class OperatorsTest {

    @Test
    void testGetValue() {
        assertEquals("<=", LESS_OR_EQUAL.getValue());
    }

    @Test
    void testValueOf() {
        assertEquals(GREATER_OR_EQUAL, valueOf("GREATER_OR_EQUAL"));
    }
}