package com.abernathe.assess.controller;

import com.abernathe.assess.model.RuleSuite;
import com.abernathe.assess.repository.IRuleSuiteDAO;
import com.abernathe.assess.service.IRuleService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("test")
class RuleSuiteControllerTest {

    @MockBean
    IRuleService iRuleService;

    @Autowired
    RuleSuiteController classUnderTest;

    @Autowired
    MockMvc mockMvc;

    @MockBean
    IRuleSuiteDAO iRuleSuiteDAO;

    @Test
    void testFindAll_shouldReturnTheSavedAlertKeyword_forANonExistingAlertKeyword() throws Exception {
        // Arrange
        List<RuleSuite> ruleList = List.of(new RuleSuite("Poids"), new RuleSuite("Fumeur"));
        when(iRuleService.findAll()).thenReturn(ruleList);
        // Act and Assert
        mockMvc.perform(get("/api/v1/rules")
                       .accept("application/json"))
               .andDo(print())
               .andExpect(status().isOk());
    }

    @Test
    void testSave_shouldReturnTheSavedRuleSuite_forANonExistingRuleSuite() throws Exception {
        // Arrange
        RuleSuite rule = new RuleSuite("N.alerts>=3^P.age<30^P.sex=F->IN_DANGER");
        rule.setId(null);
        when(iRuleService.save(any())).thenReturn(rule);
        ObjectMapper mapper = new ObjectMapper();
        // Act and Assert
        mockMvc.perform(post("/api/v1/rules")
                       .contentType(MediaType.APPLICATION_JSON)
                       .content(mapper.writeValueAsString(rule)))
               .andExpect(jsonPath("$.content", is("N.alerts>=3^P.age<30^P.sex=F->IN_DANGER")))
               .andDo(print())
               .andExpect(status().isOk());
        verify(iRuleService, times(1)).save(rule);
    }

    @Test
    void testDelete_shouldReturnHttpStatusOk_forAnExistingAlertKeyword() throws Exception {
        // Arrange
        doNothing().when(iRuleService).delete(anyInt());
        // Act and Assert
        mockMvc.perform(delete("/api/v1/rules/{id}", 1))
               .andDo(print())
               .andExpect(status().isOk());
    }
}