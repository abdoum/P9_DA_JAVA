package com.abernathe.assess.controller;

import com.abernathe.assess.model.Assess;
import com.abernathe.assess.model.AssessResult;
import com.abernathe.assess.service.IAssessService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class AssessControllerTest {

    @MockBean
    IAssessService iAssessService;

    @Autowired
    AssessController assessController;

    @Autowired
    MockMvc mockMvc;

    @Test
    void testGetAssesByPatientId_shouldReturnTheAssessResult_forAValidPatientId() throws Exception {
        // Arrange
        Assess note = new Assess("copy", "standard", 38, AssessResult.NONE);
        when(iAssessService.getResult(anyLong())).thenReturn(note);
        // Act and Assert
        mockMvc.perform(get("/reports/{patientId}", "11")
                       .accept("application/json"))
               .andExpect(jsonPath("$.patientFamily", is("standard")))
               .andExpect(jsonPath("$.age", is(38)))
               .andExpect(jsonPath("$.assessResult", is("NONE")))
               .andDo(print())
               .andExpect(status().isOk());
        verify(iAssessService, times(1)).getResult(11L);
    }

    @Test
    void testGetAssesByPatientFamilyName_shouldReturnTheAssessResult_forAValidPatientFamilyName() throws Exception {
        // Arrange
        Assess note = new Assess("copy", "standard", 38, AssessResult.NONE);
        when(iAssessService.generateByPatientFamily(anyString())).thenReturn(note);
        // Act and Assert
        mockMvc.perform(get("/reports")
                       .param("patientFamily", "standard")
                       .accept("application/json"))
               .andExpect(jsonPath("$.patientFamily", is("standard")))
               .andExpect(jsonPath("$.age", is(38)))
               .andDo(print())
               .andExpect(status().isOk());
    }
}