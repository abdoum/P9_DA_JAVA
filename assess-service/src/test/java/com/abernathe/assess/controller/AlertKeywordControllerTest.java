package com.abernathe.assess.controller;

import com.abernathe.assess.model.AlertKeyword;
import com.abernathe.assess.service.IAlertKeywordService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class AlertKeywordControllerTest {

    @MockBean
    IAlertKeywordService iAlertKeywordService;

    @Autowired
    AlertKeywordController alertKeywordController;

    @Autowired
    MockMvc mockMvc;

    @Test
    void testFindAll_shouldReturnTheFoundAlertKeywords_forAExistingAlertKeywords() throws Exception {
        // Arrange
        List<AlertKeyword> alertKeywords = List.of(new AlertKeyword("Poids"), new AlertKeyword("Fumeur"));
        when(iAlertKeywordService.findAll()).thenReturn(alertKeywords);
        // Act and Assert
        mockMvc.perform(get("/api/v1/alert-keywords")
                       .accept("application/json"))
               .andDo(print())
               .andExpect(status().isOk());
    }

    @Test
    void testSave_shouldReturnTheSavedAlertKeyword_forANonExistingAlertKeyword() throws Exception {
        // Arrange
        AlertKeyword poids = new AlertKeyword("Poids");
        poids.setId(3);
        when(iAlertKeywordService.save(any())).thenReturn(poids);
        ObjectMapper mapper = new ObjectMapper();
        // Act and Assert
        mockMvc.perform(post("/api/v1/alert-keywords")
                       .contentType(MediaType.APPLICATION_JSON)
                       .content(mapper.writeValueAsString(poids)))
               .andExpect(jsonPath("$.content", is("Poids")))
               .andDo(print())
               .andExpect(status().isOk());
    }

    @Test
    void testDelete_shouldReturnHttpStatusOk_forAnExistingAlertKeyword() throws Exception {
        // Arrange
        doNothing().when(iAlertKeywordService).delete(anyInt());
        // Act and Assert
        mockMvc.perform(delete("/api/v1/alert-keywords/{id}", 1))
               .andDo(print())
               .andExpect(status().isOk());
    }
}