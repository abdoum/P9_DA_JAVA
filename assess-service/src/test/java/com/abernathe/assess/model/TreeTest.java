package com.abernathe.assess.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ActiveProfiles("test")
class TreeTest {

    Tree<Integer> tree;

    @BeforeEach
    void setUp() {
        tree = new Tree<>(0, 2, 3);
    }

    @Test
    void testFind_shouldReturn1_forAGivenTree() {
        Node<Integer> node = tree.find(tree.getRoot(), 3);
        assertNotNull(node);
        assertEquals(1, node.getChildren().get(0).getValue());
    }

    @Test
    void testGetResult_shouldReturn0_forANoneResultValue() {
        var result = tree.getResult(List.of(2, -1, 1));
        assertEquals(AssessResult.NONE.ordinal(), result);
    }

    @Test
    void testGetChildrenFromCase_shouldReturnTheChildren_forTheGivenCase() {
        var cases = tree.getChildrenForCase(new ArrayList<>(Arrays.asList(2, -1, 0)), 7);
        assertEquals(2, cases.size());
    }

    @Test
    void testGetChildrenForCaseAndRange_shouldReturnAListOfNodes_forTheGivenCaseAndRange() {
        tree = new Tree<>(2, 2, 3);
        List<Integer> args = List.of(2, -1, 1);
        Integer min = tree.getMin();
        IntStream range = IntStream.rangeClosed(min, 3);
        Set<Node<Integer>> childrenForCaseAndRange = tree.getChildrenForCaseAndRange(new ArrayList<>(args), range);
        assertEquals(2, childrenForCaseAndRange.size());
    }


}