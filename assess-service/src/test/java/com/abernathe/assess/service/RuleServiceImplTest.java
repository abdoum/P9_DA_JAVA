package com.abernathe.assess.service;

import com.abernathe.assess.exception.EntityAlreadyExistsException;
import com.abernathe.assess.exception.EntityNotFoundException;
import com.abernathe.assess.model.RuleSuite;
import com.abernathe.assess.repository.IRuleSuiteDAO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
class RuleServiceImplTest {

    @Autowired
    IRuleService iRuleService;

    @MockBean
    IRuleSuiteDAO iRuleSuiteDAO;

    @Test
    void testSave_shouldReturnTheSavedRuleSuite_forANonExistingRuleSuite() throws EntityAlreadyExistsException {
        // Arrange
        RuleSuite rule = new RuleSuite("book clever yudhtywiutkh ywhtwyyuh aryuhwyh");
        when(iRuleSuiteDAO.save((RuleSuite) any())).thenReturn(rule);
        // Act and Assert
        RuleSuite actualResult = iRuleService.save(rule);
        assertEquals(rule, actualResult);
        verify(iRuleSuiteDAO, times(1)).save(rule);
    }

    @Test
    void testDelete_shouldRunWithoutException_forAnExistingRuleSuite() throws EntityNotFoundException {
        // Arrange
        doNothing().when(iRuleSuiteDAO).deleteById(anyInt());
        when(iRuleSuiteDAO.findById(anyInt())).thenReturn(Optional.of(new RuleSuite("rot")));
        // Act and Assert
        iRuleService.delete(1);
        verify(iRuleSuiteDAO, times(1)).deleteById(1);
    }

    @Test
    void testFindAll_shouldReturnTheExistingRulesSuites_forExistingRuleSuites() {
        // Arrange
        List<RuleSuite> ruleSuiteList = List.of(
                new RuleSuite("solve"),
                new RuleSuite("limb")
        );
        when(iRuleSuiteDAO.findAll()).thenReturn(ruleSuiteList);
        // Act and Assert
        List<RuleSuite> actualResult = iRuleService.findAll();
        assertEquals(ruleSuiteList, actualResult);
        verify(iRuleSuiteDAO, times(2)).findAll();
    }
}