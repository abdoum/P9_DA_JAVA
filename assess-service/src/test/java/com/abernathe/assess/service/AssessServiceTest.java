package com.abernathe.assess.service;

import com.abernathe.assess.bean.GenreBean;
import com.abernathe.assess.bean.NoteBean;
import com.abernathe.assess.bean.PatientBean;
import com.abernathe.assess.exception.EntityNotFoundException;
import com.abernathe.assess.model.Assess;
import com.abernathe.assess.model.AssessResult;
import com.abernathe.assess.proxy.INoteProxy;
import com.abernathe.assess.proxy.IPatientProxy;
import com.abernathe.assess.util.RuleOperation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles("test")
class AssessServiceTest {

    @Autowired
    IAssessService classUnderTest;

    @MockBean
    IPatientProxy iPatientProxy;

    @MockBean
    INoteProxy iNoteProxy;

    @MockBean
    RuleOperation ruleOperationFactory;

    @Test
    void testGenerateByPatientId_shouldReturnNone_forAPatientWithLessThan2AlertKeywordsAndAgeLessThan30() throws
            EntityNotFoundException {
        // Arrange
        PatientBean patientBean = new PatientBean("ounce", "guest",
                LocalDate.of(1995, 3, 2), GenreBean.F, "234-564-6868",
                "ZJ39tag St");
        patientBean.setId(1L);
        List<NoteBean> noteBeanList = List.of(new NoteBean[]{new NoteBean(1L, "Patient states that Poids")});
        when(iPatientProxy.findById(anyLong())).thenReturn(
                ResponseEntity.ok(Optional.of(patientBean)));
        when(iNoteProxy.getHistoryByPatientId(anyLong())).thenReturn(noteBeanList);
        // Act and Assert
        Assess assess = classUnderTest.generateByPatientId(1L);
        System.out.println("assess = " + assess);
        assertThat(assess.getAssessResult()).isEqualTo(AssessResult.NONE);
    }

    @ParameterizedTest(name = "{arguments}")
    @CsvSource({
            " '1978-08-08', 'F', 'Patient states that Poids Fumeur Microalbumine Vertige Rechute', 5 , 'NONE'",
            " '1998-08-08', 'M', 'Patient states that Poids Fumeur Réaction',3, 'IN_DANGER'",
            " '1998-08-08', 'F', 'Patient states that Poids Fumeur Réaction',3, 'BORDERLINE'",
            " '1998-08-08', 'F', 'Patient states that Poids Fumeur Hémoglobine A1C Anticorps',4, 'IN_DANGER'",
            " '1998-08-08', 'M', 'Patient states that Poids Fumeur Hémoglobine A1C Anticorps',4, 'IN_DANGER'",
            " '1998-08-08', 'M', 'Patient states that Poids Fumeur Anormal Vertige Rechute',5, 'EARLY_ON_SET'",
            " '1978-08-08', 'M', 'Patient states that Poids Fumeur Anormal Vertige Rechute Anticorps',6, 'IN_DANGER'",
            " '1978-08-08', 'F', 'Patient states that Poids Fumeur Anormal Vertige Rechute Anticorps',6, 'IN_DANGER'"
    })
    void testGenerateByPatientId_shouldReturnTheProvidedAssessResult_forAPatientWithTheProvidedCriteria(
            ArgumentsAccessor arguments) throws
            EntityNotFoundException {
        // Arrange
        PatientBean patientBean = new PatientBean("ounce", "guest",
                LocalDate.parse(arguments.getString(0)), GenreBean.valueOf(arguments.getString(1)), "234-564-6868",
                "ZJ39tag St");
        patientBean.setId(1L);
        List<NoteBean> noteBeanList = List.of(new NoteBean[]{new NoteBean(1L, arguments.getString(2))});
        when(iPatientProxy.findById(anyLong())).thenReturn(
                ResponseEntity.ok(Optional.of(patientBean)));
        when(iNoteProxy.getHistoryByPatientId(anyLong())).thenReturn(noteBeanList);
        // Act and Assert
        Assess assess = classUnderTest.generateByPatientId(1L);
        assertThat(assess.getAssessResult()).isEqualTo(AssessResult.valueOf(arguments.getString(4)));
    }

    @Test
    void testGenerateByPatientFamily_shouldReturnNone_forAPatientWithLessThan2AlertKeywordsAndAgeLessThan30() throws
            EntityNotFoundException {
        // Arrange
        PatientBean patientBean = new PatientBean("ounce", "guest",
                LocalDate.of(1995, 3, 2), GenreBean.F, "234-564-6868",
                "ZJ39tag St");
        patientBean.setId(1L);
        List<NoteBean> noteBeanList = List.of(new NoteBean[]{new NoteBean(1L, "Patient states that Poids")});
        when(iPatientProxy.findByFamilyName(anyString())).thenReturn(
                ResponseEntity.ok(Optional.of(Collections.singletonList(patientBean))));
        when(iNoteProxy.getHistoryByPatientId(anyLong())).thenReturn(noteBeanList);
        // Act and Assert
        Assess assess = classUnderTest.generateByPatientFamily(patientBean.getFamily());
        assertThat(assess.getAssessResult()).isEqualTo(AssessResult.NONE);
    }

}