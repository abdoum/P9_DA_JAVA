package com.abernathe.assess.service;

import com.abernathe.assess.exception.EntityAlreadyExistsException;
import com.abernathe.assess.exception.EntityNotFoundException;
import com.abernathe.assess.model.AlertKeyword;
import com.abernathe.assess.repository.IAlertKeywordDAO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
class AlertKeywordServiceImplTest {

    @MockBean
    IAlertKeywordDAO iAlertKeywordDAO;

    @Autowired
    IAlertKeywordService iAlertKeywordService;

    @Test
    void testSave_shouldReturnTheSavedAlertKeyword_forANonExistingAlertKeyword() throws EntityAlreadyExistsException {
        // Arrange
        AlertKeyword alertKeyword = new AlertKeyword("pigeon");
        when(iAlertKeywordDAO.getByContent(anyString())).thenReturn(Optional.empty());
        when(iAlertKeywordDAO.save(any())).thenReturn(alertKeyword);
        // Act and Assert
        AlertKeyword actualResult = iAlertKeywordService.save(alertKeyword);
        assertEquals(alertKeyword, actualResult);
        verify(iAlertKeywordDAO, times(1)).getByContent(anyString());
        verify(iAlertKeywordDAO, times(1)).save(any());
    }

    @Test
    void testDelete_shouldRunWithoutException_forAnExistingAlertKeyword() throws EntityNotFoundException {
        // Arrange
        AlertKeyword alertKeyword = new AlertKeyword("pigeon");
        when(iAlertKeywordDAO.findById(anyInt())).thenReturn(Optional.of(alertKeyword));
        doNothing().when(iAlertKeywordDAO).deleteById(anyInt());
        // Act and Assert
        iAlertKeywordService.delete(2);
        verify(iAlertKeywordDAO, times(1)).findById(anyInt());
        verify(iAlertKeywordDAO, times(1)).deleteById(anyInt());
    }

    @Test
    void testUpdate_shouldReturnTheUpdatedAlertKeyword_forAnExistingAlertKeyword() throws EntityNotFoundException {
        // Arrange
        AlertKeyword alertKeyword = new AlertKeyword("pigeon");
        alertKeyword.setId(4);
        AlertKeyword alertKeyword2 = new AlertKeyword("pigeonneau");
        alertKeyword2.setId(4);
        when(iAlertKeywordDAO.findById(anyInt())).thenReturn(Optional.of(alertKeyword));
        when(iAlertKeywordDAO.save(any())).thenReturn(alertKeyword2);
        // Act and Assert
        AlertKeyword actualResult = iAlertKeywordService.update(alertKeyword2);
        assertEquals("pigeonneau", actualResult.getContent());
        verify(iAlertKeywordDAO, times(1)).findById(anyInt());
        verify(iAlertKeywordDAO, times(1)).save(any());
    }

    @Test
    void testFindAll_shouldReturnAllTheSavedAlertKeyword_forExistingAlertKeywords() throws
            EntityAlreadyExistsException {
        // Arrange
        List<AlertKeyword> alertKeywords = List.of(new AlertKeyword("yhdyf"), new AlertKeyword("nhatuyhl"));
        when(iAlertKeywordDAO.findAll()).thenReturn(alertKeywords);
        // Act and Assert
        List<AlertKeyword> actualResult = iAlertKeywordService.findAll();
        assertEquals(alertKeywords, actualResult);
        verify(iAlertKeywordDAO, times(1)).findAll();
    }
}